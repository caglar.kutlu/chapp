"""This module is aimed to provide utilitary functions and classes for CSV file
reading tasks encountered.

Although csv is a prevalent and a simple protocol for data storage, there may
be slight variations on how some metadata is stored along with a CSV file.  The
goal of this module is to separate the metadata from the data in a CSV file and
provide a consistent interface to them.  The module does not aim to be
super-generic or a swiss-army knife.  Just bunch of functions bundled together
for convenience.

The classes in this module are written in a style conformant to the python's
csv module.

TODO:
    - Header is metadata, let it be parsed as one.
    - Make the writer code a bit more generic and consistent.
    - The whole MetaParser scheme is confusing at best.  The structure for
      metadata within csv files is incredibly inconsistent.  Change everything
      to have a more flexible interface.  Until then, just provide reader
      functions and don't implement any classes.

"""
from os import path
import csv
from csv import Dialect, QUOTE_MINIMAL
import typing as t
from functools import wraps
from datetime import datetime as DateTime
from datetime import date as Date
from datetime import time as Time
from enum import Enum
import re

import attr
import numpy as np


def _isempty(line: str):
    return line.isspace()


class MetaFinished(Exception):
    """An exception thrown as a signal to mark the end of metadata parsing."""
    pass


class BadCSV(Exception):
    pass


@attr.s(auto_attribs=True)
class MetaParser:
    """A generic class providing parsing functionality for metadata lines in a CSV.

    The grammar for a metadata line in a csv file is as follows:
    <metadata> ::= <metamarker> <key> <separator> <value>

    where
        <metamarker>:  Any string that declares the start of a metadata,
        <key>:  A key string for the metadata represented on the line,
        <separator>:  Any string used to represent separation of key-value
            pair.
        <value>:  The value string.

    The parser itself does not attempt to coerce any string into any type,
    however this functionality can be provided by the converters.

    Attributes:
        metamarker:  The marker string that a metadata line starts with.
        separator:  The string or character that separates key-value pairs in
            metadata.
        ignoreempty:  Whether or not ignore empty lines.
        trimspaces:  Whether or not trim the leading and trailing spaces from
            keys and values.
        trimseparator:  Trims the extra separator at the end if there is
            nothing to separate.
        acceptkeyonly:  Accepts the metadata lines where there is only key, but
        no value (i.e. there is no separator).  Defaults to False.
        converters:  A dictionary of key, function pairs where the function is
            called on the corresponding metadata value.  If the key of the
            metadata is not found in converters, the value is left untouched
            (possibly as string).

    """
    metamarker: str
    separator: str
    ignoreempty: bool
    trimspaces: bool
    trimseparator: bool = True
    acceptkeyonly: bool = False
    converters: t.Dict[str, t.Callable[[str], t.Any]] = attr.ib(factory=dict)

    def ismetaline(self, line: str) -> bool:
        """Returns `True` if the line contains metadata."""
        metabecausemarker = line.startswith(self.metamarker)
        return metabecausemarker

    def _get_key_value(self, line: str):
        """Returns a key-value pair as strings from given line string."""
        if not self.ismetaline(line):
            raise MetaFinished

        # Removing the meta marker string.
        line = line[len(self.metamarker):]

        spl = line.split(self.separator, 1)  # splits at 1st occ.
        try:
            key, value = spl
        except ValueError as exc:
            if self.acceptkeyonly:
                key, value = spl[0], ""
            else:
                raise MetaFinished from exc

        if self.trimspaces:
            key = key.strip()
            value = value.strip()

        if self.trimseparator:
            s = value.split(self.separator)
            # Discard separator part if it appears at the end of line.
            if (len(s) > 1) and (s[-1].strip() == ""):
                value = value[:-len(s[-1])-len(self.separator)]

        return key, value

    def read_line(self, line: str):
        """Parses the line, applies the converter if it exists and returns the
        key-value pair.  Returns None, None if line is empty and `ignoreempty`
        is True."""
        if self.ignoreempty:
            if line.isspace():
                return None, None
        key, value = self._get_key_value(line)
        if key in self.converters.keys():  # pylint: disable=no-member
            value = self.converters[key](value)  # pylint: disable=E1136
        else:
            value = value
        return key, value


class MetaParserFSV(MetaParser):
    """Parser for the R&S FSV series signal analyzer's csv files metadata"""
    def ismetaline(self, line: str) -> bool:
        supercheck = super().ismetaline(line)
        return supercheck and (line[0].isalpha())


@attr.s(auto_attribs=True)
class MetaParserN9010A(MetaParser):
    """Parser for the Keysight N9010A series signal analyzer's csv files
    metadata.

    Args:
        dataline_registered:  This is set when a line containing DATA is
            encountered.
    """
    dataline_registered: bool = False  # This is mutable!!!

    def _get_key_value(self, line: str):
        """We have to overload this as a temporary hack, because
        dataline_registered is mutable but we use the instance of this class as
        if it is immutable."""
        try:
            kv = super()._get_key_value(line)
        except MetaFinished as e:
            self.dataline_registered = False
            raise e
        return kv

    def ismetaline(self, line: str) -> bool:
        supercheck = super().ismetaline(line)
        # Using only the previous check so that we are on the actual line with
        # data.
        isameta = True
        if self.dataline_registered:
            isameta = False
        elif line.startswith('DATA'):
            # Registering for the next line check!
            self.dataline_registered = True
        return supercheck and isameta


METAPARSER_DEFAULT = MetaParser(
    metamarker='#',
    separator='=',
    ignoreempty=True,
    trimspaces=True)


METAPARSER_FSV = MetaParserFSV(
    metamarker='',
    separator=';',
    ignoreempty=False,  # not sure, being conservative
    trimspaces=False)  # not sure, being conservative

METAPARSER_N9010A = MetaParserN9010A(
    metamarker='',
    separator=',',
    ignoreempty=True,
    trimspaces=True,
    acceptkeyonly=True)


@attr.s
class XDialect(Dialect):
    """An extended class for dialect description conforming to the csv.Dialect
    interface along with some defaults used here.

    Caveats:
        If the subclasses of this class is registered via `register_dialect`
        the returned class via `get_dialect` will not hold the additional
        attributes.

    Additional Attributes:
        metaparser:  A MetaParser object.
        decimalseparator:  The character used for decimal separation in
            real numbers.  Some locales use comma(,) rather than dot(.) for
            separating decimals, and consequently some instruments (hint hint
            R&S FSV) will return csv files in this format.
    """
    delimiter = ','
    quotechar = '"'
    doublequote = True
    skipinitialspace = False
    lineterminator = '\n'  # This is not used actually.
    quoting = QUOTE_MINIMAL
    metaparser: MetaParser = METAPARSER_DEFAULT
    decimalseparator: str = '.'


CAPPDialect = XDialect


class FSVDialect(XDialect):
    delimiter = ';'
    decimalseparator = ','


# Type alias for converter dictionaries
TConverterDict = t.Dict[int, t.Callable[[str], t.Any]]


def read_csv(
        fname,
        dtype=float,
        delimiter: str = ',',
        usecols: t.Optional[t.Union[int, t.Sequence]] = None,
        skip_after_meta: int = 0,
        converters: t.Optional[TConverterDict] = None,
        metaparser: t.Optional[MetaParser] = None,
        metaconverters: t.Optional[TConverterDict] = None):
    """Reads the csv file and returns the data along with the metadata
    dictionary.

    Args:
        fname:  Full path to csv file.
        dtype:  Data type of the resulting array.  See numpy.loadtxt
            documentation.
        delimiter:  The delimiter to be used.
        usecols:  Which columns to read, 0 being the first.  If `None` reads
            all, if `int` reads single column, if `tuple` only the
            corresponding columns are read (e.g. `usecols=(1,2)` reads columns
            1 and 2).
        skip_after_meta:  Number of lines to skip after metadata.
        metaparser:  The MetaParser object to use.
        converters:  A dictionary mapping column number to a function that will
            parse the column string into the desired value.
        metaconverters:  A dictionary mapping metadata key strings into a
            function that will parse the corresponding value into the desired
            form.

    Returns:
        data:  As a multi-dimensional numpy array.
        metadict:  A dictionary containing metadata key-value pairs.

    TODO:
        - Put the metadata reading logic into MetaParser itself.
        - Instead of skiprows, you can supply a bytestring generator to
          loadtxt.
    """
    metaconverters = {} if metaconverters is None else metaconverters
    metaparser = METAPARSER_DEFAULT if metaparser is None else metaparser
    metaparser.converters = metaconverters

    with open(fname, 'r') as csvf:
        metadict = {}
        startpos = csvf.tell()  # initial position
        pos = startpos
        linecount = 0
        for line in csvf:
            try:
                key, value = metaparser.read_line(line)
                if key is None:
                    linecount += 1
                    continue
            except MetaFinished:
                break
            metadict[key] = value
            pos += len(line)
            linecount += 1

        # THIS IS TRICKY TO GET RIGHT, USING SKIPROWS INSTEAD
        # roll-back to the start of data lines
        # csvf.seek(pos+1)
        csvf.seek(startpos)

        mm = metaparser.metamarker
        comments = mm if mm != "" else None
        data = np.loadtxt(
            csvf, dtype=dtype, delimiter=delimiter, converters=converters,
            comments=comments, usecols=usecols,
            skiprows=linecount + skip_after_meta)
        return data, metadict


def write_csv(
        fname,
        data,
        delimiter: str = ',',
        header: str = None,
        metadata: t.Dict[str, str] = None,
        append: bool = False,
        **kwargs):
    """Simple csv writing function wrapping `numpy.savetxt`, see it's docs for
    explanation on arguments. """
    metadata = {} if metadata is None else metadata
    metamarker = kwargs.get('comments', '# ')

    metalines = (f"{k}={v}" for k, v in metadata.items())
    comments = metamarker + f"\n{metamarker}".join(metalines)

    # header must be nonempty for np.savetxt to write comments.
    if header is not None:
        comments += "\n" + metamarker
    else:
        header = " "

    if append:
        # HACK IF DATA IS LIST, this workaround shouldnt be here
        if isinstance(data, list):
            with open(fname, 'a') as fout:
                recordstr = ', '.join(map(lambda k: f"{k}", data))
                fout.write(recordstr)
        else:
            with open(fname, 'a') as fname:
                np.savetxt(fname, data, delimiter=delimiter)
    else:
        # HACK IF DATA IS LIST, this workaround shouldnt be here
        if isinstance(data, list):
            with open(fname, 'w') as fout:
                recordstr = ', '.join(map(lambda k: f"{k}", data))
                fout.write('\n'.join([comments, header, recordstr]))
        else:
            np.savetxt(
                fname, data,
                delimiter=delimiter, header=header, comments=comments)


def _replace_decimal_separator(line):
    replaced = line.replace(b',', b'.')
    return replaced


def _decorate_converter(fun):
    @wraps
    def decorated(line):
        out = fun(_replace_decimal_separator(line))
        return out
    return decorated


def read_fsv_dat(
        fname,
        converters: t.Optional[TConverterDict] = None,
        metaconverters: t.Optional[TConverterDict] = None):
    """Reads the .DAT files produced by FSV series spectrum analyzers.
    The .DAT files use ',' as decimal separator.  This function replaces the
    separator to be '.' even if no converter is supplied.

    Args:
        fname:  Full path to csv file.
        converters:  A dictionary mapping column number to a function that will
            parse the column string into the desired value.  The functions are
            decorated with a string replacement function that replaces the
            decimal separator ',' to '.'.  So the converter functions should
            expect number strings with '.' as decimal separator.
        metaconverters:  A dictionary mapping metadata key strings into a
            function that will parse the corresponding value into the desired
            form.

    Returns:
        data:  As a multi-dimensional numpy array.
        metadict:  A dictionary containing metadata key-value pairs.

    """
    usecols = (0, 1)
    if converters is not None:
        converters = {
            k: _decorate_converter(v) for k, v in converters.items()}
        uc = set(usecols)
        ck = set(converters.keys())
        dc = uc.difference(ck)
        for k in dc:
            converters[k] = _replace_decimal_separator
    else:
        converters = {}
        for k in usecols:
            converters[k] = _replace_decimal_separator

    return read_csv(
        fname,
        metaparser=METAPARSER_FSV, delimiter=';', usecols=usecols,
        converters=converters, metaconverters=metaconverters
        )


def read_n9010a_csv(
        fname,
        converters: t.Optional[TConverterDict] = None,
        metaconverters: t.Optional[TConverterDict] = None):
    """Reads the csv files as produced by the Keysight N9010A series spectrum
    analyzers.

    Args:
        fname:  Full path to csv file.
        converters:  A dictionary mapping column number to a function that will
            parse the column string into the desired value.
        metaconverters:  A dictionary mapping metadata key strings into a
            function that will parse the corresponding value into the desired
            form.

    Returns:
        data:  As a multi-dimensional numpy array.
        metadict:  A dictionary containing metadata key-value pairs.

    """
    return read_csv(
        fname,
        metaparser=METAPARSER_N9010A,
        delimiter=',', converters=converters, metaconverters=metaconverters)


def read_n5222a_csv(
        fname):
    """Reads the S-parameter measurement csv file.  Assumes there is only 1
    channel data in the csv.
    
    Returns:
        data:  As a multi-dimensional numpy array.
        fieldnames:  Names of the columns.
        metadict:  A dictionary containing metadata.
    """
    dateformat = "%A, %B %d, %Y %H:%M:%S"
    delimiter = ','
    with open(fname, mode='r') as fin:
        NONE = ""
        version: str = NONE
        instrument_string: str = NONE
        date: DateTime = NONE
        source: str = NONE

        chdata = re.compile("BEGIN CH[0-9]+_DATA")
        # Parsing metadata
        for line in fin:
            swith = line.startswith
            if chdata.match(line):
                break
            elif line.isspace():
                continue
            elif (version is NONE) and swith('!CSV'):
                # We remove the CSV prefix
                version = line.split(' ', 1)[-1].strip()
            elif ((instrument_string is NONE)
                  and swith('!Keysight Technologies')):
                instrument_string = line[1:].strip()
            elif (date is NONE) and swith("!Date"):
                date_str = line.split(' ', 1)[-1].strip()
                date = DateTime.strptime(date_str, dateformat)
            elif (source is NONE) and swith("!Source"):
                source = line.split(' ', 1)[-1].strip()
            else:
                raise BadCSV("Unrecognized format.")
        metadata = dict(
            version=version,
            instrument_string=instrument_string,
            date=date,
            source=source)

        rdr = csv.DictReader(fin, delimiter=delimiter)
        rows = []
        for row in rdr:
            values = list(row.values())
            if not values[0].startswith("END"):
                rows.append(values)
        rows = np.array(rows, dtype='float64')
        # Reader parses the header into fieldnames after first line read.
        fieldnames = rdr.fieldnames
        return rows, fieldnames, metadata


class BFLogType(str, Enum):
    """An Enum class for holding types of files produced by the bluefors
    computer as logs.
    """
    Power = "PRESSURE"
    Resistance = "RESISTANCE"
    Temperature = "TEMPERATURE"
    Channels = "CHANNELS"
    Errors = "ERRORS"
    Flowmeter = "FLOWMETER"
    Heaters = "HEATERS"
    Maxigauge = "MAXIGAUGE"


class BFLogParseError(Exception):
    pass


@attr.s
class BFLogNameParser:
    """Only the LakeShore name parsing implemented so far."""
    fname: str = attr.ib()
    ls_re: t.ClassVar = re.compile(r'CH(?P<cno>[0-9]) (?P<var>[P|R|T]) (?P<date>[0-9]{2,2}-[0-9]{2,2}-[0-9]{2,2}).log')  # noqa: E501
    ls_varmap: t.ClassVar = {
        'P': BFLogType.Power,
        'R': BFLogType.Resistance,
        'T': BFLogType.Temperature}

    def _parse_ls_date(self, datestr: str) -> Date:
        """Only applies to the filename, not the contents."""
        format_ = '%y-%m-%d'  # YYYY-MM-DD
        dtm = DateTime.strptime(datestr, format_)
        return dtm.date()

    def parse_lakeshore(self) -> t.Optional[t.Dict[str, t.Any]]:
        """Returns None if can't parse."""
        matched = self.ls_re.match(self.fname)
        try:
            channel = int(matched.group('cno'))
            logtype = self.ls_varmap[matched.group('var')]
            date = self._parse_ls_date(matched.group('date'))
        except AttributeError:
            # If no match, this will happen.
            return None
        
        out = {'logtype': logtype, 'channel': channel, 'date': date}
        return out

    def parse(self) -> t.Optional[t.Dict[str, t.Any]]:
        """Parses the name and returns a dictionary if succeeds.  The returned
        dictionary is ensured to have consistently 'logtype' field set, the
        other field names vary depending on the logtype."""
        lsparse = self.parse_lakeshore()
        if lsparse is None:
            raise BFLogParseError(f"Could not parse filename: {self.fname}")
        return lsparse

    @classmethod
    def create(cls, fname: str):
        """fname can be full path or basename."""
        basename = path.basename(fname)
        return cls(basename)


@attr.s(auto_attribs=True, frozen=True)
class LSData:
    channel: int
    dts: DateTime
    values: np.ndarray
    unit: str

    @classmethod
    def from_arrays(
            cls, channel: int, dts: np.ndarray, values: np.ndarray, unit: str):
        return LSData(channel, dts, values, unit)


def read_lakeshore_logfile(fname: str, logtype: BFLogType, channel: int):
    units = {
        BFLogType.Power: 'W',
        BFLogType.Resistance: 'Ω',
        BFLogType.Temperature: 'K'
        }
    unit = units[logtype]

    # For some reason, this is different than the format used in filenames...
    datetimeformat = '%d-%m-%yT%H:%M:%S'

    with open(fname) as fin:
        rdr = csv.reader(fin, delimiter=',', skipinitialspace=True)
        dts_l = []
        values_l = []

        for row in rdr:
            dtstr = "T".join((row[0], row[1]))
            dt = DateTime.strptime(dtstr, datetimeformat)
            value = float(row[2])
            values_l.append(value)
            dts_l.append(dt)

        dts = np.array(dts_l)
        values = np.array(values_l)

        return LSData.from_arrays(channel, dts, values, unit)


def read_bf_logfile(fname):
    """Reads the logfile by determining it's type from the filename, raises
    BFLogParseError if there is a problem.

    Args:
        fname: Path to the log file
    Returns:
        out: Depends on the inferred log file type.

    Return Type:
    1. If it is a LakeShore log file containing P, R or T measurement, a data
    object with fields:
        - channel: for channel number
        - dts: for datetime array
        - values: for measurement values

    """
    # Matches lakeshore log filenames, e.g. "CH1 P 19-01-01.log"
    parser = BFLogNameParser.create(fname)
    parsed = parser.parse()
    plogtype = parsed['logtype']

    BFLT = BFLogType
    if plogtype in [BFLT.Power, BFLT.Resistance, BFLT.Temperature]:
        channel = parsed['channel']
        out = read_lakeshore_logfile(fname, plogtype, channel)
        return out
    else:
        raise NotImplementedError("Currently only lakeshore log"
                                  "files can be read with this function.")

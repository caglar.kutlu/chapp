from io import StringIO
from pathlib import Path
from typing import Protocol

from groundcontrol.declarators import from_json
from xarray import DataArray, Dataset


class PreprocParameters(Protocol):
    delta_ndrop: int

    @classmethod
    def from_json(cls, fp: [str, Path]):
        # We may do sanity check on values here.
        if isinstance(fp, str):
            return from_json(StringIO(fp), cls)
        else:
            return from_json(fp, cls)


class PreprocessResult(Protocol):
    delta: DataArray
    pqa_mu: DataArray
    pbl_mu: DataArray
    gj: DataArray
    tsys: DataArray
    cavds: Dataset
    gmsqr: DataArray
    peps0: DataArray
    plfit: Dataset
    navg: DataArray
    bfield: DataArray
    params: PreprocParameters

    refname: str

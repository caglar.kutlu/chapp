"""Some utils and functions that currently has nowhere to go."""
from itertools import repeat, chain

import numpy as np
from scipy import constants as cnst

def johnson_noise_power(temperature, bandwidth):
    return cnst.Boltzmann*temperature*bandwidth


def repeating_array(arr: np.ndarray, n: int, delay: int = 0):
    """Access the n'th element of `arr` while treating arr as repeating
    forever.
    Args:
        arr: 1-d array.
        n: The n'th elementh of ever repeating `arr`.
        delay: An elementwise delay. E.g. if delay is 5, the 5th element of the
            result is equal to the 0th element.
    
    """
    if n <= len(arr):
        return arr[:n]
    else:
        narr = len(arr)
        q, r = divmod(n, narr) 
        delayed = np.roll(arr, delay)
        repeated = repeat(delayed, q)
        ccarr = np.concatenate(list(chain(repeated, [delayed[:r]])))
        return ccarr[:n]


def split_reverse_combine(sig):
    """Splits the given array in the middle and returns an array with the left
    and right splits interchanged."""
    N = len(sig);
    left = sig[:N//2];
    right = sig[N//2:];
    return np.concatenate((right,left));

import unittest
import sys

import numpy as np

from chapp.sim.processing import rebin_apply, rebin_collect


# binned_statistic raises some warnings, important to see them here.
if not sys.warnoptions:
    import os, warnings
    warnings.simplefilter("default")  # change the filter for this process
    os.environ["PYTHONWARNINGS"] = "default"  # affect subprocesses


class TestRebinning(unittest.TestCase):
    def setUp(self):
        # An example values array
        self.a = np.array([-1, 1, -2, -3, 4])

        # Simple binning
        self.a_centers = np.array([0.5, 1.5, 2.5, 3.5, 4.5])
        self.rebin_edges_simple = np.array([0, 2, 4, 6])
        self.collect_truth_simple = (np.array([-1, 1]),
                                     np.array([-2, -3]),
                                     np.array([4]))

        # Binning with empty bins on right and left
        self.rebin_edges_emptybins = np.array([-2, 0, 2, 4, 6, 8, 10])
        self.collect_truth_emptybins = np.array([
            np.array([]),
            np.array([-1, 1]),
            np.array([-2, -3]),
            np.array([4]),
            np.array([]),
            np.array([])
            ])

        self.rebin_edges_outside = np.array([0, 2, 4])
        self.collect_truth_outside = (np.array([-1, 1]), np.array([-2, -3]))

    def assert_collect_result(self, x, bins, rebin_edges, expected):
        out = rebin_collect(x, bins, rebin_edges)
        for out_bin_contents, expected_bin_contents in zip(out, expected):
            self.assertSequenceEqual(
                    out_bin_contents.tolist(),
                    expected_bin_contents.tolist())

    def test_rebin_collect_simple(self):
        x = self.a
        bins = self.a_centers
        rebin_edges = self.rebin_edges_simple
        expected = self.collect_truth_simple
        self.assert_collect_result(x, bins, rebin_edges, expected)

    def test_rebin_collect_empty_bins(self):
        x = self.a
        bins = self.a_centers
        rebin_edges = self.rebin_edges_emptybins
        expected = self.collect_truth_emptybins
        self.assert_collect_result(x, bins, rebin_edges, expected)

    def test_rebin_apply_sum(self):
        x = self.a
        bins = self.a_centers
        rebin_edges = self.rebin_edges_emptybins
        result = rebin_apply(x, bins, rebin_edges, np.sum)
        truth = np.array([np.sum(c) for c in self.collect_truth_emptybins])
        self.assertSequenceEqual(result.tolist(), truth.tolist())

    def test_rebin_ignore_outside(self):
        x = self.a
        bins = self.a_centers
        rebin_edges = self.rebin_edges_outside
        expected = self.collect_truth_outside
        self.assert_collect_result(x, bins, rebin_edges, expected)

    def test_rebin_ignore_outside_sum(self):
        x = self.a
        bins = self.a_centers
        rebin_edges = self.rebin_edges_outside
        truth = np.array([np.sum(c) for c in self.collect_truth_outside])
        result = rebin_apply(x, bins, rebin_edges, np.sum)
        self.assertSequenceEqual(result.tolist(), truth.tolist())

    def test_rebin_apply_sum_fast(self):
        x = self.a
        bins = self.a_centers
        rebin_edges = self.rebin_edges_emptybins
        result = rebin_apply(x, bins, rebin_edges, np.sum, method='fast')
        truth = np.array([np.sum(c) for c in self.collect_truth_emptybins])
        self.assertSequenceEqual(result.tolist(), truth.tolist())

    def test_rebin_ignore_outside_sum_fast(self):
        x = self.a
        bins = self.a_centers
        rebin_edges = self.rebin_edges_outside
        truth = np.array([np.sum(c) for c in self.collect_truth_outside])
        result = rebin_apply(x, bins, rebin_edges, np.sum, method='fast')
        self.assertSequenceEqual(result.tolist(), truth.tolist())


if __name__ == "__main__":
    unittest.main()

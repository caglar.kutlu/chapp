import numpy as np
from numpy.linalg import inv
from typing import Union, Tuple
from groundcontrol.analysis.nt import noise_psd
import scipy.constants as cnst


_eye2x2 = np.eye(2)
_r1 = np.array([[1, 0], [0, 0]])
_r2 = np.array([[0, 1], [0, 0]])
_r3 = np.array([[0, 0], [1, 0]])
_r4 = np.array([[0, 0], [0, 1]])


def smat2tmat_4x4(mat: np.ndarray):
    # if mat has dim > 2, the last two axes are used as matrix dims
    matI_I = mat[..., :2, :2]
    matI_II = mat[..., :2, 2:4]
    matII_I = mat[..., 2:4, :2]
    matII_II = mat[..., 2:4, 2:4]
    tI_I = matI_II - np.einsum(
        "...ij,...jk,...kl->...il", matI_I, inv(matII_I), matII_II
    )
    tI_II = matI_I @ inv(matII_I)
    tII_I = -inv(matII_I) @ matII_II
    tII_II = inv(matII_I)
    return (
        np.kron(_r1, tI_I)
        + np.kron(_r2, tI_II)
        + np.kron(_r3, tII_I)
        + np.kron(_r4, tII_II)
    )


def smat2tmat_2x2(mat: np.ndarray):
    s11 = mat[..., 0, 0]
    s12 = mat[..., 0, 1]
    s21 = mat[..., 1, 0]
    s22 = mat[..., 1, 1]
    return (1 / s21) * np.array([[s12 * s21 - s11 * s22, s11], [-s22, 1]])


def tmat2smat_2x2(mat: np.ndarray):
    t11 = mat[..., 0, 0]
    t12 = mat[..., 0, 1]
    t21 = mat[..., 1, 0]
    t22 = mat[..., 1, 1]
    return (1 / t22) * np.array([[t12, t11 * t22 - t12 * t21], [1, -t21]])


def tmat2smat_4x4(mat: np.ndarray):
    # if mat has dim > 2, the last two axes are used as matrix dims
    matI_I = mat[..., :2, :2]
    matI_II = mat[..., :2, 2:4]
    matII_I = mat[..., 2:4, :2]
    matII_II = mat[..., 2:4, 2:4]

    sI_I = matI_II @ inv(matII_II)
    sI_II = matI_I - np.einsum(
        "...ij,...jk,...kl->...il", matI_II, inv(matII_II), matII_I
    )
    sII_I = inv(matII_II)
    sII_II = -inv(matII_II) @ matII_I
    return (
        np.kron(_r1, sI_I)
        + np.kron(_r2, sI_II)
        + np.kron(_r3, sII_I)
        + np.kron(_r4, sII_II)
    )


def to_idler_compat_simple(mat: np.ndarray):
    return np.kron(_r1, mat) + np.kron(_r4, np.conjugate(mat))


def smat2gensmat(mat: np.ndarray, portZ: np.ndarray, refZ: float):
    # portZ must have 0 axis as indep dimension
    conj = np.conjugate

    gamma_arr = (portZ - refZ) / (portZ + refZ)
    d_arr = (
        np.abs(1 - conj(gamma_arr)) ** (-1)
        * (1 - gamma_arr)
        * np.sqrt(1 - np.abs(gamma_arr) ** 2)
    )

    if len(portZ.shape) > 1:
        gamma_mat = np.apply_along_axis(np.diag, 1, gamma_arr)
        d_mat = np.apply_along_axis(np.diag, 1, d_arr)
    else:
        gamma_mat = np.diag(gamma_arr)
        d_mat = np.diag(d_arr)

    U = np.eye(d_mat.shape[-1])
    return np.einsum(
        "...ij,...jk,...kl->...il",
        inv(conj(d_mat)),
        mat - conj(gamma_mat),
        inv(U - (gamma_mat @ mat)),
        d_mat,
    )


_M = np.array([[1, 0, 0, 0], [0, 0, 1, 0], [0, 1, 0, 0], [0, 0, 0, 1]])


def swap_basis(mat: np.ndarray):
    return np.einsum("ij,...jk,kl->...il", _M, mat, _M)


def mk_transline_smat(gamma: float, length, rho=0):
    alpha = np.exp(-gamma * length)
    alpha_sqr = alpha ** 2
    rho_sqr = rho ** 2
    s11 = (1 - alpha_sqr) * rho
    s12 = (1 - rho_sqr) * alpha
    s21 = (1 - rho_sqr) * alpha
    s22 = (1 - alpha_sqr) * rho

    retmat = (1 / (1 - rho ** 2 * alpha ** 2)) * np.array([[s11, s12], [s21, s22]])

    # if gamma is an array
    # return np.moveaxis(retmat, -1, 0)

    return retmat


def mk_delay_line(theta: float):
    alpha = np.exp(-1j * theta)

    return np.array([[0, alpha], [alpha, 0]])


def mk_circ_jpa(scirc: np.ndarray, sjpa: np.ndarray, theta):
    """Creates a circulator+JPA s-matrix.  Note that phase due to the transmission line
    connecting the JPA is assumed to not change between idler and signal modes.  This is
    crucial if you are planning to use this model for a wide frequency range.

    Args:
        scirc: 6x6 matrix for the 3 port element representing circulator-like behavior.
        sjpa: 2x2 s-matrix for the JPA.
        theta: Phase change from the circulator to the JPA port.
    """
    sc11 = scirc[..., 0, 0]
    sc12 = scirc[..., 0, 1]
    sc13 = scirc[..., 0, 2]
    sc21 = scirc[..., 1, 0]
    sc22 = scirc[..., 1, 1]
    sc23 = scirc[..., 1, 2]
    sc31 = scirc[..., 2, 0]
    sc32 = scirc[..., 2, 1]
    sc33 = scirc[..., 2, 2]

    sc44 = np.conjugate(scirc[..., 0, 0])
    sc45 = np.conjugate(scirc[..., 0, 1])
    sc46 = np.conjugate(scirc[..., 0, 2])
    sc54 = np.conjugate(scirc[..., 1, 0])
    sc55 = np.conjugate(scirc[..., 1, 1])
    sc56 = np.conjugate(scirc[..., 1, 2])
    sc64 = np.conjugate(scirc[..., 2, 0])
    sc65 = np.conjugate(scirc[..., 2, 1])
    sc66 = np.conjugate(scirc[..., 2, 2])

    gj = sjpa[..., 0, 0]
    gi = sjpa[..., 0, 1]
    gip = sjpa[..., 1, 0]
    gjp = sjpa[..., 1, 1]

    ph = np.exp(2j * theta)
    alpha = gj * gjp - gi * gip

    D1 = -ph * (1 + sc22 * sc55 * alpha) + sc22 * gj + ph ** 2 * sc55 * gjp
    c1 = ph * sc55 * alpha - gj
    c2 = sc22 * alpha - ph * gjp

    smat = np.array(
        [
            [
                sc11 + (sc12 * sc21 * c1 / D1),
                sc13 + (sc12 * sc23 * c1 / D1),
                -(ph * sc12 * sc54 * gi) / D1,
                -(ph * sc12 * sc56 * gi) / D1,
            ],
            [
                sc31 + (sc21 * sc32 * c1) / D1,
                sc33 + (sc23 * sc32 * c1) / D1,
                -ph * sc32 * sc54 * gi / D1,
                -ph * sc32 * sc56 * gi / D1,
            ],
            [
                -ph * sc21 * sc45 * gip / D1,
                -ph * sc23 * sc45 * gip / D1,
                sc44 + sc45 * sc54 * ph * c2 / D1,
                sc46 + sc45 * sc56 * ph * c2 / D1,
            ],
            [
                -ph * sc21 * sc65 * gip / D1,
                -ph * sc23 * sc65 * gip / D1,
                sc64 + sc54 * sc65 * ph * c2 / D1,
                sc66 + sc56 * sc65 * ph * c2 / D1,
            ],
        ]
    )

    if len(smat.shape) > 2:
        return np.moveaxis(smat, -1, 0)
    else:
        return smat


def mk_composite_circ(rc, tcb, tcf):
    return np.array(
        [
            [rc + rc * tcf, tcb ** 2 + rc * tcb * tcf ** 2, tcb ** 2 * tcf],
            [tcf ** 2, rc + 2 * rc * tcb * tcf, tcb ** 2 + rc * tcb * tcf ** 2],
            [tcb * tcf ** 2, tcf ** 2, rc + 2 * rc * tcb * tcf],
        ]
    )


def mk_circ(rc, tcb, tcf):
    return np.array([[rc, tcb, tcf], [tcf, rc, tcb], [tcb, tcf, rc]])


def mk_iso(rc, tcb, tcf):
    return np.array([[rc, tcb], [tcf, rc]])


def mk_jpa_smat(dnu, delta, eps, gamma):
    # quantities except dnu are normalized with gamma
    dnun = dnu / gamma
    denom = (1 - 1j * dnun) ** 2 + delta ** 2 - eps ** 2

    v11 = (delta - 1j) ** 2 - dnun ** 2 - eps ** 2
    v12 = 2 * 1j * eps

    smat = np.array(
        [
            [
                v11 / denom,
                v12 / denom,
            ],
            [
                np.conjugate(v12) / denom,
                np.conjugate(v11) / denom,
            ],
        ]
    )

    try:
        _ = len(dnu)
        return np.moveaxis(smat, -1, 0)
    except TypeError:
        return smat


def calc_gamma_cav(df: np.ndarray, bw0, beta):
    """Args:
    df: f - f0
    bw0: f0/q0
    """
    zc = (1 / beta) * (2j * df / bw0 + 1)  # normalized to z0
    gamma = (zc - 1) / (zc + 1)
    return gamma


def flipconj(arr: np.ndarray):
    return np.conjugate(arr[::-1])


def mk_chain_smat_1(dnu, rc, tcb, tcf, theta0, theta1, theta2, rA, gA, det, eps, jdamp):
    """Computes the s-matrix of the composite chain using the following parameters:

        dnu: Array of f - fp/2.
        rc: Circulator reflection coefficient (see 2).
        tcb: Circulator backward transmission coefficient (see 2).
        tcf: Circulator forward transmission coefficient (see 2).
        theta0: The phase change from the circulator to the JPA (see 1).
        theta1: The phase change from the cavity to the first circulator (see 1).
        theta2: The phase change from the isolator to the amplifier input (see 1).
        rA: S11 of the amplifier.
        gA: S21 of the amplifier.
        det: Normalized JPA detuning parameter (see 3, 4).
        eps: Normalized pump power (see 3).
        jdamp: Damping rate parameter for the JPA.

    Remarks:
        1. Cable length is assumed to be small enough, so a scalar phase can be used.
        2. The circulator is assumed to be perfectly symmetric.  In the case of
           3-junction circulator, all junctions are assumed to be identical.
           For the isolator, the parameters are considered to be the same with
           the circulator.
        3. Detuning is defined as fp/2 - fr0 where fr0 is the bare resonance of
           the JPA.
        4. Normalized parameters are defined as normparam = param/gamma where
           gamma is the damping rate of the JPA.

    """

    s_tl1 = mk_delay_line(theta1)
    s_tl2 = mk_delay_line(theta2)

    s_ccc = mk_composite_circ(rc, tcb, tcf)
    s_i = mk_iso(rc, tcb, tcf)
    s_jpa = mk_jpa_smat(dnu, det, eps, jdamp)
    s_cj = mk_circ_jpa(s_ccc, s_jpa, theta0)
    s_A = np.array([[rA, 0], [gA, 0]])

    t2s = tmat2smat_2x2
    s2t = smat2tmat_2x2
    t2s_ = tmat2smat_4x4
    s2t_ = smat2tmat_4x4
    tics_ = to_idler_compat_simple
    sb_ = swap_basis

    t2_2x2 = np.einsum("...ij,...jk,...kl->...il", s2t(s_i), s2t(s_tl2), s2t(s_A))
    t2_4x4 = s2t_(sb_(tics_(t2s(t2_2x2))))

    t1 = s2t_(sb_(tics_(s_tl1))) @ s2t_(sb_(s_cj))
    t2 = np.broadcast_to(t2_4x4, t1.shape)

    stot = sb_(t2s_(t1 @ t2))

    return stot


def mk_chain_smat_2_w_mirror(
    dnu, rc, tcb, tcf, theta0, theta1, rA, gA, det, eps, jdamp
):
    """Computes the s-matrix of the simplified chain using the following parameters.
        Also returns the s-matrix where JPA is replaced with a short.

        dnu: Array of f - fp/2.
        rc: Circulator reflection coefficient (see 2).
        tcb: Circulator backward transmission coefficient (see 2).
        tcf: Circulator forward transmission coefficient (see 2).
        theta0: The phase change from the circulator to the JPA (see 1).
        theta1: The phase change from the cavity to the first circulator (see 1).
        gA: Gain of an amplifier connected directly at the 3-junction circulator output.
        det: Normalized JPA detuning parameter (see 3, 4).
        eps: Normalized pump power (see 3).
        jdamp: Damping rate parameter for the JPA.

    Remarks:
        1. Cable length is assumed to be small enough, so a scalar phase can be used.
        2. The circulator is assumed to be perfectly symmetric.  In the case of
           3-junction circulator, all junctions are assumed to be identical.
           For the isolator, the parameters are considered to be the same with
           the circulator.
        3. Detuning is defined as fp/2 - fr0 where fr0 is the bare resonance of
           the JPA.
        4. Normalized parameters are defined as normparam = param/gamma where
           gamma is the damping rate of the JPA.

    """
    s_tl1 = mk_delay_line(theta1)

    s_ccc = mk_composite_circ(rc, tcb, tcf)
    s_jpa = mk_jpa_smat(dnu, det, eps, jdamp)
    s_mirr = np.broadcast_to(np.eye(2), (len(dnu), 2, 2))
    s_cj = mk_circ_jpa(s_ccc, s_jpa, theta0)
    s_cmirror = mk_circ_jpa(s_ccc, s_mirr, theta0)

    s_A = np.array([[rA, 0], [gA, 0]])

    t2s = tmat2smat_2x2
    s2t = smat2tmat_2x2
    t2s_ = tmat2smat_4x4
    s2t_ = smat2tmat_4x4
    tics_ = to_idler_compat_simple
    sb_ = swap_basis

    t2_2x2 = s2t(s_A)
    t2_4x4 = s2t_(sb_(tics_(t2s(t2_2x2))))

    t1_1 = s2t_(sb_(tics_(s_tl1)))
    t1_2 = s2t_(sb_(s_cj))
    t1_2m = s2t_(sb_(s_cmirror))

    t1 = t1_1 @ t1_2
    t1m = t1_1 @ t1_2m
    t2 = np.broadcast_to(t2_4x4, t1.shape)

    stot = sb_(t2s_(t1 @ t2))
    stotm = sb_(t2s_(t1m @ t2))

    return stot, stotm


def calc_power_gains(dnu, smat, dfc, bwc, beta) -> Tuple[np.ndarray, np.ndarray]:
    # dnu = f - fsc
    # dfc = f0 - fsc
    # df0 = f - f0 = dnu - dfc
    s11 = smat[..., 0, 0]
    s21 = smat[..., 1, 0]
    s23 = smat[..., 1, 2]
    s33 = smat[..., 2, 2]
    s13 = smat[..., 0, 2]
    s31 = smat[..., 2, 0]

    df = dnu - dfc
    gammas = calc_gamma_cav(df, bwc, beta)
    # ASSUMING dnu has equal number of positive and negative elements.
    gammasp = flipconj(gammas)

    c1 = (s31 / (1 - s33 * gammasp)) * gammasp
    s11_1 = s11 + c1 * s13
    s21_1 = s21 + c1 * s23
    c2 = (s13 / (1 - s11 * gammas)) * gammas
    # s11_2 = s33 + c2 * s31
    s21_2 = s23 + c2 * s21

    M = 1 / (1 - s11_1 * gammas)
    Mp = flipconj(M)

    g1 = np.abs(s21_1 * M) ** 2
    g2 = np.abs(s21_2 * Mp) ** 2

    return g1, g2


def calc_noise_psd_1(dnu, smat, dfc, bwc, beta, fsc, tcav, shemt):
    """
    Args:
        dnu: f - fp/2
        dfc: fcav - fpump/2.
        bwc: fcav/q0cav
        beta: Cavity strong port coupling.
    """
    # dnu = f - fsc
    # dfc = f0 - fsc
    # df0 = f - f0 = dnu - dfc
    s11 = smat[..., 0, 0]
    s21 = smat[..., 1, 0]
    s23 = smat[..., 1, 2]
    s33 = smat[..., 2, 2]
    s13 = smat[..., 0, 2]
    s31 = smat[..., 2, 0]

    df = dnu - dfc
    gammas = calc_gamma_cav(df, bwc, beta)
    # ASSUMING dnu has equal number of positive and negative elements.
    gammasp = flipconj(gammas)

    c1 = (s31 / (1 - s33 * gammasp)) * gammasp
    s11_1 = s11 + c1 * s13
    s21_1 = s21 + c1 * s23
    s21_2 = s23 + (s13 / (1 - s11 * gammas)) * gammas * s21

    M = 1 / (1 - s11_1 * gammas)
    Mp = flipconj(M)

    scav = noise_psd(tcav, fsc)
    out = (np.abs(s21_1 * M) ** 2 + np.abs(s21_2 * Mp) ** 2) * scav + shemt
    return out

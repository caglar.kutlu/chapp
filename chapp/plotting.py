"""A module to hold plotting related functions.  The stuff here are subject to
change on a regular basis as good interface for graphical programming is hard
to get right.
"""
import typing as t
from fractions import Fraction

import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.integrate import trapz
import numpy as np


Array = np.ndarray
Axes = mpl.axes.Axes


def parse_fraction(s: str):
    """Parses a string in the form of "3:1" to a fraction"""
    try:
        m, n = map(int, s.split(':'))
    except ValueError as e:
        raise ValueError(f"Couldn't parse {s} to Fraction.") from e
    return Fraction(m, n)


def signoiseplot(
        x: np.ndarray,
        y: np.ndarray,
        bins: int = 50,
        noise: t.Optional[Array] = None,
        width_ratio: t.Union[Fraction, str] = "3:1",
        xlim: t.Optional[t.Tuple[float, float]] = None,
        ylim: t.Optional[t.Tuple[float, float]] = None,
        xlabel: str = "",
        ylabel: str = "",
        title: str = "",
        figsize: t.Optional[t.Tuple[float, float]] = None) -> t.List[Axes]:
    """A plot with the signal plot on the left and the histogram of it on the
    right.

    Args:
        x: 1-d array of variables.
        y: 1-d array of measurement samples.
        bins: Number of bins to use when making the histogram. 
        noise: Optional 1-d array of the same length as y, which corresponds to
            noise without any signal content.  Noise is assumed to be additive.
        width_ratio: Ratio of left pane to right pane as a fraction. Can be
            given as a proper `Fraction` or a `str` with the form "a:b".
        xlim: Limits of the x axis on the left plot (not the histogram!).
            Whole range is used if `None`. Defaults to `None`.
        ylim: Limits of the plot and the domain of the histogram.  Guessed if
            `None`. Defaults to `None`.
        xlabel: The x-axis label for the left plot.
        ylabel: The y-axis label for the left plot.
        title: Title of the whole figure.

    Returns:
        fig: Figure object.
        axs: List of axes.  0th element is the left one.

    """
    if isinstance(width_ratio, str):
        width_ratio = parse_fraction(width_ratio)
    
    left_w = width_ratio.numerator
    right_w = width_ratio.denominator
    tot_w = left_w + right_w

    fig = plt.figure(figsize=figsize)
    ax_left = plt.subplot2grid((1, tot_w), (0, 0), colspan=left_w)
    ax_right = plt.subplot2grid((1, tot_w), (0, left_w), colspan=right_w)
    fig.subplots_adjust(wspace=0)

    ax_left.plot(x, y)
    ax_left.set_ylim(ylim)
    
    y_hist, edges, _ = ax_right.hist(
        y, bins=bins, range=ylim, orientation='horizontal',
        histtype='stepfilled')
    binwidth = edges[1] - edges[0]

    ax_right.set_ylim(ylim)
    
    if noise is not None:
        noise_hist, _, _ = ax_right.hist(
            noise, bins=edges, range=ylim, orientation='horizontal',
            color='k', histtype='stepfilled', fill=False, linewidth=2,
            edgecolor='k')
    else: 
        # If noise isn't given, use y assuming it's mostly noise.
        noise_hist = y_hist
        noise = y

    noise_sigma = noise.std()
    # fit may fail if values are too small (idk why.), hence /.std()
    mu_fit, sigma_fit = map(
        lambda k: k*noise_sigma,
        norm.fit(noise/noise_sigma))

    hist_domain = sorted(y)
    hist_energy = trapz(noise_hist, edges[:-1] + binwidth/2)
    hist_fit = hist_energy*norm.pdf(
        hist_domain, loc=mu_fit,
        scale=sigma_fit)
    ax_right.plot(hist_fit, hist_domain, 'r')
    ax_right.set_xlim((0.1, noise_hist.max()*3/2))
    ax_right.set_xscale('log')

    text = f"$\\mu = {mu_fit:.3e}$\n$\\sigma = {sigma_fit:.3e}$"
    ax_right.text(
        0.95, 0.95,
        text, horizontalalignment='right', verticalalignment='top',
        transform=ax_right.transAxes,
        bbox={'ec': 'k', 'fill': True, 'fc': 'w', 'alpha': 0.75})

    ax_right.yaxis.tick_right()
    ax_right.xaxis.tick_top()
    ax_right.yaxis.set_offset_position('left')

    ax_left.grid()
    ax_right.grid()

    ax_left.set_xlabel(xlabel)
    ax_left.set_ylabel(ylabel)
    ax_right.set_xlabel("Count")
    return fig, [ax_left, ax_right]



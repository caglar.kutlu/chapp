import numpy as np
import xarray as xr
from xarray import DataArray as DA
from numpy.random import default_rng, Generator
from chapp.physics import DEFAULT_AXIONDM_CONTEXT as ACTX

# import os


shapefa = 5.9e9


wflen = 1001
waveform_step = 62.5

wfdnu = ACTX.mkwfdomain(wflen, waveform_step)

waveform = ACTX.get_waveform_labframe(shapefa, wfdnu)


# wfpath = os.environ["WFPATH"]
# wfda = xr.open_dataarray(wfpath).sel(nu_a=shapefa, dnu_a=0, method="nearest")
# waveform = wfda.data
# waveform_step = wfda.df[[0, 1]].diff('df').item()


rng: Generator = default_rng(1234)


ndnu = 16001
assert (ndnu % 2) == 1
ngrunstep = 1000

halfspan = 500e3
dnu_, step = np.linspace(-halfspan, halfspan, ndnu, retstep=True)
print(step)
assert step == waveform_step
dnu = DA(dnu_, dims="dnu", name="dnu", coords={"dnu": dnu_})
grunstep = DA(np.arange(ngrunstep), dims="grunstep", name="grunstep")


randomized = ((5.85e9 + grunstep * rng.uniform(15e3, 25e3, size=ngrunstep)) // 250) * 250
regular = 5.85e9 + grunstep * 20e3 + 5e3
fsc = DA(
    regular,
    dims="grunstep",
    name="fsc",
    coords={"grunstep": grunstep},
)
fbin = fsc + dnu


faxion = 5.86e9

maskda = np.abs(fbin - faxion) < step
idx = np.argwhere(maskda.data)

sigmat = np.zeros_like(fbin.data)

# Can np.put_along_axis be used to eliminate this loop?
for grsid, dnuid in idx:
    indices = np.arange(dnuid, dnuid + len(waveform))
    np.put(sigmat[grsid], indices, waveform, mode="clip")


sigda = xr.DataArray(
    sigmat,
    dims=("grunstep", "dnu"),
    coords={"grunstep": ("grunstep", grunstep.data), "dnu": ("dnu", dnu.data)},
)
sigda.coords["fsc"] = ("grunstep", fsc.data)
sigda.coords["fbin"] = (["grunstep", "dnu"], fbin.data)


snrmat = np.ones_like(sigmat)
snrda = sigda.copy(deep=False, data=snrmat).rename("snr")


dsigda = (
    (sigda * snrda).pipe(lambda da: da + da.assign_coords(dnu=-da.dnu)).rename("sig")
)


ds = xr.merge([dsigda, snrda, sigda.rename("sig_s")])


def plotfew(grsid=range(21, 30, 2)):
    grsid = list(iter(grsid))
    ds.sig.sel(grunstep=idx[grsid, 0]).plot(x="dnu", hue="grunstep")

"""This module provides tools for emulating the behavior of various kinds of
spectrum analyzers.

TODO:
    - Define a proper base for Knobs, don't create a lot of Knobs hierarchy.
      Make it so that every knob will provide a core set of variables to
      spectrum makers.  These are probably nperseg, nsamples etc.
    - Move a lot of stuff from FSVSpectrumMaker to the base OR make an
      intermediate maker that implements FFTSpectrumMaker.
    - Find a way to make Knobs contribute to the initialization not takeover as
      of now. The idea is to write Knobs of spectrum analyzers independent of
      their functions.  Knobs provide parameters for the spectrum analyzer.
    - When you think of a SpectrumMaker inheriting Knobs, it sounds weird.
      However, if you think of Knobs as a mixin, we are essentially adding
      "Knobability" to our classes.  Maybe a better name?
"""
import typing as t

import attr
import numpy as np

from chapp.sim.windowing import WindowFunc, windows, get_window
from chapp.sim.processing import spectraggram, rebin_apply, rebin_collect
from chapp.sim.sources import Source, thermal_noise


@attr.s(auto_attribs=True)
class Knobs:
    """This defines some core parameters found in most spectrum analyzers.

    TODO: Figure out how to use validators for these attributes.

    Args:
        rbw(float): Resolution bandwidth.
        swt(float): Sweep time.
        sweep_points(int): Sweep points.
        span(float): Span.
        center_frequency(float): Center frequency.
        start_frequency(float): Start frequency of the sweep.
        stop_frequency(float): Stop frequency of the sweep.

    """
    _rbw: float
    _swt: float  # intentionally not exposed via property
    _sweep_points: float
    _start_frequency: float
    _stop_frequency: float
    _center_frequency: float
    _span: float

    @property
    def rbw(self):
        return self._rbw

    @rbw.setter
    def rbw(self, value):
        newswt = self.calc_swt(value)
        self._swt = newswt
        self._rbw = value

    @property
    def sweep_points(self):
        return self._sweep_points

    @sweep_points.setter
    def sweep_points(self, value):
        self._sweep_points = value

    @property
    def start_frequency(self):
        return self._start_frequency

    @start_frequency.setter
    def start_frequency(self, value):
        newcenter = self.calc_center(value, self.stop_frequency)
        newspan = self.calc_span(value, self.stop_frequency)
        self._center_frequency = newcenter
        self._span = newspan
        self._start_frequency = value

    @property
    def stop_frequency(self):
        return self._stop_frequency

    @stop_frequency.setter
    def stop_frequency(self, value):
        newcenter = self.calc_center(self.start_frequency, value)
        newspan = self.calc_span(self.start_frequency, value)
        self._center_frequency = newcenter
        self._span = newspan
        self._stop_frequency = value

    @property
    def center_frequency(self):
        return self._center_frequency

    @center_frequency.setter
    def center_frequency(self, value):
        newstart = self.calc_start(value, self.span)
        newstop = self.calc_stop(value, self.span)
        self._start_frequency = newstart
        self._stop_frequency = newstop
        self._center_frequency = value

    @property
    def span(self):
        return self._span

    @span.setter
    def span(self, value):
        newstart = self.calc_start(self.center_frequency, value)
        newstop = self.calc_stop(self.center_frequency, value)
        self._start_frequency = newstart
        self._stop_frequency = newstop
        self._span = value

    @property
    def bin_width(self):
        return self.span/self.sweep_points

    def calc_swt(self, rbw):
        """Simplest case for 'no windowing'."""
        return 1/rbw

    def calc_center(self, start, stop):
        return (stop+start)/2

    def calc_span(self, start, stop):
        return stop-start

    def calc_start(self, center, span):
        return center-span/2

    def calc_stop(self, center, span):
        return center+span/2

    @classmethod
    def create_from_start_stop(cls, rbw: float = 100.0,
                               swt: float = 10e-3,
                               sweep_points: int = 1000,
                               start_frequency: float = 0,
                               stop_frequency: float = 100e3,
                               **kwargs):  # for subclasses
        """Generate self-consistent knobs using start and stop frequencies."""
        obj = cls(rbw=rbw, swt=swt, sweep_points=sweep_points,
                  start_frequency=start_frequency,
                  stop_frequency=stop_frequency, center_frequency=0, span=0,
                  **kwargs)
        center = obj.calc_center(start_frequency, stop_frequency)
        span = obj.calc_span(start_frequency, stop_frequency)
        obj._center_frequency = center
        obj._span = span
        return obj

    @classmethod
    def create_from_center_span(cls, rbw: float = 100.0,
                                swt: float = 10e-3,
                                sweep_points: int = 1000,
                                center_frequency: float = 50e3,
                                span: float = 100e3,
                                **kwargs):  # for subclasses
        """Generate self-consistent knobs using center frequency and span."""
        obj = cls(rbw=rbw, swt=swt, sweep_points=sweep_points,
                  start_frequency=0, stop_frequency=0,
                  center_frequency=center_frequency, span=span,
                  **kwargs)
        start = obj.calc_start(center_frequency, span)
        stop = obj.calc_stop(center_frequency, span)
        obj._start_frequency = start
        obj._stop_frequency = stop
        return obj

    @classmethod
    def create(
            cls,
            rbw: float = 100.0,
            swt: float = 10e-3,
            sweep_points: int = 1000,
            start_frequency: t.Optional[float] = None,
            stop_frequency: t.Optional[float] = None,
            center_frequency: t.Optional[float] = None,
            span: t.Optional[float] = None):
        """Generate self-consistent knobs.  Either provide (start,stop) or
        (center,span). """

        # Defaults
        span_default = 100e3

        i, f, c, s = start_frequency, stop_frequency, center_frequency, span

        if (i, f, c, s) == (None, None, None, None):
            return cls.create_from_center_span(
                    rbw, swt, sweep_points)
        elif (i, f, c, s) == (i, None, None, None):
            stop = i+span_default
            return cls.create_from_start_stop(
                    rbw, swt, sweep_points,
                    start_frequency=i, stop_frequency=stop)
        elif (i, f, c, s) == (None, f, None, None):
            start = f-span_default
            return cls.create_from_start_stop(
                    rbw, swt, sweep_points,
                    start_frequency=start, stop_frequency=f)
        elif (i, f, c, s) == (None, None, c, None):
            return cls.create_from_center_span(
                    rbw, swt, sweep_points,
                    center_frequency=c, span=span_default)
        elif (i, f, c, s) == (None, None, None, s):
            return cls.create_from_center_span(
                    rbw, swt, sweep_points,
                    center_frequency=center_frequency, span=s)
        elif (i, f, c, s) == (i, f, None, None):
            return cls.create_from_start_stop(
                    rbw, swt, sweep_points,
                    start_frequency=i, stop_frequency=f)
        elif (i, f, c, s) == (i, None, None, s):
            stop = i+span_default
            return cls.create_from_start_stop(
                    rbw, swt, sweep_points,
                    start_frequency=i, stop_frequency=stop)
        elif (i, f, c, s) == (None, None, c, s):
            return cls.create_from_center_span(
                    rbw, swt, sweep_points,
                    center_frequency=c, span=s)
        else:
            raise ValueError("Either provide (start, stop) or (center, span)"
                             "or none of them.")


@attr.s(auto_attribs=True)
class ModernKnobs(Knobs):
    """This class implements some controls and parameters that may be found on
    modern FFT spectrum analyzers.  If you wanna add a feature that is generic
    enough, add here rather than specific implementation.

    Attributes:
        window:  Windowing function, a WindowFunc instance
        overlap:  Overlapping as a ratio of overlapping to total bin size in
            segments.
        is_auto_swt:  Is the instrument set to autosweep time setting or
            not.
        is_auto_overlap:  If set to True, recommended overlap value (rov) for
            the given window is used.

    """
    _window: WindowFunc
    overlap: float  # ratio of overlap
    _is_auto_swt: bool
    _is_auto_overlap: bool

    @property
    def window(self):
        return self._window

    @window.setter
    def window(self, window: t.Union[WindowFunc, str]):
        if isinstance(window, str):
            self._window = get_window(window)
        self._window = window
        if self._is_auto_overlap:
            self.overlap = window.rov
        if self._is_auto_swt:
            self._swt = self.calc_swt(self.rbw)

    def set_auto_swt(self, isauto: bool):
        self._is_auto_swt = isauto
        if isauto:
            self._swt = self.calc_swt(self.rbw)

    def set_auto_overlap(self, isauto: bool):
        self._is_auto_overlap = isauto
        if isauto:
            self.overlap = self.window.rov

    @property
    def rbw(self):
        return self._rbw

    @rbw.setter
    def rbw(self, value):
        self._rbw = value
        if self._is_auto_swt:
            newswt = self.calc_swt(value)
            self._swt = newswt

    @property
    def swt(self):
        return self._swt

    @swt.setter
    def swt(self, value):
        minswt = self.calc_swt(self.rbw)
        # If swt is lower than value, set to minimum
        self._swt = max(minswt, value)
        self.set_auto_swt(False)

    def calc_swt(self, rbw: float):
        """Returns the minimum sweep time required to have 3dB bandwidth of
        `rbw`"""
        # Can't resolve bins lower than 1.
        n3dbbw = max(self.window.n3dbbw, 1)
        swt = n3dbbw/rbw
        return swt

    @classmethod
    def create(
            cls,
            rbw: float = 100.0,
            swt: t.Optional[float] = None,  # if None, set auto
            sweep_points: int = 1000,
            start_frequency: t.Optional[float] = None,
            stop_frequency: t.Optional[float] = None,
            center_frequency: t.Optional[float] = None,
            span: t.Optional[float] = None,
            window=windows.Rectangular,
            overlap=0,
            is_auto_swt=True,
            is_auto_overlap=True):
        """Generate self-consistent knobs.  Either provide (start,stop) or
        (center,span).

        TODO: Finish up writing conditions for start,stop,center,span
        inputs. (write tests first)

        """

        extraargs = dict(window=window,
                         overlap=overlap, is_auto_swt=is_auto_swt,
                         is_auto_overlap=is_auto_overlap)

        # Defaults
        span_default = 100e3

        i, f, c, s = start_frequency, stop_frequency, center_frequency, span

        if (i, f, c, s) == (None, None, None, None):
            obj = cls.create_from_center_span(
                    rbw, swt, sweep_points,
                    **extraargs)
        elif (i, f, c, s) == (i, None, None, None):
            stop = i+span_default
            obj = cls.create_from_start_stop(
                    rbw, swt, sweep_points,
                    start_frequency=i, stop_frequency=stop,
                    **extraargs)
        elif (i, f, c, s) == (None, f, None, None):
            start = f-span_default
            obj = cls.create_from_start_stop(
                    rbw, swt, sweep_points,
                    start_frequency=start, stop_frequency=f,
                    **extraargs)
        elif (i, f, c, s) == (None, None, c, None):
            obj = cls.create_from_center_span(
                    rbw, swt, sweep_points,
                    center_frequency=c, span=span_default,
                    **extraargs)
        elif (i, f, c, s) == (None, None, None, s):
            obj = cls.create_from_center_span(
                    rbw, swt, sweep_points,
                    center_frequency=center_frequency, span=s,
                    **extraargs)
        elif (i, f, c, s) == (i, f, None, None):
            obj = cls.create_from_start_stop(
                    rbw, swt, sweep_points,
                    start_frequency=i, stop_frequency=f,
                    **extraargs)
        elif (i, f, c, s) == (i, None, None, s):
            obj = cls.create_from_start_stop(
                    rbw, swt, sweep_points,
                    start_frequency=i, stop_frequency=i+s, **extraargs)
        elif (i, f, c, s) == (None, None, c, s):
            obj = cls.create_from_center_span(
                    rbw, swt, sweep_points,
                    center_frequency=c, span=s,
                    **extraargs)
        else:
            raise ValueError("Either provide (start, stop) or (center, span)"
                             "or none of them.")
        obj.set_auto_overlap(is_auto_overlap)
        if swt is None:
            obj.set_auto_swt(True)
        else:
            obj.set_auto_swt(False)

        return obj


@attr.s
class BasebandKnobs(ModernKnobs):
    @property
    def start_frequency(self):
        return self._start_frequency

    @property
    def stop_frequency(self):
        return self._stop_frequency

    @property
    def center_frequency(self):
        return self._center_frequency

    def calc_start(self, center, span):
        return -self.rbw/2

    def calc_stop(self, center, span):
        start = self.calc_start(center, span)
        stop = start + span
        return stop

    @classmethod
    def create(
            cls,
            rbw: float = 100.0,
            swt: t.Optional[float] = None,  # if None, set auto
            sweep_points: int = 1000,
            span: t.Optional[float] = 100e3,
            window=windows.Rectangular,
            overlap=0,
            is_auto_swt=True,
            is_auto_overlap=False):
        """Creates the knobs object for baseband spectrum maker.

        Note:
            - Start and stop frequencies correspond to the edges of the bins.
              Therefore, if the first bin corresponds to DC, the
              start_frequency will be equal to `0 - bin_width/2`.
        """
        bin_width = span/sweep_points
        start = 0 - bin_width/2
        stop = start + span
        obj = cls(
                rbw=rbw, swt=swt, sweep_points=sweep_points, span=span,
                window=window, overlap=overlap, is_auto_swt=is_auto_swt,
                is_auto_overlap=is_auto_overlap,
                start_frequency=start, stop_frequency=stop,
                center_frequency=span/2)

        obj.set_auto_overlap(is_auto_overlap)
        if swt is None:
            obj.set_auto_swt(True)
        else:
            obj.set_auto_swt(False)

        return obj


@attr.s(auto_attribs=True, frozen=True)
class Detector:
    """Class that defines the type of Detectors that are used to emulate
    spectrum analyzer trace behavior.  The detectors are, by definition, to be
    applied on the voltage values, rather than power values.

    Attributes:
        name: Name of the detector.
        aggfun:  A function that returns a single value from a 1-D array.

    """
    name: str
    aggfun: t.Callable[[np.ndarray], np.ndarray]

    def apply(self, x: np.ndarray) -> np.ndarray:
        return self.aggfun


_detectors = {
    "MaxPeak": Detector("MaxPeak", np.max),
    "MinPeak": Detector("MinPeak", np.min),
    "FSVSample": Detector("FSVSample", lambda ar: ar[0]),
    "TrueSample": Detector("TrueSample", np.random.choice),
    "Sample": Detector("Sample", np.random.choice),
    "RMS": Detector("RMS", lambda x: np.sqrt(np.mean(np.power(x, 2))))
    }


class DetectorNotFoundError(Exception):
    pass


@attr.s(auto_attribs=True)
class BasebandSpectrumMaker:
    """This is a spectrum maker class that makes the spectrum from 0 frequency
    to `span`.  Thus, the `start_frequency` and `stop_frequency` parameters are
    ignored when performing the spectral analysis, and only used in the
    returning frequency array.  In other words, this class DO NOT emulate
    downconversion.

    Notes:
        - No `trace` functionality is added here.
        - The FFT, by definition can not provide consistent values for DC and
          NYQUIST frequency terms.  So by default the result of `analyze` will
          drop these frequency bins.  If you want to keep them, change these
          attributes.
        - The definition of `span` is the frequency range between the
          right-edge
          of last bin and the left-edge of the first bin.
        - Returned frequency values by generate are the `center` frequencies
          for the bins.

    Attributes:
        input:  A `Source` instance.
        knobs:  A `Knobs` instance to get the required parameters for spectral
            analysis.
        detector:  Detector type used for the spectral analysis.
        drop_dc:  Whether or not analyze drops the bin corresponding to 0
            frequency.
        pad_coeff:  Determines the amount of padding to be applied.  For
            example if this is given as 3, 2*nperseg 0 will be appended to the
            segments before computing FFT's.

    Examples:
        >>> import matplotlib.pyplot as plt
        >>> from chapp.sim.sources import sine, thermal_noise
        >>> from chapp.sim.processing import welch
        >>> power_rms = 1e-3
        >>> sig = sine(amplitude=np.sqrt(2*50*power_rms), frequency=20e3, phase=0)
        >>> sm = BasebandSpectrumMaker.create()
        >>> sm.input = sig + thermal_noise(temperature=1e16)
        >>> sm.knobs.window = windows.Rectangular
        >>> sm.detector = _detectors['RMS']
        >>> sm.drop_dc = False
        >>> sm.knobs.swt = 0.1
        >>> sm.pad_coeff = 1
        >>> samples = sm.sample()
        >>> f, s = sm.analyze(samples)
        >>> _ = plt.plot(f, 10*np.log10(s) + 30, label='BasebandSpectrumMaker+RMS')
        >>> fw, w = welch(samples, sm.sample_rate, window=sm.knobs.window, nperseg=sm.nperseg, noverlap=sm.noverlap, nfft=sm.nfft, scaling='spectrum')
        >>> fw, w = (fw[1:], w[1:]) if sm.drop_dc else (fw, w)
        >>> _ = plt.plot(fw, 10*np.log10(w/50) + 30, label='Welch')
        >>> _ = plt.ticklabel_format(scilimits=(-3,3), useMathText=True, useOffset=False)
        >>> _ = plt.xlabel("Frequency [Hz]")
        >>> _ = plt.ylabel("Power [dBm]")
        >>> _ = plt.legend()
        >>> _ = plt.show()

    """  # noqa: E501
    _input_impedance: float
    input: Source
    knobs: BasebandKnobs
    _detector: Detector
    drop_dc: bool
    pad_coeff: float

    # Available detectors
    detectors: t.ClassVar[t.Dict[str, Detector]] = _detectors

    @property
    def detector(self):
        return self._detector

    @detector.setter
    def detector(self, detector: t.Union[Detector, str]):
        if isinstance(detector, str):
            try:
                detector = self.detectors[detector]
            except KeyError as exc:
                raise DetectorNotFoundError(
                    f"Detector {detector} is not available.") from exc
        self._detector = detector

    @property
    def sample_rate(self):
        """Sample rate is directly computed from the span of the measurement.
        """
        return self.knobs.span*2

    @property
    def nsamples(self):
        """Returns the total sample count required to obtain a spectrum with the
        current `knobs` parameters."""
        knobs = self.knobs
        return int(np.ceil(knobs.swt * self.sample_rate))

    @property
    def nperseg(self):
        knobs = self.knobs
        segment_time = knobs.calc_swt(knobs.rbw)
        return int(np.ceil(segment_time*self.sample_rate))

    @property
    def noverlap(self):
        knobs = self.knobs
        overlap_ratio = knobs.overlap
        nperseg = self.nperseg
        noverlap = int(np.floor(nperseg*overlap_ratio))
        return noverlap

    @property
    def nfft(self):
        return int(np.floor(self.nperseg * self.pad_coeff))

    @property
    def internal_npoints(self):
        """Number of points used to represent the whole one-sided spectrum
        internally.  The convention here is used by np.fft.rfft as well."""
        nfft = self.nfft
        if nfft % 2 == 0:
            return nfft/2 + 1
        else:
            return (nfft+1)/2

    @property
    def internal_bin_width(self):
        return self.knobs.span/self.internal_npoints

    def sample(self):
        n = self.nsamples
        fs = self.sample_rate
        samples = self.input.sample(n, fs)
        return samples

    def analyze(self, samples: np.ndarray):
        """Performs the spectral analysis on input 1-D time domain array.

        Detection:
            - Detection in FFT spectrum analyzers is 2-fold:
            1. If the sweep time is longer than the one required for a single
            FFT, multiple spectra are computed.  These multiple spectrum bins
            are first aggregated via the detector.
            2. The original FFT bins are different than the sweep points shown
            on a spectrum analyzer.  The internal bins are combined to equal
            the amount of sweep points via the detector type.

        """
        # mode is set to magnitude
        f, v = spectraggram(
                samples, self.sample_rate,
                window=self.knobs.window,
                nperseg=self.nperseg, noverlap=self.noverlap,
                nfft=self.nfft, scaling='spectrum', mode='magnitude',
                aggfun=self.detector.aggfun)
        # dropping the bin at exactly nyquist from further processing.
        f, v = self.detect_view(f, v)
        s = np.abs(v**2)/self._input_impedance
        # TODO: CHECK THE CORRECTNESS OF THIS!
        s[1:] *= 2
        if self.drop_dc:
            f = f[1:]
            s = s[1:]
        return f, s

    def detect_view(self, f: np.ndarray, v: np.ndarray):
        """Performs the detector on the captured spectrum.

        Length of the output is determined by sweep_points.

        Args:
            f:  Frequency array corresponding to centers of bins in the
                spectrum.
            v:  Magnitude of the DFT computation, proportional to sqrt(power).
                points:

        Returns:
            f_out:  Frequency values corresponding to sweep points and to the
                values after detector.
            v_out:  Values after detector.

        """
        sweep_points = self.knobs.sweep_points
        rebin_edges, binwidth = self.calc_view_frequency_edges(sweep_points)
        func = self.detector.aggfun
        detected = rebin_apply(v, f, rebin_edges, func, method='fast')
        f_out = rebin_edges[:-1]+binwidth/2
        v_out = detected

        return f_out, v_out

    def calc_view_frequency_edges(self, npoints=None):
        """Constructs an array of frequency edge values for bins.
        Note that some of the implicit assumptions made here may not hold for
        some devices, some tweaking may be necessary.

        """
        knobs = self.knobs
        npoints = knobs.sweep_points if npoints is None else npoints
        # It's important to differentiate between bin centers and edges.
        start_bin = knobs.start_frequency + knobs.rbw/2
        # stop_bin = knobs.stop_frequency - knobs.rbw/2

        binwidth = knobs.span/knobs.sweep_points
        start = start_bin - binwidth/2
        # stop = stop_bin + binwidth/2
        # edges = np.linspace(start, stop, npoints+1)
        edges = start + np.arange(npoints+1)*binwidth
        return edges, binwidth

    def generate(self):
        samples = self.sample()
        f, s = self.analyze(samples)
        return f, s

    @classmethod
    def from_knobs(
            cls,
            knobs: BasebandKnobs,
            detector: t.Union[str, Detector] = "Sample",
            input: t.Optional[Source] = None,
            input_impedance: float = 50, drop_dc=True,
            pad_coeff=1):
        """Creates a `SpectrumMaker` instance using an existing Knobs
        instance."""
        # If no input is provided, use a thermal source.
        if input is None:
            input = thermal_noise(  # pylint: disable=no-value-for-parameter
                temperature=290.0,
                resistance=input_impedance
                )

        # Temporarily setting this
        tmp_detector = _detectors['Sample']
        obj = cls(
            input_impedance, input, knobs,
            tmp_detector, drop_dc, pad_coeff)

        # invoking setter
        obj.detector = detector
        return obj

    @classmethod
    def create(
            cls,
            rbw: float = 100.0,
            swt: t.Optional[float] = None,
            sweep_points: int = 1000,
            span: t.Optional[float] = 100e3,
            window: WindowFunc = windows.Rectangular,
            overlap: float = 0,
            is_auto_swt: bool = True,
            is_auto_overlap: bool = True,
            detector: t.Union[str, Detector] = "Sample",
            input: t.Optional[Source] = None,
            input_impedance: float = 50,
            drop_dc: bool = True,
            pad_coeff: float = 1):
        """Create a SpectrumMaker instance from the given arguments. """

        knobs = BasebandKnobs.create(
            rbw=rbw, swt=swt,
            sweep_points=sweep_points,
            span=span, window=window, overlap=overlap,
            is_auto_swt=is_auto_swt,
            is_auto_overlap=is_auto_overlap)
        return cls.from_knobs(
            knobs=knobs, detector=detector, input=input,
            input_impedance=input_impedance, drop_dc=drop_dc,
            pad_coeff=pad_coeff)

    @classmethod
    def fsvlike(
            cls,
            rbw: float = 100.0,
            swt: t.Optional[float] = None,
            sweep_points: int = 1000,
            span: t.Optional[float] = 100e3,
            detector: t.Union[str, Detector] = "Sample"):
        """Creates the instance with the options similar mirroring those found
        in R&S FSV series analyzers.
        """
        input_impedance: float = 50
        input: t.Optional[Source] = None
        drop_dc = True
        is_auto_overlap: bool = False
        is_auto_swt: bool = True
        overlap: float = 0.5
        window = get_window('BH92')
        pad_coeff = 4

        detectors = _detectors.copy()
        detectors['Sample'] = detectors['FSVSample']

        obj = cls.create(
            rbw, swt, sweep_points, span,
            window, overlap, is_auto_swt, is_auto_overlap,
            detector, input, input_impedance, drop_dc, pad_coeff)
        
        obj.detectors = detectors
        return obj

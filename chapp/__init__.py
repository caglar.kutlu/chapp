__version__ = "0.1"

from .kernel import kernel_from_bins, correlate
from chapp.preprocessors import ppopen

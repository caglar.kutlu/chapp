"""Noise Power versus Sweep Time
This simulation is intended to replicate the behaviour of R&S FSV series
spectrum analyzer when used in the RMS detector mode.

"""

import numpy as np
from matplotlib import pyplot as plt

from chapp.sim import BasebandSpectrumMaker, detectors, windows,\
        thermal_noise

def watt2dbm(w): return 10*np.log10(w)+30


OPTS = dict(
    span=100e3,
    rbw=100,
    sweep_points=1000,
    is_auto_swt=True,
    is_auto_overlap=True,
    window=windows.BH92,
    detector=detectors['RMS'],
    input_impedance=50,
    drop_dc=True
    )

SM = BasebandSpectrumMaker.create(**OPTS)

# Measurement Sweep Parameters
START = 0.02
STOP = 1
N = 5

# Noise Parameters
TEMPERATURE = 290
RESISTANCE = 50

# pylint: disable=E1120
SRC = thermal_noise(
    temperature=TEMPERATURE,
    resistance=RESISTANCE)

SM.input = SRC

SWTS = np.linspace(START, STOP, N)

# pylint: disable=C0103
fig, ax = plt.subplots()
fdom = np.zeros(SM.knobs.sweep_points)
for i, swt in enumerate(SWTS):
    SM.knobs.swt = swt
    f, s = SM.generate()
    text = f"SWT = {swt} s"
    if not i:
        fdom = f
    else:
        fdom += f.max() + SM.knobs.rbw
    ax.plot(fdom, watt2dbm(s))
    ylim = ax.get_ylim()
    ypos = ylim[0] + (ylim[1] - ylim[0])*0.8
    ax.text(
        np.median(f) + fdom[0],
        ypos, text, fontsize=10, ha='center', va='bottom',
        bbox={'ec': 'k', 'fc': 'w'})

plt.show()

"""Effective Number of Averages vs Sweep Time

Due to the effect of windowing and RMS detection along with the extra padding,
the usual average number estimator:  (mu/std)^2 overestimates the average
number.  This example demonstrates that result.
"""
import numpy as np
import sys
from matplotlib import pyplot as plt

from chapp.sim import BasebandSpectrumMaker, detectors, windows,\
        thermal_noise
from chapp.io import read_csv, write_csv


SEED = 1234
np.random.seed(SEED)


def watt2dbm(w): return 10*np.log10(w)+30


SM = BasebandSpectrumMaker.fsvlike(
    rbw=100, sweep_points=1000, span=100e3, detector="RMS")


# Measurement Sweep Parameters
SWT_START = 0.1
SWT_STOP = 0.5
SWT_N = 10
SWT_REPEAT = 1

SWEEP_POINTS = [1000, 1896, 7585]

# Noise Parameters
TEMPERATURE = 290
RESISTANCE = 50

# pylint: disable=E1120
SRC = thermal_noise(
    temperature=TEMPERATURE,
    resistance=RESISTANCE)

SM.input = SRC

SWT_LIST = np.linspace(SWT_START, SWT_STOP, SWT_N)

SWT_LIST_REP = sorted(np.repeat(SWT_LIST, SWT_REPEAT))

# Output filename
OUTNAME = 'neff_simulation.csv'


def calc_neff(s):
    # IMPORTANT! DROPPING THESE BECAUSE THEY ARE OUTLIER
    p = s[1:-1]
    return (p.mean()/p.std())**2


print(f"Overlap value: {SM.knobs.overlap}")
print(f"Overlap bins: {SM.noverlap}")


neff = {}
neff_fits = {}
for swpnts in SWEEP_POINTS:
    SM.knobs.sweep_points = swpnts
    neff[swpnts] = []
    print(f"Measuring for SWPPOINTS: {swpnts}")
    try:
        for swt in SWT_LIST_REP:
            print(f"Measuring for SWT: {swt:.2f}")
            SM.knobs.swt = swt
            _, s = SM.generate()
            neff[swpnts].append(calc_neff(s))
    except KeyboardInterrupt:
        print("\nAborting...")
        sys.exit(1)
    p = np.polyfit(SWT_LIST_REP, neff[swpnts], 1)
    neff_fits[swpnts] = p


def save(fname, swts, neff, neff_fits):
    metadata = {'linear_fit': str(neff_fits)}
    delimiter = ','
    header = delimiter.join(
        ["swt[s]", *map(lambda s: f"NSWP{s}", neff.keys())])
    datamat = np.vstack([swts, *neff.values()]).transpose()
    write_csv(fname, datamat, delimiter, header, metadata)


save(OUTNAME, SWT_LIST_REP, neff, neff_fits)
print(neff_fits)

markercycle = ['*', 'o', '^']

fig, ax = plt.subplots()
for i, (k, v) in enumerate(neff.items()):
    ax.plot(SWT_LIST_REP, v, markercycle[i])
    ax.plot(SWT_LIST, np.poly1d(neff_fits[k])(SWT_LIST), '-', label=k)

plt.show()

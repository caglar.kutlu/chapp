#!/bin/bash

echo "NEED TO BE MODIFIED TO RUN ON HUB01"
exit 42
 
# MUST BE RUN FROM capphed rootdir
CQ2HED="poetry run python -m capphed"

OUTNAME="$HOME/Measurements/CAPP8TB6G/8TB6G-HE-P1/capp8tb6g-he-p1"
DATADIR="$HOME/Measurements/CAPP8TB6G/8TB6G-HE-P1/axiondata"
# Datasets
CD10_R4="$DATADIR/CD10-R4/exp00000029"
CD10_R5="$DATADIR/CD10-R5/exp00000030"
CD10_R6="$DATADIR/CD10-R6/exp00000032"
CD10_R7="$DATADIR/CD10-R7/exp00000035"

SOURCE="CAPP8TB-6G"

NTMEASDIR="$HOME/Measurements/CAPP8TB6G/8TB6G-HE-P1/NTMEAS_CD10_SPEQAN_BINW15625_HIAVG_X10dB_HIREP.proc.nc"

CAPPHEDDIR="$HOME/Work/capphed"

THISDIR=$PWD

pushd $CAPPHEDDIR


##############
### CD10-R4 ##
RUNNAME="CD10-R4"
RUNDIR=$CD10_R4

echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

$CQ2HED\
    --exp $RUNDIR\
    --conf $RUNDIR/../conf_capp8tb.py\
    --name $RUNNAME --out "$OUTNAME"\
    --source $SOURCE\
    --ntmeas $NTMEASDIR\
    #--slice 10\


#############
## CD10-R5 ##
RUNNAME="CD10-R5"
RUNDIR=$CD10_R5

echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

$CQ2HED\
    --exp $RUNDIR\
    --conf $RUNDIR/../conf_capp8tb.py\
    --name $RUNNAME --out $OUTNAME\
    --source $SOURCE\
    --slice 538\


#############
## CD10-R6 ##
RUNNAME="CD10-R6"
RUNDIR=$CD10_R6

echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

$CQ2HED\
    --exp $RUNDIR\
    --conf $RUNDIR/../conf_capp8tb.py\
    --name $RUNNAME --out $OUTNAME\
    --source $SOURCE\
    --slice 71\


#############
## CD10-R7 ##
RUNNAME="CD10-R7"
RUNDIR=$CD10_R7

echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

$CQ2HED\
    --exp $RUNDIR\
    --conf $RUNDIR/../conf_capp8tb.py\
    --name $RUNNAME --out $OUTNAME\
    --source $SOURCE\
    --slice 1324\

popd

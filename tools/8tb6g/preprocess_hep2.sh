#!/bin/bash

LOGDIR="$HOME/workspace/logs/$(date -Iminutes)_process_hep2.sh.log"

script -qc "python3 -m chapp.preprocessors.pp1 process $HEP2DS1 hep2_pp1.nc -c 7" $LOGDIR
script -qac "python3 -m chapp.preprocessors.pp1 process $HEP2DS2 hep2_pp1.nc -c 7 --ignore-sideband-covariance -r CD12-R1 CD12-R2 CD12-R3 CD12-R4" $LOGDIR
script -qac "python3 -m chapp.preprocessors.pp1 process $HEP2DS2 hep2_pp1.nc -c 7 -r CD12-R5 CD12-R6" $LOGDIR

import xarray as xr
import netCDF4 as nc
import numpy as np
import pandas as pd
from typing import Sequence, Optional, List, Literal, Union, Dict, Iterable
import attr
from pathlib import Path
from itertools import zip_longest


class HEDError(Exception):
    pass


class FileNotSupportedError(HEDError):
    pass


def _check_hed(ds: nc.Dataset):
    try:
        conventions = ds.Conventions
    except AttributeError:
        try:
            conventions = ds.conventions
        except AttributeError:
            raise FileNotSupportedError("File does not have Conventions attribute.")

    if not conventions.startswith("CAPP-HED"):
        raise FileNotSupportedError(f"Conventions are not supported ({conventions})")


def _read_table(path, group):
    ds = xr.open_dataset(path, group=group)
    df = ds.to_dataframe()
    ds.close()
    return df


def is_row_close(arr, axis, rtol=1e-05, atol=1e-08, equal_nan=False):
    """Performs `numpy.isclose` between the first row (along axis) and the rest of the
    rows.  Returns `True` if all rows are close enough, `False` otherwise."""

    row0 = np.take(arr, 0, axis)
    comparison = np.isclose(arr, row0, rtol, atol, equal_nan)
    return np.all(comparison)


@attr.define
class HEDDataset:
    _paths: Iterable[Path]

    runtable: pd.DataFrame
    runname: Optional[str] = None
    mastertable: Optional[pd.DataFrame] = None

    _path: Optional[Path] = None
    _r2p: Dict[str, Path] = attr.ib(factory=dict)

    @property
    def path(self):
        return self._path

    def select(self, runname: str):
        self._check_run_exists(runname)
        self._path = self._r2p[runname]
        mt = self._get_master_table(runname)
        self.mastertable = mt
        self.runname = runname

    def _get_master_table(self, runname: str, exclude_cols: Sequence[str] = ()):
        rgloc = f"exp/{runname}"
        fname = self.path
        meas_t = xr.open_dataset(fname, group=f"{rgloc}/MeasTable").to_dataframe()
        state_t = xr.open_dataset(fname, group=f"{rgloc}/StateTable").to_dataframe()
        jpa_t = xr.open_dataset(fname, group=f"{rgloc}/JPAState").to_dataframe()
        exp_t = xr.open_dataset(fname, group=f"{rgloc}/ExpState").to_dataframe()
        cav_t = xr.open_dataset(fname, group=f"{rgloc}/CavityState").to_dataframe()

        grandstate: pd.DataFrame = (
            state_t.join(jpa_t, on="jpa_state_id")
            .join(exp_t, on="exp_state_id")
            .join(cav_t, on="cavity_state_id")
        )

        grandmeas = meas_t.join(
            grandstate.rename({"name": "mode"}, axis="columns"), on="state_id"
        )
        df: pd.DataFrame = grandmeas.set_index(
            ["runstep", "mode", "type", "mref"], drop=True
        )
        df.drop(columns=exclude_cols, inplace=True, errors="ignore")
        return df

    def get_ntmeas_baseline_dataset(self, chunks_d=None) -> xr.Dataset:
        """This api will most likely change."""
        ds = xr.open_dataset(self.path, group="exp/NTMeas-BASELINE", chunks=chunks_d)[
            ["tn", "tn_err", "gain", "gain_err"]
        ]
        return ds

    def get_dataset(
        self,
        name: str,
        dnu_mode: Optional[Literal["fsc"]] = "fsc",
        chunks: Optional[int] = None,
        fail_if_inconsistent: bool = True,
        sel_runstep: Optional = slice(0, None),
    ):
        chunks = None if chunks is None else {"runstep": chunks}
        groupname = f"exp/{self.runname}/{name}"
        try:
            ds = xr.open_dataset(self.path, group=groupname, chunks=chunks).sel(
                runstep=sel_runstep
            )
        except OSError as e:
            raise KeyError(e.args[0])
        
        try:
            _ = ds.attrs['name']
        except KeyError:
            ds.attrs['name'] = name

        if dnu_mode == "fsc":
            masterdf = self.mastertable
            fparr = (
                masterdf.loc[ds.runstep.data, "ACTIVE", :, :]
                .reset_index(["mode", "type", "mref"])
                .fp
            )
            fsc = (fparr / 2).rename("fsc").to_xarray()
            ds.coords["fsc"] = fsc
            ds.fsc.attrs = ds.fbin.attrs.copy()
            ds.fsc.attrs["long_name"] = "$f_{sc}$"
            ds.fsc.attrs["comment"] = "Equal to fp/2 where fp is JPA pump frequency."
            ds.coords["dnu"] = ds.fbin - fsc
            dnuattrs = ds.fbin.attrs.copy()
            dnuattrs["comment"] = "fbin - fsc."
            dnuattrs["long_name"] = "$\\Delta \\nu$"

            ds.attrs["_sanity"] = ""
            if is_row_close(ds.dnu.data, axis=ds.dnu.get_axis_num("runstep")):
                # i.e. if all spectra are centered at fp/2
                # This means we can share dnu coordinate in all runsteps.
                # From xarray terms, this means the coordinate variable dnu
                # must have only one dimension, which is 'frequency'.
                # Here we are dropping the 'runstep' dimension from dnu
                ds.coords["dnu"] = (
                    "frequency",
                    ds.dnu.head(runstep=1).squeeze(dim="runstep", drop=True).data,
                )
                # IMPORTANT to assign as array, not dataarray, because it somehow
                # affects fbin coordinate otherwise.
                ds = ds.swap_dims(frequency="dnu")
            elif fail_if_inconsistent:
                # more info here would be helpful
                raise HEDError(f"Inconsistent 'dnu' cooordinate. ({groupname})")
            else:
                # BE CAREFUL
                ds.attrs["_sanity"] = "inconsistent_dnu"

            ds.dnu.attrs = dnuattrs
        return ds

    @classmethod
    def from_path(cls, path: Union[Path, Iterable[Path]]):
        if isinstance(path, Path):
            ncds = nc.Dataset(path, mode="r")
            _check_hed(ncds)
            runtable = _read_table(path, "exp/RunTable")
            ncds.close()
            r2p = dict(zip_longest(runtable.index, [path], fillvalue=path))
            paths = [path]
        else:
            # EXPERIMENTAL
            paths = path
            rts = []
            r2p = {}
            for path in paths:
                runtable = _read_table(path, "exp/RunTable")
                rts.append(runtable)
                r2p.update(dict(zip_longest(runtable.index, [path], fillvalue=path)))
            runtable = pd.concat(rts)
        return cls(paths, runtable, r2p=r2p)

    def _check_run_exists(self, runname: str):
        try:
            self.runtable.loc[runname]
        except KeyError:
            raise HEDError(f"No record of '{runname}'.")

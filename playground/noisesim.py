import numpy as np
import xarray as xr

from numpy.random import default_rng, Generator


rng: Generator = default_rng()


ndnu = 8001
assert (ndnu % 2) == 1
ngrunstep = 1000

halfspan = 500e3
dnu = np.linspace(-halfspan, halfspan, ndnu)
grunstep = np.arange(ngrunstep)

_a = 0.2 + 1e-2 * rng.standard_normal(ngrunstep)
_b = 0.25 / halfspan

sigma_u_sqr = np.multiply.outer(_a, np.ones(ndnu)) + np.multiply.outer(_b, dnu) ** 2
sigma_c_sqr = np.ones_like(sigma_u_sqr) - sigma_u_sqr

sigma_u = np.sqrt(sigma_u_sqr)
sigma_c = np.sqrt(sigma_c_sqr)

x_u = sigma_u * rng.standard_normal((ngrunstep, ndnu))

x_c_right = rng.standard_normal((ngrunstep, ndnu // 2))
x_c_center = rng.standard_normal((ngrunstep, 1))
x_c_left = np.fliplr(x_c_right)

x_c = sigma_c * np.hstack([x_c_left, x_c_center, x_c_right])

x = x_u + x_c

ds = xr.Dataset(
    {
        "x_u": (["grunstep", "dnu"], x_u),
        "x_c": (["grunstep", "dnu"], x_c),
        "x": (["grunstep", "dnu"], x),
        "sus": (["grunstep", "dnu"], sigma_u_sqr),
        "scs": (["grunstep", "dnu"], sigma_c_sqr),
    },
    coords={"grunstep": ("grunstep", grunstep), "dnu": ("dnu", dnu)},
)


fsc = 5.85e9 + grunstep*20e3 + rng.normal(0, 20e3)
ds.coords['fsc'] = ('grunstep', fsc)
ds.coords['fbin'] = (['grunstep', 'dnu'], ds.fsc + ds.dnu)

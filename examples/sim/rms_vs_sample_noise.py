"""Noise Power versus Sweep Time
This simulation is intended to replicate the behaviour of R&S FSV series
spectrum analyzer when used in the RMS detector mode.

"""

import numpy as np
from matplotlib import pyplot as plt
import time

from chapp.sim import BasebandSpectrumMaker, detectors, windows,\
        thermal_noise

def watt2dbm(w): return 10*np.log10(w)+30


OPTS = dict(
    span=100e3,
    rbw=100,
    sweep_points=1000,
    is_auto_swt=True,
    is_auto_overlap=True,
    window=windows.BH92,
    input_impedance=50,
    drop_dc=True,
    pad_coeff=4
    )

SM = BasebandSpectrumMaker.create(**OPTS)

# Noise Parameters
TEMPERATURE = 290
RESISTANCE = 50

# pylint: disable=E1120
SRC = thermal_noise(
    temperature=TEMPERATURE,
    resistance=RESISTANCE)

SM.input = SRC

# For better comparison
SEED = int(time.time())

SM.detector = SM.detectors['RMS']
np.random.seed(SEED)
f_rms, s_rms = SM.generate()

SM.detector = SM.detectors['Sample']
np.random.seed(SEED)
f_sample, s_sample = SM.generate()

fig, ax = plt.subplots()
ax.plot(f_sample, watt2dbm(s_sample), label=f'SAMPLE $\mu$={watt2dbm(s_sample.mean()):.2f} dBm')
ax.plot(f_rms, watt2dbm(s_rms), label=f'RMS $\mu$={watt2dbm(s_rms.mean()):.2f} dBm')
ax.legend()

print(f"RMS: MEAN/SAMPLE: {s_rms.mean()/s_rms.std():.3f}")
print(f"SAMPLE: MEAN/SAMPLE: {s_sample.mean()/s_sample.std():.3f}")

plt.show()

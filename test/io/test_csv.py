import os
import unittest
from datetime import datetime

import numpy as np

from chapp.io.csv import read_csv, read_fsv_dat, read_n9010a_csv,\
        read_n5222a_csv


THISDIR = os.path.dirname(os.path.abspath(__file__))
TESTDATAPATH = os.path.join(THISDIR, 'testdata')


class SimpleCsvTest(unittest.TestCase):
    def setUp(self):
        self.fpath = os.path.join(TESTDATAPATH, "csv.csv")
        self.truth_metadata_fsv = dict(
            Type="FSV-4",
            Version="3.20",
            Values="5000")
        self.truth_metadata = self.truth_metadata_fsv
        self.truth_data = np.array([
            [2099750000, -20.575180053710938],
            [2099750100.020004, -20.580917358398437],
            [2099750200.0400081, -20.568723678588867],
            [2099750300.0600121, -20.59564208984375]])

        self.truth_metadata_n9010a = {
            'Trace': '',
            'Swept SA': '',
            'A.17.55': 'N9010A',
            '507 B25 EP3 PFR': '01',
            'Segment': '0',
            'Number of Points': '101',
            'DATA': ''
            }

        self.emptiesfpath = os.path.join(TESTDATAPATH, "csv_empty_line.csv")
        self.fsvdatpath = os.path.join(TESTDATAPATH, "csv_fsv.dat")
        self.n9010acsvpath = os.path.join(TESTDATAPATH, "csv_n9010a.csv")
        self.n5222acsvpath = os.path.join(TESTDATAPATH, "csv_n5222a.csv")
        self.truth_metadata_n5222a = {
            'version': 'A.01.01',
            'instrument_string': 'Keysight Technologies,N5222A,MY51421830,A.10.45.03',
            'date': datetime(2019, 4, 9, 16, 31, 21),
            'source': 'Standard'}
        self.truth_n5222a_data = np.array([
            [10000000, -28.533838, -36.39241],
            [11000000, -28.459814, -33.703453],
            [12000000, -27.778585, -41.204926]
            ])
        self.truth_n5222a_fieldnames = ['Freq(Hz)', 'S11(DB)', 'S11(DEG)']

    def test_read_csv_simple(self):
        data, metadata = read_csv(self.fpath)
        self.assertSequenceEqual(data.tolist(), self.truth_data.tolist())
        self.assertDictEqual(metadata, self.truth_metadata)

    def test_read_csv_with_empty_metadata_line(self):
        data, metadata = read_csv(self.emptiesfpath)
        self.assertSequenceEqual(data.tolist(), self.truth_data.tolist())
        self.assertDictEqual(metadata, self.truth_metadata)

    def test_read_fsv_dat(self):
        data, metadata = read_fsv_dat(self.fsvdatpath)
        self.assertSequenceEqual(data.tolist(), self.truth_data.tolist())
        self.assertDictEqual(metadata, self.truth_metadata_fsv)

    def test_read_n9010a_csv(self):
        data, metadata = read_n9010a_csv(self.n9010acsvpath)
        self.assertSequenceEqual(data.tolist(), self.truth_data.tolist())
        self.assertDictEqual(metadata, self.truth_metadata_n9010a)

    def test_read_n9010a_csv_twice(self):
        data, metadata = read_n9010a_csv(self.n9010acsvpath)
        self.assertSequenceEqual(data.tolist(), self.truth_data.tolist())
        self.assertDictEqual(metadata, self.truth_metadata_n9010a)

        data, metadata = read_n9010a_csv(self.n9010acsvpath)
        self.assertSequenceEqual(data.tolist(), self.truth_data.tolist())
        self.assertDictEqual(metadata, self.truth_metadata_n9010a)

    def test_read_n5222a_csv(self):
        data, fields, metadata = read_n5222a_csv(self.n5222acsvpath)
        self.assertSequenceEqual(data.tolist(), self.truth_n5222a_data.tolist())
        self.assertSequenceEqual(fields, self.truth_n5222a_fieldnames)
        self.assertDictEqual(metadata, self.truth_metadata_n5222a)


if __name__ == "__main__":
    unittest.main()

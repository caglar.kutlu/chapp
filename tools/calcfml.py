from chapp.physics import DEFAULT_AXIONDM_CONTEXT as adctx
import numpy as np

# KR = 15
# KG = 15
KR = 10
KG = 20
bs = 62.5

misalignments = np.linspace(-KR*bs, 0, 101)
nuarr = np.add.outer(misalignments, adctx.mkwfdomain(KG, bs*KR))
wf = adctx.get_waveform_labframe(5.9e9, nuarr)
lqn = np.mean(wf, axis=0)
FML = np.sqrt(np.sum(lqn**2)/(bs*KR))


print(f"F_ML: {FML:.3e}")
print(f"Effective bandwidth (62.5 Hz/FML): {bs/FML:.3e} Hz")

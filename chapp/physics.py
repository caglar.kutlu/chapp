"""Functions, constants and utilities related to Axion physics.

"""
import typing as t
from typing import Callable, Optional
import attr

from scipy.stats import chi2
from scipy.integrate import quad_vec
import scipy.constants as cnst
from numpy import heaviside
from numpy.typing import NDArray
import numpy as np


# Some typical values for parameters
# DISCLAIMER:  I will remove these variables from top level later.

# RMS velocity of DM in Milky-Way.
BETARMS = 270e3 / cnst.speed_of_light


# Velocity of sun divided by RMS velocity of DM
RSUN = np.sqrt(2 / 3)


# Dimensionless Couplings
G_KSVZ = -0.97


def _mkspect(nu: float, fun: Callable, nuarr: NDArray):
    domain = nuarr + nu
    return fun(domain)


@attr.define
class axiondm_context:
    g_ksvz: float = -0.97  # model
    Lambda: float = 75 * (cnst.eV * 1e6)  # from qcd
    rho: float = 0.45 * (cnst.eV * 1e9) / 1e-6  # dm density
    beta_dm: float = 270e3 / cnst.speed_of_light  # RMS velocity of DM in Milky-Way.
    rsun: float = np.sqrt(2 / 3)
    _axifun: Optional[Callable] = None
    _integration_workers: int = 40

    def signal_power(
        self,
        freq,
        fcav,
        q_l,
        beta,
        formfactor,
        bfield,
        volume,
        g_gamma: t.Optional[float] = None,
    ):
        g_gamma = self.g_ksvz if g_gamma is None else g_gamma
        bw = fcav / q_l
        U00 = (g_gamma * cnst.fine_structure / np.pi) ** 2
        U01 = ((cnst.hbar * cnst.c) ** 3) * self.rho / (self.Lambda**4)
        U02 = (2 * np.pi * fcav / cnst.mu_0) * (bfield**2) * volume
        C0 = formfactor * q_l * beta / (1 + beta)
        C1 = 1 / (1 + (2 * (freq - fcav) / bw) ** 2)
        return U00 * U01 * U02 * C0 * C1

    def _get_waveform(
        self, axifun: t.Callable, f_a: float, nuarr: np.ndarray, misalignment: float
    ):
        self._axifun = axifun
        fstep = np.diff(nuarr.reshape(-1)[[0, 1]]).item()

        return quad_vec(
            _mkspect,
            misalignment,
            misalignment + fstep,
            args=(self._axifun, nuarr),
            workers=self._integration_workers,
        )[0]

    def mkwfdomain(self, bins: int, fstep: float):
        return np.arange(bins) * fstep

    def get_waveform_restframe(
        self, f_a: float, nuarr: np.ndarray, misalignment: float = 0
    ):
        """Get the waveform in restframe.

        The resulting array fulfills the following relation:

            $$
            WF[i+1] = \\int_{δν + ν[i]}^{δν + ν[i+1]} g(f - f_a) df
            $$

        Misalignment can also be introduced directly in the nuarr variable.

        Example:
            >>> actx = DEFAULT_AXIONDM_CONTEXT
            >>> misalignment = 10
            >>> nuarr = actx.mkwfdomain(10, 10) + misalignment
            >>> wf = get_waveform_restframe(5.9e9, nuarr)

        If you want to calculate waveform for multiple misalignments, the best way is to
        pass a matrix for nuarr where rows have different misalignments.

        Example:
            >>> actx = DEFAULT_AXIONDM_CONTEXT
            >>> misalignments = np.add.outer(-10, 10, 21)
            >>> nuarr = np.outer.add(misalignments, actx.mkwfdomain(10, 10))
            >>> wf = get_waveform_restframe(5.9e9, nuarr)

        Args:
            f_a: Axion frequency to determine the shape.
            nuarr:  Equidistantly spaced frequency array, ν[i].
            misalignment:  Misalignment of the axion, δν.
        """

        return self._get_waveform(
            self.get_dist_restframe(f_a, loc=0), f_a, nuarr, misalignment
        )

    def get_waveform_labframe(
        self, f_a: float, nuarr: np.ndarray, misalignment: float = 0
    ):
        """Get the waveform in labframe.

        The resulting array fulfills the following relation:

            $$
            WF[i+1] = \\int_{δν + ν[i]}^{δν + ν[i+1]} g(f - f_a) df
            $$

        Misalignment can also be introduced directly in the nuarr variable.

        If you want to calculate waveform for multiple misalignments, the best way is to
        pass a matrix for nuarr where rows have different misalignments.

        Example:
            >>> actx = DEFAULT_AXIONDM_CONTEXT
            >>> misalignments = np.linspace(-10, 10, 21)
            >>> nuarr = np.outer.add(misalignments, actx.mkwfdomain(10, 10))
            >>> wf = get_waveform_restframe(5.9e9, nuarr)


        Args:
            f_a: Axion frequency to determine the shape.
            nuarr:  Equidistantly spaced frequency array, ν[i].
            misalignment:  Misalignment of the axion, δν.
        """

        return self._get_waveform(
            self.get_dist_labframe(f_a, loc=0), f_a, nuarr, misalignment
        )

    def get_dist_restframe(
        self, f_a: float, loc: t.Optional[float] = None
    ) -> t.Callable[[float], float]:
        """Returns the frequency distribution function for the Axion signal in the
        simplest possible model.

        Args:
            f_a: Frequency corresponding to the rest mass of the axion.

        Returns:
            func: A function that takes one frequency argument and returns the
                distribution value for it.

        Notes:
            See eq. (13) in [1].

            [1]: Brubaker, B. M.(2017). HAYSTAC axion search analysis procedure.
                Physical Review D, 96(12), 1-34.

        """
        betarms = self.beta_dm
        loc = f_a if loc is None else loc
        global _distrestframe912738  # hack for parallelizing
        _distrestframe912738 = chi2(df=3, loc=loc, scale=(f_a * betarms**2) / 6).pdf

        return _distrestframe912738

    def get_dist_labframe(
        self, f_a: float, loc: t.Optional[float] = None
    ) -> t.Callable[[float], float]:
        """Returns the frequency distribution function for the Axion signal in the
        lab frame.  This is the restframe_distribution boosted into the earth
        frame.

        Args:
            f_a: Frequency corresponding to the rest mass of the axion.
            betasquared: Expectation value for squared velocity of
                DM in Milky-Way. (normalized to c)
            r: Velocity of sun divied by RMS velocity of DM.  Defaults to
                ``sqrt{2/3}``.
            loc: Defines the `edge` of the distribution.  If `None`, it is equal to
                `f_a`.  Defaults to `None`.

        Returns:
            func: A function that takes one frequency argument and returns the
                distribution value for it.

        Notes:
            See eq. (14) in [1].

            [1]: Brubaker, B. M.(2017). HAYSTAC axion search analysis procedure.
                Physical Review D, 96(12), 1-34.

        """
        betarms = self.beta_dm
        r = self.rsun
        loc = f_a if loc is None else loc
        beta_sqr = betarms**2
        global _distlabframe912738  # hack for parallelizing

        def _distlabframe912738(f):
            diff = f - loc
            delta = heaviside(diff, 0) * diff
            a0 = (2 / np.sqrt(np.pi)) * (np.sqrt(3 / 2) * 1 / (r * f_a * beta_sqr))
            sinh = np.sinh(3 * r * np.sqrt(2 * delta / (f_a * beta_sqr)))
            exp = np.exp(-3 * delta / (f_a * beta_sqr) - 3 * r**2 / 2)
            return a0 * sinh * exp

        return _distlabframe912738


# This should be the way to access defaults.
DEFAULT_AXIONDM_CONTEXT = axiondm_context()


def get_dist_restframe(
    f_a: float, betarms: float = BETARMS, loc: t.Optional[float] = None
) -> t.Callable[[float], float]:
    """Returns the frequency distribution function for the Axion signal in the
    simplest possible model.

    Args:
        f_a: Frequency corresponding to the rest mass of the axion.
        betarms: RMS velocity of DM in Milky-Way.

    Returns:
        func: A function that takes one frequency argument and returns the
            distribution value for it.

    Notes:
        See eq. (13) in [1].

        [1]: Brubaker, B. M.(2017). HAYSTAC axion search analysis procedure.
            Physical Review D, 96(12), 1-34.

    """
    dc = attr.evolve(DEFAULT_AXIONDM_CONTEXT, beta_dm=betarms)
    return dc.get_dist_restframe(f_a, loc)


def get_dist_labframe(
    f_a: float, betarms: float = BETARMS, r: float = RSUN, loc: t.Optional[float] = None
) -> t.Callable[[float], float]:
    """Returns the frequency distribution function for the Axion signal in the
    lab frame.  This is the restframe_distribution boosted into the earth
    frame.

    Args:
        f_a: Frequency corresponding to the rest mass of the axion.
        betasquared: Expectation value for squared velocity of
            DM in Milky-Way. (normalized to c)
        r: Velocity of sun divied by RMS velocity of DM.  Defaults to
            ``sqrt{2/3}``.
        loc: Defines the `edge` of the distribution.  If `None`, it is equal to
            `f_a`.  Defaults to `None`.

    Returns:
        func: A function that takes one frequency argument and returns the
            distribution value for it.

    Notes:
        See eq. (14) in [1].

        [1]: Brubaker, B. M.(2017). HAYSTAC axion search analysis procedure.
            Physical Review D, 96(12), 1-34.

    """
    dc = attr.evolve(DEFAULT_AXIONDM_CONTEXT, beta_dm=betarms, rsun=r)
    return dc.get_dist_restframe(f_a, loc)

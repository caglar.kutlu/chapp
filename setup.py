#!/usr/bin/env python

from distutils.core import setup
from chapp import __version__

setup(name='chapp',
      version=__version__,
      description="CAPP's Haloscope Analysis Programming Package",
      author='Caglar Kutlu',
      author_email='caglar.kutlu@gmail.com',
      url='',
      packages=['chapp', 'chapp.sim', 'chapp.io'],
      )

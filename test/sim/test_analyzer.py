import unittest

import numpy as np

from chapp.sim.analyzer import BasebandSpectrumMaker
from chapp.sim.windowing import windows


class TestBasebandSpectrumMaker(unittest.TestCase):
    def setUp(self):
        self.SpectrumMaker: BasebandSpectrumMaker = BasebandSpectrumMaker

    def test_calc_view_frequency_edges(self):
        SPAN = 100e3
        SWEEP_POINTS = 1000
        sm = self.SpectrumMaker.create(
                span=SPAN,
                sweep_points=SWEEP_POINTS
                )
        fedges, bin_width = sm.calc_view_frequency_edges(SWEEP_POINTS)
        truth_bin_width = SPAN/SWEEP_POINTS

        self.assertEqual(bin_width, truth_bin_width)

        self.assertEqual(fedges[1] - fedges[0], truth_bin_width)

    def test_bin_width(self):
        SPAN = 100e3
        SWEEP_POINTS = 1000
        sm = self.SpectrumMaker.create(
                span=SPAN,
                sweep_points=SWEEP_POINTS
                )
        f, s = sm.generate()
        truth_bin_width = SPAN/SWEEP_POINTS
        bin_width_from_f = f[1] - f[0]

        self.assertEqual(sm.knobs.bin_width, truth_bin_width)

        self.assertEqual(bin_width_from_f, truth_bin_width)

    def test_frequency_min_and_max(self):
        sm = self.SpectrumMaker.create(drop_dc=False)
        sm.span = 100e3
        f, s = sm.generate()
        binwidth = sm.knobs.bin_width
        truth_min = 0
        truth_max = 100e3 - binwidth
        self.assertEqual(f.min(), truth_min)
        self.assertEqual(f.max(), truth_max)

    def test_span_from_frequency_values(self):
        sm = self.SpectrumMaker.create(drop_dc=False)
        sm.span = 100e3
        f, s = sm.generate()
        binwidth = sm.knobs.bin_width
        leftmostedge = f.min() - binwidth/2
        rightmostedge = f.max() + binwidth/2
        span_from_f = rightmostedge - leftmostedge
        self.assertEqual(sm.span, span_from_f)

    def test_spectrum_return_size(self):
        sm = self.SpectrumMaker.create(
                drop_dc=False,
                sweep_points=10, span=1e3
                )
        f, s = sm.generate()

        self.assertEqual(len(f), len(s))

    def test_overlap_set(self):
        sm = self.SpectrumMaker.create(window=windows.BH92)
        ov_default = sm.knobs.overlap
        noverlap_default = sm.noverlap
        ov_new = 0.5
        sm.knobs.overlap = ov_new
        noverlap_new = sm.noverlap
        # Due to integer conversion at some point, they can not be precisely
        # equal, 3 decimal place equivalence is fine.
        self.assertAlmostEqual(
                noverlap_new/noverlap_default,
                ov_new/ov_default, places=3)


if __name__ == "__main__":
    unittest.main()

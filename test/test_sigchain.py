import unittest
import numpy as np
from numpy.testing import assert_allclose, assert_equal
from chapp.sigchain import (
    smat2tmat_2x2,
    smat2tmat_4x4,
    tmat2smat_2x2,
    tmat2smat_4x4,
    swap_basis,
)


def _mkrandmat(n):
    re, im = np.random.randn(2, n, n)
    return re + 1j * im


class TestTransformations(unittest.TestCase):
    def setUp(self):
        self.ntest = 100

    def test_smat_tmat_2x2(self):
        for i in range(self.ntest):
            mat = _mkrandmat(2)
            tmat = smat2tmat_2x2(mat)
            smat = tmat2smat_2x2(tmat)
            assert_allclose(smat, mat)

    def test_smat_tmat_4x4(self):
        for i in range(self.ntest):
            mat = _mkrandmat(4)
            tmat = smat2tmat_4x4(mat)
            smat = tmat2smat_4x4(tmat)
            assert_allclose(smat, mat)

    def test_swap_basis(self):
        for i in range(self.ntest):
            mat = _mkrandmat(4)
            assert_equal(mat, swap_basis(swap_basis(mat)))

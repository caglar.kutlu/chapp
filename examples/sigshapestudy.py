# coding: utf-8
"""
Estimating the integrated axion waveform for experimental parameters.
This also replicates the study done in Haystac 2017 paper about the choice of expected
misalignment range.

"""

import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
from chapp.physics import DEFAULT_AXIONDM_CONTEXT
from scipy.interpolate import UnivariateSpline

dactx = DEFAULT_AXIONDM_CONTEXT
dactx._integration_workers = 10
NOFFSETS = 300  # no need to increase

# HAYSTAC 2017  z = 0.695856  (approximates to 0.7 in text)
FSTEP = 1000
N = 5
F_A = 5.75e9
FUNC = dactx.get_waveform_restframe

# CAPP8TB6G 2022  z = 0.997737
FSTEP = 62.5 * 4
N = 100
F_A = 5.89e9
FUNC = dactx.get_waveform_labframe

nuarr = dactx.mkwfdomain(N, FSTEP)
offsets = np.linspace(-FSTEP, FSTEP, NOFFSETS)
numat = np.add.outer(offsets, nuarr)

dall = FUNC(F_A, numat)
dallsum = np.sum(dall, axis=1)
dallsum_r = np.roll(dallsum, NOFFSETS // 2)

spline = UnivariateSpline(offsets, dallsum - dallsum_r, s=0)
cfi, cff = spline.roots()[:2]
z = -cfi / FSTEP

fig, ax = plt.subplots()
ax.plot(offsets, dallsum, label="o")
ax.plot(offsets, dallsum_r, label="+")
ax.axvline(cfi, color="r")
ax.axvline(cff, color="r")
ax.set_title(f"z={z}")
ax.legend()

da = xr.DataArray(dall, coords={"offset": ("offset", offsets), "nu": ("nu", nuarr)})
fig, ax = plt.subplots()
d0 = FUNC(F_A, nuarr)
ax.plot(nuarr, d0, label="0 offset")
ax.plot(nuarr, da.sel(offset=slice(cfi, cff)).mean(dim="offset").data, label="Average")

# plt.show()

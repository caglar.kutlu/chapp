"""A module to access resources (data etc.) installed in the user system.  Currently,
the implementation is very simple."""
import os
from pathlib import Path
from typing import Optional


DATAPATH = Path(os.environ["AXIONDATAPATH"])


def list_hed_files(expname: Optional[str] = None):
    if expname is None:
        return list(DATAPATH.glob("*.hednc"))
    elif expname.lower() in ["capp8tb6g", "capp8tb-6g", "8tb6g"]:
        return list(
            path
            for path in DATAPATH.glob("*.hednc")
            if path.name.startswith("capp8tb6g-he-p")
        )

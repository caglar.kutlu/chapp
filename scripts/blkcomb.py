import os
import sys
from pathlib import Path
from typing import Optional, Union, Sequence

from chapp import ppopen
from chapp.blockcomb import BlockCombinationProcessor, BlockCombParams
from datargs import arg, argsclass, parse
from dataclasses import asdict
from groundcontrol.declarators import from_json, to_json
from loguru import logger
import json
import cattrs


cattrs.register_structure_hook(Path, lambda s, t: Path(s))
cattrs.register_unstructure_hook(Path, str)


DEFAULT_PARAMS = BlockCombParams(
    n_reb=10,
    n_blk=20,
    ndrop=5,
    wfpath=os.environ.get("WFPATH", "WFBANK.nc"),
    wf_fa=5.9e9,
    rebin_sbsym=True
)


@argsclass
class Process:
    infile: Path = arg(positional=True, help="Path to preprocessed dataset.")
    params: Optional[Path] = arg(aliases=["-p"], help="Path to parameters JSON file.")
    only_prep: bool = arg(
        help="Prepares and saves the output without complete processing.", default=False
    )
    save_prep: bool = False
    outfile: Optional[Path] = arg(
        help=(
            "Output file path.  The output will be derived from input if this is not"
            " given."
        ),
        default=None,
    )
    chunks: Optional[int] = arg(
        help="Chunks data in grunstep with this number.", default=None
    )
    slice: Optional[Sequence[int]] = arg(
        help="Slice on grunstep.  Useful for debugging.", default=None
    )
    bfield: Optional[float] = arg(
        default=None,
        help="Provides the average B-field strength for signal power calculation.",
    )
    volume: Optional[float] = arg(
        default=None, help="Provides the cavity volume for signal power calculation."
    )
    no_use_alt_gmsqr: bool = arg(
        default=False,
        help="Use the alternative estimation for gmsqr if available.",
    )
    overwrite: bool = False

    def to_json(self):
        d_ = {"proc_opts": cattrs.unstructure(self)}
        return json.dumps(d_, indent=None)


@argsclass
class MakeParams:
    @staticmethod
    def make_params():
        to_json(DEFAULT_PARAMS, Path("./blkcparams.json"))


@argsclass(description=__doc__)
class Opts:
    action: Union[MakeParams, Process]


def process(procopts: Process):
    if procopts.params is None:
        params = DEFAULT_PARAMS
        logger.warning(
            "Parameter file is not provided.  Using default parameters as shown"
            f" below:\n{to_json(params)}"
        )
        response = input("Do you want to use these parameters? [n]/Y")
        if response != "Y":
            abort()
    else:
        params = BlockCombParams.from_json(procopts.params)

    outfile = (
        Path(procopts.infile.name).with_suffix(".blkc.nc")
        if procopts.outfile is None
        else procopts.outfile
    )
    if outfile.exists() and (not procopts.overwrite):
        logger.warning(
            f"{outfile} exists.  The contents will be overwritten, do you want"
            " to continue? [n]/Y"
        )
        response = input()
        if response != "Y":
            abort()
    logger.info(f"Using {outfile} for output.")

    if procopts.volume is None:
        volume = 3.103e-3
        logger.warning(f"Volume is not provided, using 8TB6G value: {volume} m^3")
        procopts.volume = volume

    if procopts.bfield is None:
        bfield = 6.965
        logger.warning(f"B-field is not provided, using 8TB6G value: {bfield} T")
        procopts.bfield = bfield

    chunks = {"grunstep": procopts.chunks} if procopts.chunks is not None else None
    if procopts.slice is not None:
        sel_ = {"grunstep": slice(*procopts.slice)}
    else:
        sel_ = None

    if not procopts.no_use_alt_gmsqr:
        name_map = {"gmsqr": "gmsqr2"}
    else:
        name_map = None
    ppr = ppopen(
        procopts.infile,
        chunks=chunks,
        sel=sel_,
        name_map=name_map,
        _bfield=procopts.bfield,
        _volume=procopts.volume,
    )
    bcp = BlockCombinationProcessor.make(ppr, params)
    logger.info("Preparing intermediate dataset for block combination.")
    bcp.prepare()
    logger.info("Finished preparing the intermediate dataset.")

    comment = procopts.to_json()

    if not procopts.only_prep:
        logger.info("Proceeding with block combination.")
        grandds = bcp.process().assign_attrs(comment=comment)
        logger.info("Saving the result.")
        grandds.to_netcdf(outfile, mode="w")
    else:
        grandds = None

    if procopts.save_prep:
        logger.info("Saving the prep data.")
        try:
            bcp.save_prep(outfile)
        except FileNotFoundError:
            bcp.save_prep(outfile, mode="w")
    return bcp, grandds


def abort():
    logger.info("Aborting.")
    return sys.exit(42)


def main(args):
    if isinstance(args.action, MakeParams):
        args.action.make_params()
    elif isinstance(args.action, Process):
        try:
            result = process(args.action)
        except KeyboardInterrupt:
            logger.error("Keyboard interrupt received.")
            return abort()
        else:
            logger.info("Done.")
            return result


if __name__ == "__main__":
    args = parse(Opts)
    bcp, grandds = main(args)

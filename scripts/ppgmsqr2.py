"""This script augments a preprocess data by calculating sigma_u^2, sigma_c^2 and
re-estimating gmsqr.  

The gmsqr estimation is done based on the empirical observation that
(sigma_u^2)*sqrt(gj) ~const over dnu.  The newly estimated gmsqr is appended to the
original dataset with the name 'gmsqr_2'.

"""
from pathlib import Path
import xarray as xr
from xarray import DataArray
from datargs import parse, argsclass, arg
from loguru import logger
from chapp import ppopen
import numpy as np
import netCDF4 as nc
from chapp.preprocessors import (
    calc_corr_uncorr_sigmas,
    calc_gmsqr_from_corr_sigma_u_sqr,
)


@argsclass
class ProgOpts:
    path: Path = arg(positional=True, help="Path to preprocessed dataset.")


def main(opts: ProgOpts):
    ds = ppopen(opts.path)
    gj = ds.gj
    # Check if gj has symmetric dnu dim
    dnup = gj.dnu.sel(dnu=slice(0, None)).data
    dnum = gj.dnu.sel(dnu=slice(0, None, -1)).data
    symdnu_gj = np.all(np.isclose(dnup, -dnum))

    if not symdnu_gj:
        raise ValueError("G_J dataset has non-symmetric dnu coordinate.")
        # maybe we can just use gj instead of taking mean

    logger.info("Assuming gj symmetry around fsc.")
    gjsymm: DataArray = (gj + gj.assign_coords(dnu=-gj.dnu)) / 2

    cfact: DataArray = (gjsymm - 1) / gjsymm
    cfactre = cfact.reindex_like(ds.gmsqr, method="nearest")
    gjsymmre = gjsymm.reindex_like(ds.gmsqr, method="nearest")

    logger.info("Calculating σ_u^2, σ_c^2.")
    scs, sus = calc_corr_uncorr_sigmas(ds.gmsqr, cfactre)
    logger.info("Estimating γ_c^2.")
    gmsqr = (
        calc_gmsqr_from_corr_sigma_u_sqr(sus, gjsymm, cfact)
        .sel(dnu=slice(0, None))
        .to_dataset(name="gmsqr")
    )

    ds.close()
    logger.info(f"Writing new estimation to: {opts.path}:gmsqr2")
    gmsqr.to_netcdf(opts.path, mode="a", group="gmsqr2")
    # cfact.to_netcdf(opts.path, mode="a", group="cfact")
    # gjsymm.to_netcdf(opts.path, mode="a", group="gjsymm")


if __name__ == "__main__":
    main(parse(ProgOpts))

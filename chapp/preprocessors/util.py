from xarray import DataArray, apply_ufunc
import numpy as np


def _apply(fun, da: DataArray):
    return apply_ufunc(fun, da, dask="allowed")


def calc_corr_uncorr_sigmas(gmsqr: DataArray, cfact: DataArray):
    """Returns:
            (sigma_c_sqr, sigma_u_sqr)
    """
    sigma_c_sqr = gmsqr * (1 + cfact ** 2) / (2 * cfact)
    sigma_u_sqr = 1 - sigma_c_sqr

    return sigma_c_sqr, sigma_u_sqr


def calc_gmsqr_from_corr_sigma_u_sqr(
    sigma_u_sqr: DataArray, gj: DataArray, cfact: DataArray
):
    """The gmsqr estimation is done based on the empirical observation that
    (sigma_u^2)*sqrt(gj) ~const over dnu.  The newly estimated gmsqr is appended to the
    original dataset with the name 'gmsqr_2'.

    Notes:
        The invariant is calculated by reindexing sqrt(gj) on σ_u^2 with nearest
        algorithm.  The resulting dataset is computed with the cfact index assuming gj
        and cfact have same dimensions and coordinates.
    """
    # WE MODEL THIS AS CNST OVER DNU EMPIRICALLY!
    gjsqrt = _apply(np.sqrt, gj)
    invariantoverdnu = sigma_u_sqr * gjsqrt.reindex_like(sigma_u_sqr, method='nearest')
    sigma_u_sqr_est = invariantoverdnu.mean(dim="dnu") / gjsqrt
    gmsqr = (1 - sigma_u_sqr_est) * 2 * cfact / (1 + cfact ** 2)
    return gmsqr

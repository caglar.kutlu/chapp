"""This module contains windowing related classes and several windowing objects.

Main windowing related class is WindowFunc.

You may use this module in two ways:
    1. Get the WindowFunc class and construct your own windows with the
    classmethods `make` and `from_scipy_window`, or
    2. Get a premade collection of windows such as windows_heinzel and use the
    windows from it.

Attributes:
    windows_heinzel (namedtuple of SignalWindow): This is essentially a
        dictionary containing the window definitions from the work of Heinzel et al.
        [1, p.29].  The namedtuple type is used to be able to have dotted access
        to members as this is sort of a `constants` data structure.  Don't rely
        on other features of the returning type as the type may change.

Doctest:
    import numpy as np
    >>> n = 10
    >>> window = 'blackmanharris'
    >>> BH = WindowFunc.from_scipy_window('BH', window)
    >>> a = BH.as_array(n)
    >>> b = scipy.signal.get_window(window, n)
    >>> np.all(a == b)
    True

    >>> np.all(a == windows_heinzel.BH92.as_array(n))
    True

References:
    [1]: Heinzel, G. Rudiger, A., Schilling R.  "Spectrum and spectral density estimation by the Discrete Fourier
    transform (DFT), including a comprehensive list of window functions and some
    new flat-top windows. " 2002, Feb 15.  Retrieved from: https://holometer.fnal.gov/GH_FFT.pdf

"""
import typing as t
from collections import namedtuple
from functools import partial

import attr
import numpy as np
import scipy.signal


WindowCallable = t.Callable[[np.ndarray], np.ndarray]

@attr.s(auto_attribs=True, frozen=True)
class WindowFunc:
    """A class to hold the specifics of a windowing function.  

    Attributes:
        name: Name of the windowing function.
        nenbw:  Normalized equivalent noise bandwidth normalized.
        n3dbbw:  Normalized 3 dB bandwidth of the window.
        flatnessdb:  Flatness of window's response function in dB.  
        rov:  Recommended overlap value for the window.

    Args:
        _func: A function that returns the multiplied version of the input array
            with the window function.

    Raises:
        ValueError:  If apply method is called and there is no `_func`.

    """

    name: str
    nenbw: float
    n3dbbw: float
    flatnessdb: float
    rov: float
    _func: t.Optional[WindowCallable] = None

    def apply(self, x: np.ndarray) -> np.ndarray:
        """Applies the window function to the input.

        Args:
            x:  Input array.
        Returns:
            out: Windowed output array.
        
        """
        if self._func is None:
            raise ValueError(f"No concrete function for window '{self.name}'"
                              " was provided.")
        return self._func(x)

    def as_array(self, n: int):
        """Returns the window sample coefficient array for given array length.

        Args:
            n: Length of the window.
        Returns:
            out: Array with length `n`.

        """
        return self.apply(np.ones(n))

    @staticmethod
    def _apply_scipy(x: np.ndarray,
                     window_name: str,
                     args: t.Tuple[t.Any,...]=()) -> np.ndarray:
        """Applies the window from scipy with the name `window_name` with the
        given `args`.

        Args:
            x:  Array that window to be applied on.
            window_name:  Name of the window as it appears in scipy docs.
            args:  A tuple of arguments that the window requires.

        """
        n = len(x)
        winargs = (window_name, *args)
        window_arr = scipy.signal.get_window(winargs, n)
        return window_arr*x

    @classmethod
    def create(cls, name: str,
                    func: t.Optional[WindowCallable],
                    nenbw: t.Optional[float] = -1,
                    n3dbbw: t.Optional[float] = -1,
                    flatnessdb: t.Optional[float] = -1,
                    rov: t.Optional[float] = -1):
        """Factory method to create a WindowFunc instance.

        Args:
            name: Name of the window function.
            nenbw: Normalized equivalent noise bandwidth.
            n3dbbw: Normalized 3dB bandwidth.
            flatnessdb: Flatness of the window function.  Expressed in dB.           
            rov: Recommended overlap ratio.

        """

        # TODO: Calculate nenbw, n3dbbw etc. if not provided.
        return cls(name, nenbw, n3dbbw, flatnessdb, rov, func)

    @classmethod
    def from_dict(cls, dct):
        """Convenience function to use as `from_dict(attr.asdict(other))`"""
        tmp = {}
        # Stripping underscores if any.
        for k,v in dct.items():
            key = k if not k.startswith('_') else k[1:]
            tmp[key] = v
        return cls.create(**tmp)

    @classmethod
    def from_scipy_window(cls, name: str,
                               name_in_scipy: str,
                               window_args: t.Tuple[t.Any, ...] = (),
                               **kwargs):
        """Factory method to create a WindowFunc instance from a
            scipy.signal.get_window compatible name.

        Args:
            name: Name of the window function.
            name_in_scipy: Name of the window function as given in scipy.signal.
            window_args: Parameters for the window if any required.
            **kwargs: Any other keyword argument that `create` factory has.

        """
        # if we don't check this, silent errors will show up.
        if not isinstance(window_args, tuple): 
            raise TypeError("'window_args' argument must be a tuple.")

        func = partial(cls._apply_scipy, window_name=name_in_scipy,
                args=window_args)
        return cls.create(name, func, **kwargs)


class WindowNotFoundError(Exception):
    pass


def get_window(name: str):
    """A function returning the window corresponding to the given name if it
    exists."""
    try:
        window = _windows_heinzel[name]
    except KeyError as exc:
        raise WindowNotFoundError(
            f"Window with name {name} does not exist.") from exc
    return window


# I haven't checked the other windows yet, so these are the only available ones
# for outside use
_availables_heinzel = ['Rectangular', 'BH92']


# TODO: Construct these properly using WindowFunc.{create|from_scipy_window}
# TODO: CHANGE rov's into values between 0 and 1! Currently they are percentage!
_windows_heinzel = {
        "Rectangular": WindowFunc.from_scipy_window("Rectangular", 'boxcar',
            nenbw=1, n3dbbw=0.8845, flatnessdb=-3.9224, rov=0),
        "BH92": WindowFunc.from_scipy_window("BH92", 'blackmanharris',
            nenbw=2.0044, n3dbbw=1.8962, flatnessdb=-0.8256, rov=0.661),
        # APPLY TODO TO BELOW
        "Welch": WindowFunc("Welch", 1.2, 1.1535, -2.2248, 29.3),
        "Bartlett": WindowFunc("Bartlett", 1.3333, 1.2736, -1.8242, 50),
        "Hanning": WindowFunc("Hanning", 1.5, 1.4382, -1.4236, 50),
        "Hamming": WindowFunc("Hamming", 1.3628, 1.3008, -1.7514, 50),
        "Nuttall3": WindowFunc("Nuttall3", 1.9444, 1.8496, -0.8630, 64.7),
        "Nuttall4": WindowFunc("Nuttall4", 2.31, 2.1884, -0.6184, 70.5),
        "Nuttall3a": WindowFunc("Nuttall3a", 1.7721, 1.6828, -1.0453, 61.2),
        "Kaiser3": WindowFunc("Kaiser3", 1.7952, 1.7025, -1.0226, 61.9),
        "Nuttall3b": WindowFunc("Nuttall3b", 1.7037, 1.6162, -1.1352, 59.8),
        "Nuttall4a": WindowFunc("Nuttall4a", 2.1253, 2.0123, -0.7321, 68),
        "Nuttall4b": WindowFunc("Nuttall4b", 2.0212, 1.9122, -0.8118, 66.3),
        "Kaiser4": WindowFunc("Kaiser4", 2.0533, 1.9417, -0.7877, 67),
        "Nuttall4c": WindowFunc("Nuttall4c", 1.9761, 1.8687, -0.8506, 65.6),
        "Kaiser5": WindowFunc("Kaiser5", 2.283, 2.1553, -0.6403, 70.5),
        "SFT3F": WindowFunc("SFT3F", 3.1681, 3.1502, 0.0082, 66.7),
        "SFT3M": WindowFunc("SFT3M", 2.9452, 2.9183, -0.0115, 65.5),
        "FTNI": WindowFunc("FTNI", 2.9656, 2.9355, 0.0169, 65.6),
        "SFT4F": WindowFunc("SFT4F", 3.797, 3.7618, 0.0041, 75),
        "SFT5F": WindowFunc("SFT5F", 4.3412, 4.291, -0.0025, 78.5),
        "SFT4M": WindowFunc("SFT4M", 3.3868, 3.3451, -0.0067, 72.1),
        "FTHP": WindowFunc("FTHP", 3.4279, 3.3846, 0.0096, 72.3),
        "HFT70": WindowFunc("HFT70", 3.4129, 3.372, -0.0065, 72.2),
        "FTSRS": WindowFunc("FTSRS", 3.7702, 3.7274, -0.0156, 75.4),
        "SFT5M": WindowFunc("SFT5M", 3.8852, 3.834, 0.0039, 76),
        "HFT90D": WindowFunc("HFT90D", 3.8832, 3.832, -0.0039, 76),
        "HFT95": WindowFunc("HFT95", 3.8112, 3.759, 0.0044, 75.6),
        "HFT116D": WindowFunc("HFT116D", 4.2186, 4.1579, -0.0028, 78.2),
        "HFT144D": WindowFunc("HFT144D", 4.5386, 4.4697, 0.0021, 79.9),
        "HFT169D": WindowFunc("HFT169D", 4.8347, 4.7588, 0.0017, 81.2),
        "HFT196D": WindowFunc("HFT196D", 5.1134, 5.0308, 0.0013, 82.3),
        "HFT223D": WindowFunc("HFT223D", 5.3888, 5.3, -0.0011, 83.3),
        "HFT248D": WindowFunc("HFT248D", 5.6512, 5.5567, 0.0009, 84.1),
}

windows = namedtuple('BuiltinWindows', _windows_heinzel.keys())(**_windows_heinzel)

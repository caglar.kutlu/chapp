"""This module contains data types for holding measurement data.  The
observable, as used in this module refers to values or arrays of values
obtained from an instrument or derived using some values obtained from an
instrument.

"""

import typing as t
from itertools import product
from enum import Enum

import attr
import numpy as np


def _db2lin(magdb: float):
    return 10**(magdb/20)


def _linmagdeg2complex(maglin: float, angledeg: float):
    complx = maglin*np.exp(1j*np.radians(angledeg))
    return complx


def _logmagdeg2complex(magdb: float, angledeg: float):
    maglin = _db2lin(magdb)
    return _linmagdeg2complex(maglin, angledeg)


def _realimag2complex(real: float, imag: float):
    return np.complex(real, imag)


class SDataFormat(str, Enum):
    """Enum type holding data formats for creating SParameters instances."""
    LogDeg = "LogDeg"
    LinDeg = "LinDeg"
    RealImag = "RealImag"


_SDF = SDataFormat
_DATAFORMAT_CONVERTERS = {
    _SDF.LogDeg: _logmagdeg2complex,
    _SDF.LinDeg: _linmagdeg2complex,
    _SDF.RealImag: _realimag2complex
}


@attr.s(auto_attribs=True, frozen=True)
class SParameters:
    """Holds the s-parameter data.  Frequencies are always in Hz, and
        s-parameters are stored as linear complex values.

    Attributes:
        farray:  A 1-d numpy array containing measurement frequencies.
        values:  A 2-d numpy array containing s-parameters as complex values
            in the columns with the ordering [S11, S12, ..., S21, S22 ...].
            The rows correspond to frequencies in fdata.
        ports:  The number of ports of the component that the measurement data
            belongs to.
        ismagnitude:  Whether or not the s-parameters are magnitude only.
    """

    farray: np.ndarray
    values: np.ndarray
    ports: int
    names: t.List[str]
    ismagnitude: bool

    def _c2mag(self):
        # make sure this returns float type not complex type arrs
        return np.abs(self.values)

    def as_magnitude(self):
        """Returns a new instance of SParameters with linear magnitude
        values"""
        values = self._c2mag()
        return self.from_arrays_complex(self.farray, values)

    @staticmethod
    def _gennames(ports: int = 1):
        ports_str = "".join(map(str, range(1, ports+1)))
        # Cartesian product
        pairs = product(ports_str, repeat=2)
        names = [f"s{''.join(pair)}" for pair in pairs]
        return names

    @classmethod
    def from_arrays_complex(cls, farray: np.ndarray, values: np.ndarray):
        """Returns SParameters instance from the supplied 1-d frequency array
        and 2-d s-parameter array.
        """
        ismagnitude = not np.iscomplexobj(values)
        paramcount = values.shape[1]
        # Total parameter space is the cartesian product PxP.
        _ports = np.sqrt(paramcount)
        if not _ports.is_integer():
            raise ValueError("The S-Parameter values must have enough columns"
                             "to fully characterize n-port device.")
        else:
            ports = int(_ports)

        names = cls._gennames(ports)
        return cls(farray, values, ports, names, ismagnitude)

    @classmethod
    def from_matrix_complex(cls, data: np.ndarray):
        """Returns SParameters instance using data matrix with the first column
        as frequency values.

        Args:
            data:  The data matrix containing the frequencies and s-parameter
                measurements.  Whether or not s-parameters are for magnitude
                is determined by inspecting the type of the input data array.
                If float -> magnitude, else complex.  The columns must be
                ordered in the order of port permutations, i.e. s11,s12,s13 and
                so on.

        """
        farray = data[:, 0].astype('float')
        values = data[:, 1:]
        return cls.from_arrays_complex(farray, values)

    @classmethod
    def from_arrays(
            cls, farray: np.ndarray, values: np.ndarray,
            data_format: t.Union[SDataFormat, str]):
        """Returns SParameters instance using the supplied 1-d frequency array
        and s-parameter values from the 2-d array.

        Every s-parameter data is assumed to span two columns.  The
        meaning of each column is determined by the `data_format` argument.

        For example, if data_format is "LogDeg" then the first column
        after frequency data will be magnitude in dB and the second will be
        angle in degrees.

        Args:
            farray:  1-d frequency array.
            values:  The data matrix containing the s-parameters. 
                The columns must be ordered in the order of port
                permutations, e.g. s11mag, s11deg, s12mag, s12deg,...
            data_format:  Must be one of ['LogDeg', 'LinDeg', 'RealImag']
        """
        cnvfun = _DATAFORMAT_CONVERTERS[data_format]
        paramcount = values.shape[1]//2
        slices = [slice(2*i, 2*i+2) for i in range(paramcount)]

        values_new_lst = []
        for slc in slices:
            arg1, arg2 = values[:, slc].T
            zval = cnvfun(arg1, arg2)
            values_new_lst.append(zval)

        values_new = np.vstack(values_new_lst).T
        return cls.from_arrays_complex(farray, values_new)

    @classmethod
    def from_matrix(
            cls, data: np.ndarray, data_format: t.Union[SDataFormat, str]):
        """Returns SParameters instance using data matrix with the first column
        as frequency values and consecutive values for s-parameters.

        Every s-parameter data is assumed to span two columns.  The
        meaning of each column is determined by the `data_format` argument.

        For example, if data_format is "LogMag_Angle" then the first column
        after frequency data will be magnitude in dB and the second will be
        angle in degrees.

        Args:
            data:  The data matrix containing the frequencies and s-parameter
                measurements. The columns must be ordered in the order of port
                permutations, e.g. farray,s11mag, s11deg, s12mag, s12deg,...
            data_format:  Must be one of ['LogDeg', 'LinDeg', 'RealImag']

        """
        farray = data[:, 0]
        values = data[:, 1:]
        return cls.from_arrays(farray, values, data_format)

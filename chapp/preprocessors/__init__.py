from pathlib import Path
from typing import Dict, Optional, Sequence, Union

from chapp.preprocessors.pp1 import PreprocessResult as _PP1PreprocessResult
from chapp.preprocessors.protocols import PreprocessResult, PreprocParameters
from chapp.preprocessors.util import calc_corr_uncorr_sigmas, calc_gmsqr_from_corr_sigma_u_sqr


def ppopen(
        path: Path,
        group=".",
        chunks: Optional[Dict[str, int]] = None,
        sel: Optional[Dict[str, Union[slice, int, Sequence]]] = None,
        name_map: Optional[Dict[str, str]] = None, **kwargs
        ) -> PreprocessResult:
    """Opens a preprocessed data file.

    Args:
        path: Path to the file.
        group: Group to search under.
        chunks: Chunking on certain dimensions.  Usually what you want is {'grunstep':
            <number-like-100>} or `None`.
        sel: A selection on some dimensions.  Used to avoid processing large datasets,
            typical usage is {'grunstep': slice(<some-number>, <another-number>)}.
        name_map: Mapping the PreprocessResult acceptable dataset names to available
            names in the dataset.  For example if you have an alternative gmsqr estimation
            recorded as gmsqr2 and you wanna use that one instead of the canonical one,
            supply: {'gmsqr': 'gmsqr2'}.

    """
    # kwargs may be _bfield and _volume for amending that information into
    # a preprocess result
    return _PP1PreprocessResult.from_path(path, group, chunks, sel, name_map, **kwargs)

"""This module exposes some functions to generate kernels from known PSDs.  It
is intended to be a gateway to a more generic matched filter design interface
in the future.

The kernel we use in haloscope signal analysis comes from the free gas
treatment of axion in it's rest frame.  The frequency distribution of the power
spectrum of the voltage signal forms our kernel.  In reality, the estimated
power spectrum of the signal will not be identical to the theoretical
formulation *even if the measurement devices are perfect and ideal*.  The
difference is important when one wants to form a matched filter which is an
estimator for the signal power with the best signal-to-noise ratio.

"""
import typing as t

import numpy as np
from scipy.integrate import quad


def kernel_from_bins(
        psd: t.Callable[[float], float],
        bins: np.ndarray,
        misalignment: float) -> np.ndarray:
    """Returns the kernel values from the given psd and bins using the
    misalignment percentage.  Each value correspond to the integral of psd for
    that bin shifted by the misalignment percentage.

    Args:
        psd: The normalized power spectral density function to calculated the
            kernel bin values.
        bins: Center values for the bins.
        misalignment: A value between 0 and 1 parameterizing the misalignment
            of the psd function with respect to bins.
    """
    binwidth = bins[1] - bins[0]
    miswidth = misalignment*binwidth
    halfwidth = binwidth/2

    integrateds = []
    for bin_ in bins:
        left = bin_ - halfwidth + miswidth
        right = bin_ + halfwidth + miswidth
        # Discarding the error etc. for now.
        integral = quad(psd, left + miswidth, right + miswidth)[0]
        integrateds.append(integral)

    return np.array(integrateds)


def correlate(
        signal: np.ndarray,
        kernel: np.ndarray,
        normalize: t.Optional[str] = None,
        noise: t.Optional[np.ndarray] = None) -> np.ndarray:
    """Correlates the signal with the given kernel.

    This function computes the correlation of signal with kernel using the
    `numpy.correlate`.  If normalize is `None`, no normalization is applied.

    If `normalize` is 'simple', the output is divided by it's own standard
    deviation.  If `normalize` is 'reference', the correlation over the extra
    `noise` argument will be computed and the result of signal correlation will
    be divided by it's standard deviation.

    Args:
        signal: Signal to search the kernel in.
        kernel: The model to search in signal.
        normalize: Must be either `None` or one of ['simple', 'reference'].  If
            'reference' the `noise` input must be given as a 1-d array of the
            same length as `signal`.
        noise: Refernce noise array to be used in case `normalize` is
            'reference'.

    Returns:
        out: Correlation output.
    """

    corr = np.correlate(signal, kernel, mode='valid')

    if normalize is None:
        out = corr
    elif normalize == 'simple':
        out = corr/corr.std()
    elif normalize == 'reference':
        if noise is None:
            raise TypeError("correlate() missing noise argument")
        elif len(noise) != len(signal):
            raise ValueError("noise and signal have different lengths")
        corr_noise = correlate(noise, kernel)
        out = corr/corr_noise.std()
    return out

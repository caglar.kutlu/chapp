import unittest
from chapp.io.culdaqutils import CQRun, CQExp
from pathlib import Path


class TestCQExp(unittest.TestCase):
    def setUp(self):
        self.exppath = Path(
            "~/Measurements/CAPP8TB6G/8TB6G-HE-P1/axiondata/CD10-R4/exp00000029"
        ).expanduser()
        self.cqexp = CQExp.from_path(self.exppath)

    def test_list_runfiles_signature(self):
        lst = self.cqexp.list_runfiles()
        self.assertIsInstance(lst, list)

    def test_runfile_creation(self):
        rf = self.cqexp.list_runfiles()[0]
        cqrun = CQRun.from_path(rf)
        self.assertIsNotNone(cqrun)

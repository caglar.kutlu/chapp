#!/usr/bin/env python
"""This file "flattens" preprocess results.  Runsteps are collected in a file-global
index called 'grunstep'. Runstep and runname are kept as coordinates.
"""
import os
from collections import defaultdict
from pathlib import Path
from typing import Sequence, Union

import netCDF4 as nc
import pandas as pd
import xarray as xr
from chapp.preprocessors.pp1 import PreprocessResult
from datargs import arg, argsclass, parse
from loguru import logger


@argsclass
class Opts:
    files: Sequence[Path] = arg(positional=True)
    out: Path = arg(positional=True)
    chunks: str = arg(
        aliases=["-c"],
        default="auto",
        help="Runstep chunking.  Must be either 'auto', 'none', or a positive integer.",
    )

    def __post_init__(self):
        self.chunks = self._convert_chunks()

    def _convert_chunks(self):
        if self.chunks == "auto":
            return self.chunks
        elif self.chunks == "none":
            return None
        else:
            try:
                return int(self.chunks)
            except Exception as e:  # it will most likely throw ValueError anyways
                raise ValueError(
                    "chunks option must be either a positive integer, 'auto' or 'none'."
                ) from e


args = parse(Opts)

SORT = True
NCFILES = args.files
CHUNKS = {"runstep": args.chunks}
OUTNAME = args.out

ncrn_al = []  # ncfile, runname assoc list
runstep_d = {}
_runname_d = {}

logger.info("Collecting information about available datasets.")
for ncfile in NCFILES:
    with nc.Dataset(ncfile) as ncds:
        logger.info(f"Inspecting {ncfile}.")
        for runname, ncgrp in ncds.groups.items():
            if runname in _runname_d:
                raise KeyError(
                    f"Encountered multiple {runname} (in {_runname_d[runname]} and"
                    f" {ncfile})."
                )
            ncrn_al.append((ncfile, runname))
            runstep_d[runname] = ncgrp["delta"]["runstep"][:]
            logger.info(f"Runname {runname} has {len(runstep_d[runname])} runsteps.")

            _runname_d[runname] = ncfile


# Get available xarray dataset names using the first run
ncf0, runname0 = ncrn_al[0]
with nc.Dataset(ncf0) as ncds0:
    ppparams = ncds0[runname0].preproc_params
    xrdsnames = ncds0[runname0].groups.keys()

logger.info(f"Available datasets are inferred from {ncf0}::{runname0} are {xrdsnames}.")
logger.info(f"Preprocess parameters are read from {ncf0}::{runname0}.")

dataset_d = defaultdict(list)
for name in xrdsnames:
    logger.info(f"Collecting '{name}' datasets.")
    for i, (ncfile, runname) in enumerate(ncrn_al):
        ds = xr.open_dataset(ncfile, group=f"{runname}/{name}", chunks=CHUNKS)
        if not hasattr(ds, "fsc"):
            logger.warning(
                f"{runname} is missing 'fsc' coordinate in '{name}' dataset. "
                f" Using one from 'delta' dataset of {ncrn_al[i][1]}."
            )
            ds.coords["fsc"] = dataset_d["delta"][i].fsc

        if name == "gmsqr":
            if "rep" in ds.dims:
                logger.warning(
                    "Averaging over the forgotten 'rep' dimension of 'gmsqr' data from"
                    f" {runname}."
                )
                ds = ds.mean(dim="rep", keep_attrs=True)
        elif name == "navg":
            rstemplate = dataset_d["delta"][i].runstep
            if "runstep" not in ds.dims:
                ds = ds.broadcast_like(rstemplate)

        dataset_d[name].append(ds)

combined_d = dict()

logger.info("Creating global runstep (grunstep) coordinate array.")
multiindex = (
    pd.DataFrame.from_dict(runstep_d, orient="index")
    .stack()
    .index.set_names(["run", "runstep"])
)

for k, ds_l in dataset_d.items():
    logger.info(f"Concatenating {k}.")
    cated = xr.concat(ds_l, dim="runstep")
    combined = (
        cated.rename(runstep="grunstep")
        .assign_coords(grunstep=multiindex)
        .reset_index("grunstep")
    )
    # I am not sure if this helps with the incorrect str type issue.
    combined.coords["run"] = combined.run.astype(str)
    combined_d[k] = combined

if SORT:
    final_d = {k: v.sortby("fsc") for k, v in combined_d.items()}
else:
    final_d = combined_d

pr = PreprocessResult.from_xarray_datasets(final_d, params=ppparams)

logger.info(f"Writing to {OUTNAME}.")
pr.to_netcdf(OUTNAME)

with nc.Dataset(OUTNAME, mode="a") as ncds:
    ncds: nc.Dataset
    ncds.setncattr("preproc_params", ppparams)

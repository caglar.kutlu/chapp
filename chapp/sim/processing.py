"""This module provides a functional interface to spectral estimation given a
time series data.


Notes
======
# Rebinning VERY ROUGH EXPLANATION
Suppose we have bin centers given as 0.5, 1.5, and 3.5, denoting the values
 in them using letters {a, b, c,...}.
The |'s represent the bin edge positions.

0   a   1   b   2   c   3   d   4   e   5   f   5
|-------|-------|-------|-------|-------|-------|

We would like to change the binning to the following:
0     [a,b]     2     [c,d]     4     [e,f]     6     []     8
|---------------|---------------|---------------|---------------|

If we label the bins with cardinal numbers {1, 2, 3,}

A digitize operation on the first value set using the binning in the second will
give us the bin numbers of the values correspond to
{a , b , c , d , e , f}
{1 , 1 , 2 , 2 , 3 , 3}

I would like to convert this result into a list of arrays each of which
correspond to the collection of numbers corresponding to the bins indexed
starting from the first bin in an increasing order.

Thus, the result should be:
[{a,b}, {c,d}, {e,f}, {}]

What we know:
- The digitizing operation must be performed on a sequence that is already
  sorted.  Thus, the result will always be of the form:
  {1,1,2,4,5,...)
- Note that s_{i+1} > a_i always if the input is sorted.
- Let s_k = {1, 1, 2, 4, 5}
- From here it's obvious that 3rd bin is empty.
- Compute the collections via split(unique()) as usual.
- This will return:
    {a,b},{c},{d},{f}
- However we need: {a,b}, {c}, {}, {d}, {e}
- To insert the empty bin at the correct place:
- Compute a_i = s_{i+1} - s_i, i = 0, 1, 2, 3
- Find k where a_k > 1
- Insert [] into indice 


"""
import typing as t
import sys
from numbers import Number
import random

import attr
import numpy as np
import scipy.signal
from scipy.stats import binned_statistic

from chapp.sim.windowing import WindowFunc, windows


# binned_statistic raises some warnings about deprecation, silencing.
if not sys.warnoptions:
    import warnings
    warnings.simplefilter("ignore")


def welch(x: np.ndarray,
          fs: float=1.0,
          window: WindowFunc=windows.BH92,
          nperseg: t.Optional[int] = None,
          noverlap: t.Optional[int] = None,
          nfft: t.Optional[int] = None, 
          scaling: str = 'density'):
    """This is a wrapper around scipy.signal.welch.  The defaults have changed
    and some of the controls are removed.  If you want more control, use
    scipy.signal.welch directly instead. 

    Args: 
        x: Time series of measurement values.
        fs: Sampling frequency.
        window: Desired window to use.  Must be a proper instance of
            WindowFunc.
        nperseg: Length of each segment.  If `None`, set to cover all of x.
        noverlap: Number of points to overlap between segments.  If `None`,
            ``noverlap = 0``.  Defaults to `None`.
        nfft: Determines whether or not 0-padding is done.   Must be greater or
            equal to nperseg.   When greater than `nperseg`, remaining samples
            are filled with zeros. If `None`, FFT length is `nperseg`.  Defaults
            to `None`.
        scaling: { 'density', 'spectrum' } Selects between computing the power
            spectral density ('density') where `Pxx` has units of V**2/Hz and
            computing the power spectrum ('spectrum') where `Pxx` has units of V**2,
            if `x` is measured in V and `fs` is measured in Hz. Defaults to
            'density'.

    Returns:
        f: Array of sample frequencies.
        Pxx: Power spectral density or power spectrum of x.

    Examples:
        >>> from scipy import signal
        >>> import matplotlib.pyplot as plt
        >>> np.random.seed(1234)
        >>> fs = 10e3
        >>> N = 1e5
        >>> amp = 2*np.sqrt(2)
        >>> freq = 1234.0
        >>> noise_power = 0.001 * fs / 2
        >>> time = np.arange(N) / fs
        >>> x = amp*np.sin(2*np.pi*freq*time)
        >>> x += np.random.normal(scale=np.sqrt(noise_power), size=time.shape)
        >>> f, Pxx_den = welch(x, fs, nperseg=1024)
        >>> _ = plt.semilogy(f, Pxx_den)
        >>> _ = plt.ylim([0.5e-3, 1])
        >>> _ = plt.xlabel('frequency [Hz]')
        >>> _ = plt.ylabel('PSD [V**2/Hz]')
        >>> _ = plt.show()

    """

    detrend = False # actual default is 'constant' in signal.welch
    nperseg = len(x) if nperseg is None else nperseg # 256 in signal.welch
    noverlap = 0 if noverlap is None else noverlap # can be 50% in signal.welch
    nfft = nperseg if nfft is None else nfft
    return_onesided = True

    window = window.as_array(nperseg)
    
    return scipy.signal.welch(x, fs, window, nperseg, noverlap, nfft, detrend,
            return_onesided, scaling)


def spectrogram(
        x: np.ndarray,
        fs: float = 1.0,
        window: WindowFunc = windows.BH92,
        nperseg: t.Optional[int] = None,
        noverlap: t.Optional[int] = None,
        nfft: t.Optional[int] = None,
        scaling: str = 'density',
        mode: str = 'psd'):
    """This is a wrapper around scipy.signal.spectrogram.  The defaults have changed
    and some of the controls are removed.  If you want more control, use
    scipy.signal.spectrogram directly instead. 

    Args: 
        x: Time series of measurement values.
        fs: Sampling frequency.
        window: Desired window to use.  Must be a proper instance of
            WindowFunc.
        nperseg: Length of each segment.  If `None`, set to cover all of x.
        noverlap: Number of points to overlap between segments.  If `None`,
            ``noverlap = 0``.  Defaults to `None`.
        nfft: Determines whether or not 0-padding is done.   Must be greater or
            equal to nperseg.   When greater than `nperseg`, remaining samples
            are filled with zeros. If `None`, FFT length is `nperseg`.  Defaults
            to `None`.
        scaling: Chooses the output scaling.  Options are ['density',
            'spectrum']. Selects between computing the power spectral density
            ('density') where `Pxx` has units of V**2/Hz and computing the power
            spectrum ('spectrum') where `Pxx` has units of V**2, if `x` is measured
            in V and `fs` is measured in Hz. Defaults to 'density'.
        mode : Defines what kind of return values are expected. Options are
            ['psd', 'complex', 'magnitude', 'angle', 'phase']. 'complex' is
            equivalent to the output of `stft` with no padding or boundary
            extension. 'magnitude' returns the absolute magnitude of the
            STFT. 'angle' and 'phase' return the complex angle of the STFT,
            with and without unwrapping, respectively.

    Returns:
        f: Array of sample frequencies.
        t: Array of segment times.
        Sxx: Spectrogram of x. By default, the last axis of Sxx corresponds to the segment times.

    Examples:
        >>> import matplotlib.pyplot as plt
        >>> import numpy as np
        >>> fs = 10e3
        >>> N = 1e5
        >>> amp = 2 * np.sqrt(2)
        >>> noise_power = 0.01 * fs / 2
        >>> time = np.arange(N) / float(fs)
        >>> mod = 500*np.cos(2*np.pi*0.25*time)
        >>> carrier = amp * np.sin(2*np.pi*3e3*time + mod)
        >>> noise = np.random.normal(scale=np.sqrt(noise_power), size=time.shape)
        >>> noise *= np.exp(-time/5)
        >>> x = carrier + noise
        >>> # Note the defaults are different from scipy.signal.spectrogram
        >>> window = WindowFunc.from_scipy_window('tukey', 'tukey', (.25,))
        >>> f, t, Sxx = spectrogram(x, fs, nperseg=256, noverlap=128//8, window=window)
        >>> _ = plt.pcolormesh(t, f, Sxx)
        >>> _ = plt.ylabel('Frequency [Hz]')
        >>> _ = plt.xlabel('Time [sec]')
        >>> _ = plt.show();

    """

    detrend = False  # actual default is 'constant'
    nperseg = len(x) if nperseg is None else nperseg  # 256 in signal.welch
    noverlap = 0 if noverlap is None else noverlap  # can be 50% in signal.welch
    nfft = nperseg if nfft is None else nfft
    return_onesided = True
    axis = -1

    window = window.as_array(nperseg)
 
    return scipy.signal.spectrogram(x, fs, window, nperseg, noverlap, nfft, detrend,
            return_onesided, scaling, axis, mode)


def spectraggram(
        x: np.ndarray,
        fs: float = 1.0,
        window: WindowFunc = windows.BH92,
        nperseg: t.Optional[int] = None,
        noverlap: t.Optional[int] = None,
        nfft: t.Optional[int] = None,
        scaling: str = 'density',
        mode: str = 'psd',
        aggfun: t.Callable[[np.ndarray], Number] = np.mean
        ) -> (np.ndarray, np.ndarray):
    """Aggregating spectrogram.  Time-slices of the spectrogram is aggregated
    with the provided function.  

    Args: 
        x: Time series of measurement values.
        fs: Sampling frequency.
        window: Desired window to use.  Must be a proper instance of
            WindowFunc.
        nperseg: Length of each segment.  If `None`, set to cover all of x.
        noverlap: Number of points to overlap between segments.  If `None`,
            ``noverlap = 0``.  Defaults to `None`.
        nfft: Determines whether or not 0-padding is done.   Must be greater or
            equal to nperseg.   When greater than `nperseg`, remaining samples
            are filled with zeros. If `None`, FFT length is `nperseg`.
            Defaults to `None`.
        scaling: Chooses the output scaling.  Options are ['density',
            'spectrum']. Selects between computing the power spectral density
            ('density') where `Pxx` has units of V**2/Hz and computing the
            power spectrum ('spectrum') where `Pxx` has units of V**2, if `x`
            is measured in V and `fs` is measured in Hz. Defaults to 'density'.
        mode : Defines what kind of return values are expected. Options are
            ['psd', 'complex', 'magnitude', 'angle', 'phase']. 'complex' is
            equivalent to the output of `stft` with no padding or boundary
            extension. 'magnitude' returns the absolute magnitude of the
            STFT. 'angle' and 'phase' return the complex angle of the STFT,
            with and without unwrapping, respectively.
        aggfun: Aggregating function to be used.  Must take a single array as
            an argument and produce a single scalar as output.

    Returns:
        f: Array of center frequencies of the bins.
        Axx: Aggregated spectrogram.

    Examples:
        >>> from scipy import signal
        >>> import matplotlib.pyplot as plt
        >>> np.random.seed(1234)
        >>> fs = 1e4
        >>> N = 1e5
        >>> amp = np.sqrt(1e-3)
        >>> freq = 1234.0
        >>> noise_power = 1e-6 * fs / 2
        >>> time = np.arange(N) / fs
        >>> x = amp*np.sin(2*np.pi*freq*time)
        >>> x += np.random.normal(scale=np.sqrt(noise_power), size=time.shape)
        >>> window = WindowFunc.from_scipy_window('boxcar', 'boxcar', ())
        >>> opts = dict(nperseg=1024, noverlap=0, window=window, scaling='spectrum')
        >>> f, Pxx = spectraggram(x, fs, mode='psd', **opts)
        >>> f, Pxx_welch = welch(x, fs, **opts)
        >>> _ = plt.plot(f, 10*np.log10(Pxx)+30, label='Spectraggram_RMS')
        >>> _ = plt.plot(f, 10*np.log10(Pxx_welch)+30, label="Welch's")
        >>> _ = plt.legend();
        >>> _ = plt.xlabel('Frequency [Hz]')
        >>> _ = plt.ylabel('Power [dBm]')
        >>> _ = plt.show()
        >>> np.allclose(Pxx, Pxx_welch)
        True

        #>>> _ = plt.ylim([0.5e-3, 1])
    """

    f,t,Sxx = spectrogram(x, fs, window, nperseg, noverlap, nfft, scaling, mode)
        
    # Last axis assumed to contain time slices.
    # Borrowing the logic from scipy.signal.csd
    if len(Sxx.shape) >= 2 and Sxx.size > 0:
        if Sxx.shape[-1] > 1:
            Axx = np.apply_along_axis(aggfun, -1, Sxx)
        else:
            Axx = np.reshape(Sxx, Sxx.shape[:-1])
    return f, Axx


def _rebin_helper(x: np.ndarray,
                  bins: np.ndarray, 
                  rebin_edges: np.ndarray,
                  ignore_out_of_bounds: bool = True):
    """Performs a rebinning for the input bins defined by `bins`. Collects the 
    input `x` array values into bins defined by `bin_edges`.  The empty bin
    contents are omitted, and the bin numbers of the non-empty bins are
    returned.

    Args:
        x:  Input array.
        bins:  Center values for the bins currently corresponding to x values.
            Must be the same length as x.
        rebin_edges:  Edges of the bins to collect x values into.  Length of this
            must be `len(x)+1`.
        ignore_out_of_bounds:  Ignores out of bounds values properly.

    Returns:
        out:  List of arrays.  Each array is a collection of all the x values
            that fall into the corresponding bin in the `bin_numbers` array.
        bin_numbers:  An array of the same length as `out`.  Denotes the number
            of the bin at which an element of out belong to.

    """
    # The indices of the bins to which each value in input array belongs.
    d = np.digitize(bins, rebin_edges)

    if ignore_out_of_bounds:
        validmask = np.logical_and(d != 0, d != len(rebin_edges))
        d = d[validmask]
        x = x[validmask]
    
    nonempty, split_indices = np.unique(d, return_index=True)
    """
    nonempty(:np.ndarray:):  Contains the ordinal number of non-empty bins.
    split_indices(:np.ndarray):  Indices at which to perform out split
    """

    # Contents of the non-empty bins
    out = np.split(x, split_indices[1:])
    return out, nonempty


def rebin_collect(
        x: np.ndarray,
        bins: np.ndarray,
        rebin_edges: np.ndarray,
        ignore_out_of_bounds: bool = True):
    """Performs a rebinning for the input bins defined by `bins`. Collects the 
    input `x` array values into bins defined by `bin_edges` and returns the
    result as a list of these collections.

    Args:
        x:  Input array.
        bins:  Center values for the bins currently corresponding to x values.
            Must be the same length as x.
        rebin_edges:  Edges of the bins to collect x values into.  Length of
            this must be `len(x)+1`.
        ignore_out_of_bounds:  Ignores out of bounds values properly.

    Returns:
        out:  List of arrays.  Each array is a collection of all the x values
            that fall into the corresponding bin.

    """
    filled_bins, filled_bin_numbers = _rebin_helper(
        x, bins, rebin_edges,
        ignore_out_of_bounds)

    # Ordinal numbers for all of the bins
    bin_numbers = set(np.arange(1, len(rebin_edges)))

    # Ordinal numbers of the empty bins
    empty_bin_numbers = bin_numbers.difference(filled_bin_numbers)

    for i in empty_bin_numbers:
        # If you want to optimize, start here.
        filled_bins.insert(i-1, np.array([]))

    return filled_bins


def rebin_apply(
        x: np.ndarray,
        bins: np.ndarray,
        rebin_edges: np.ndarray,
        func: t.Callable[[np.ndarray],Number],
        ignore_out_of_bounds: bool = True,
        method: str = 'generic') -> np.ndarray:
    """Performs a rebinning for the input bins defined by `bins`. Collects the 
    input `x` array values into bins defined by `bin_edges`.  After collecting,
    the input `func` is applied to each collection and the result is returned
    as a numpy array.
    
    Notes:
        - Untested if bins and/or rebin_edges are not sorted in an increasing
          fashion.
        - scipy.stats package has a better-performing function called
          `binned_statistic`, I may wanna use that instead.

    Args:
        x:  Input array.
        bins:  Center values for the bins currently corresponding to x values.
            Must be the same length as x.
        rebin_edges:  Edges of the bins to collect x values into.  Length of
            this.
        func:  Function that takes a single 1-D array and returns a Number.
        ignore_out_of_bounds:  Ignores out of bounds values properly.
        method:  The internal method to use.  It can be 'generic' or 'fast'.
            Defaults to 'generic'.'generic' one uses the `rebin_collect` of
            this library, whereas 'fast' uses the `binned_statistic` from
            `scipy.stats`.  The latter may not work for all functions, but is
            approximately 8 times faster than the former (complexity may be
            similar).

    Returns:
        out:  An array of Numbers.

    """
    if method == "generic":
        collected = rebin_collect(x, bins, rebin_edges, ignore_out_of_bounds)
        out = np.array(list(map(func, collected)))
    elif method == "fast":
        if ignore_out_of_bounds:
            range_ = (np.min(rebin_edges), np.max(rebin_edges))
        else:
            range_ = None
        out = binned_statistic(
            bins, x, statistic=func, bins=rebin_edges, range=range_).statistic
    return out


def _compare():
    import timeit
    from scipy.stats import binned_statistic
    def create_params(n):
        bincenters = np.sort(np.random.random(n))*10*n
        values = np.random.normal(size=n)
        rebin_edges = np.sort(random.sample(range(10*n), n))
        return values, bincenters, rebin_edges

    N = 100
    v,b,r = create_params(N)

    def profile_rebin_apply():
        rebin_apply(v, b, r, np.mean)

    def profile_binned_statistic():
        binned_statistic(b, v, 'mean', r)
        
    TESTCOUNT = 1000  # the function is called this many times in a test.
    REPEATTEST = 10  # test is repeated this many times.
    
    rebapp_t = timeit.repeat(
            profile_rebin_apply, number=TESTCOUNT, repeat=REPEATTEST)
    binstat_t = timeit.repeat(
            profile_binned_statistic, number=TESTCOUNT, repeat=REPEATTEST)

    print(
        f"Functions are called {TESTCOUNT} times for each test"
        f"with a total of {REPEATTEST} tests.")

    print("rebin_apply:")
    print(f"\tMean: {np.mean(rebapp_t):.2f} s Std: {np.std(rebapp_t):.2e}")
    print("binned_statistic:")
    print(f"\tMean: {np.mean(binstat_t):.2f} s Std: {np.std(binstat_t):.2e}")


def _profile(fname=None):
    import cProfile
    def create_params(n):
        bincenters = np.sort(np.random.random(n))*10*n
        values = np.random.normal(size=n)
        rebin_edges = np.sort(random.sample(range(10*n), n))
        return values, bincenters, rebin_edges

    N = 100000
    v,b,r = create_params(N)

    cProfile.runctx(
            'rebin_apply(v, b, r, np.mean)', globals=globals(),
            locals=locals(), filename=fname)

    # Result of basic profiling:
    #   optimize away the list.insert function in
    #   rebin_collect first.


if __name__ == "__main__":
    # For profiling
    _profile("processing.profile")

"""This module provides class based abstractions for signal generation as well
as some instances of those classes for simple signal types

At this stage, this API is not flexible and will definitely change.

Notes:
    - Seems like defining signals as classes is a better way rather than making
      all the signals an instance of `Source`.  Inheritance or factory?

TODO:
    - Check out Zero Padding note in your jupyternotebooks repo, you implemented
      a simpler scheme for defining signals.  Adapt it here.

Example:
    >>> import matplotlib.pyplot as plt
    >>> n, fs = 1000, 1e6
    >>> sig = thermal_noise()
    >>> noise = sig(n, fs)
    >>> t = np.arange(n)/fs
    >>> _ = plt.figure()
    >>> _ = plt.plot(t, noise)
    >>> _ = plt.ticklabel_format(axis='both', scilimits=(-3,3), useMathText=True)
    >>> _ = plt.title("Just Thermal Noise")
    >>> _ = plt.xlabel("Time [s]")
    >>> _ = plt.ylabel("Noise [V]")

    # Summing example:
    >>> import matplotlib.pyplot as plt
    >>> n, fs = 1000, 1e6
    >>> noise_s = thermal_noise()
    >>> sine_s = sine(amplitude=1e-6, frequency=1e4, phase=0)
    >>> sig = sine_s + noise_s
    >>> samples = sig(n, fs)
    >>> t = np.arange(n)/fs
    >>> _ = plt.figure()
    >>> _ = plt.plot(t, samples)
    >>> _ = plt.ticklabel_format(axis='both', scilimits=(-3,3), useMathText=True)
    >>> _ = plt.title("Signal + Thermal Noise")
    >>> _ = plt.xlabel("Time [s]")
    >>> _ = plt.ylabel("Sample Voltage [V]")

    # Baseband Axion example:
    >>> import matplotlib.pyplot as plt
    >>> n, fs = int(1e6), 50e6
    >>> sig = baseband_axion_lab(loc=10e3, total_power=1, f_axion=2.5e9)
    >>> samples = sig(n, fs)
    >>> t = np.arange(n)/fs
    >>> _ = plt.figure()
    >>> _ = plt.plot(t, samples)
    >>> _ = plt.ticklabel_format(axis='both', scilimits=(-3,3), useMathText=True)
    >>> _ = plt.title("Baseband Axion Signal in Galaxy Frame")
    >>> _ = plt.xlabel("Time [s]")
    >>> _ = plt.ylabel("Sample Voltage [V]")

    Baseband Axion Periodic Example:
    >>> import matplotlib.pyplot as plt
    >>> n, fs = int(5e6), 50e6
    >>> sig = baseband_axion_galaxy_periodic(period=0.02, loc=10e3, total_power=1, f_axion=2.5e9)
    >>> samples = sig(n, fs)
    >>> t = np.arange(n)/fs
    >>> _ = plt.figure()
    >>> _ = plt.plot(t, samples)
    >>> _ = plt.ticklabel_format(axis='both', scilimits=(-3,3), useMathText=True)
    >>> _ = plt.title("Periodic Axion in Lab Frame")
    >>> _ = plt.xlabel("Time [s]")
    >>> _ = plt.ylabel("Sample Voltage [V]")

    # Show everything together.
    >>> _ = plt.show();

"""
import typing as t
from inspect import signature
import functools
from numbers import Number

import attr
import numpy as np
from numpy.fft import irfft
from scipy.stats import chi2
import scipy.constants as cnst
from scipy import heaviside

from chapp.sim.utils import johnson_noise_power,\
        repeating_array, split_reverse_combine
from chapp.physics import get_dist_labframe, get_dist_restframe, BETARMS, RSUN


class SignatureError(TypeError):
    pass


_SourceFunction = t.Callable[[float, float], np.ndarray]


@attr.s(auto_attribs=True)
class Source(_SourceFunction):
    """A class for sampling signals.  Use this instead of passing around
    functions.  You can sum signals.

    Attributes:
        name: Name of the signal.

    """
    _func: _SourceFunction
    name: str = ""
    args_dict: t.Dict[str, t.Any] = attr.Factory(dict)

    def __call__(self, n, fs):
        return self._func(n, fs, **self.args_dict)

    def _lift(self, applyfun, otherfun):
        def f(n, fs):
            rself = self(n, fs)
            rother = otherfun(n, fs)
            return applyfun(rself, rother)
        return f
        
    def __add__(self, other):
        summed = self._lift(lambda a,b: a+b, other)

        if hasattr(other, 'name'):
            newname = f"({self.name}+{other.name})"
        else:
            newname = f"({self.name} + {other.__name__})"
        return self.create(summed, name=newname)

    def sample(self, n: int, fs: float) -> np.ndarray:
        return self._func(n, fs, **self.args_dict)

    @property
    def isnull(obj):
        return obj._func is None
    
    @classmethod
    def create(cls, func: _SourceFunction,
                  name=""):
        return cls(func=func, name=name)

    @classmethod
    def create_partial(cls, func: t.Callable,
                            name: str="",
                            **kwargs):
        """Convenience function that freezes a function's several parameters
        before creating the signal.

        Args:
            func: Function to create signal from.
            name: Name of the signal.
            **kwargs:  Extra keyword arguments to be passed to the function
                before creating the signal.

        """
        func = functools.partial(func, **kwargs)
        return cls.create(func=func, name=name)

    @classmethod
    def wrap(cls, func, name=""):
    # -> t.Callable[[t.Any,...], t.Callable[[t.Any,...], Source]]:
        """Wraps the function into a Source returning function."""
        sign = signature(func) 
        params = sign.parameters.keys()
        if not ( ('n' in params) and ('fs' in params) ):
            raise SignatureError("The source function must have `n` and `fs` as"
                                 "it's first two arguments.")

        @functools.wraps(func)
        def wrapped(**kwargs):
            def f(n, fs):
                return func(n=n, fs=fs, **kwargs)
            return cls.create(func=f, name=name)

        # Restoring the original names of the arguments of func.
        # Assuming the first two to be n and fs, thus removing them from the
        # signature.
        sign = sign.replace(parameters=tuple(sign.parameters.values())[2:])
        wrapped.__signature__ = sign

        return wrapped

    @classmethod
    def make(cls, name=""):
            # -> t.Callable[[t.Callable[[t.Any,...], np.ndarray]], t.Callable[[t.Any,...], Source]]:
        """To be used as a decorator that constructs a Source instance from a
        function definition, with a name argument.

        """

        def decorator(func):
            return cls.wrap(func=func, name=name)
        return decorator


null = Source(None, "Null")


@Source.make(name="ThermalNoise")
def thermal_noise(n: int, fs: float,
                  temperature: float=290.0,
                  resistance: float=50.0) -> np.ndarray:
    """A function that generates thermal noise samples with given
    parameters.  In an ideally anti-aliased sampling process, the sampling
    bandwidth will be half of the sampling, this process uses that information.
    
    Args:
        temperature: Temperature of the element that noise originates from,
            specified in `Kelvin`.
        resistance: Resistance of the element that the noise originates from,
            specified in `Ohm`.

    SamplerArgs:
        n: Number of samples.
        fs: Sampling frequency, specified in `Hz`.

    Returns:
        s: Sampled data.
    
    """
    bw = fs/2
    sigma = np.sqrt(johnson_noise_power(temperature, bw)*resistance)
    s = np.random.normal(size=n, loc=0, scale=sigma)
    return s


@Source.make(name='Sine')
def sine(n:int, fs: float, 
              amplitude: float, frequency: float, phase: float=0):
    """Simple function that returns a sine signal.

    """
    t = np.arange(n)/fs
    return amplitude*np.sin(2*np.pi*frequency*t + phase)


def psd_sampler(
        n: int, fs: float, func: t.Callable[[float], float],
        total_power: float = 1,
        initial_phase: t.Union[str, float, np.ndarray] = "random",
        resistance: float = 50):
    """Returns an array of given time domain samples from the power spectral
    density function given.  The samples are voltages for the power dissipated
    over a 50 ohm resistor.

    Args:
        n: Number of samples.
        fs: Sampling rate.
        func: PSD function, assumed to be normalized to 1 under integral.
        total_power: Total integrated power. PSD is multiplied with this value.
        initial_phase: Initial phases given to frequency points in the PSD
            value.  It can be "random", or a `float` representing any real
            value, or a 1-D array of size `n//2 + 1` as an assignemnt of phase
            to each frequency value in the PSD array.
        resistance: Resistance of the resistor of which the power was
            dissipated.

    Note:
        This method may introduce errors if n is not high enough.  A better
        method may use the integration of PSD in adjacent bins or filtering a
        random input with the shape of PSD.

    """
    nrfft = n//2 + 1
    f_nyq = fs/2
    freq = np.linspace(0, f_nyq, nrfft)
    # We need to treat the distribution as power spectral density.
    p_mag_sss = total_power*func(freq)
    dt = 1/fs
    T = n*dt
    # Converting from a power spectral density to an fft spectrum.
    p_mag_rfft = ((p_mag_sss*T)/dt**2)/2
    # 2 in denominator since we only care about positive frequencies.

    v_mag_sss = np.sqrt(p_mag_rfft*resistance)
    
    j = complex(0,1)
    if initial_phase == "random":
        p_phases = np.random.uniform(0, 2*np.pi, nrfft)
    elif isinstance(initial_phase, Number):
        p_phases = np.ones(nrfft)*initial_phase
    elif isinstance(initial_phase, np.ndarray):
        p_phases = initial_phase
    else:
        raise ValueError(f"Unknown phase choice \"{initial_phase}\".")

    p_phases[0] = 0  # DC phase is always zero
    v_sss = v_mag_sss*np.exp(j*p_phases)

    # to get odd number of points n must be specified (see docs for irfft)
    v_td = irfft(v_sss, n=n)  # this does *sqrt(N) normalization
    return v_td


@Source.make(name='AxionGalaxy')
def baseband_axion_galaxy(
        n: int, fs: float, loc: float, total_power: float,
        f_axion: float, betarms: float = BETARMS,
        resistance: float = 50):
    """Creates the time domain sampler for an axion signal corresponding to the
    galaxy frame, downconverted ideally to baseband at an offset given by
    `loc`."""
    func = get_dist_restframe(f_axion, betarms, loc=loc)
    v_td = psd_sampler(n, fs, func, total_power, "random", resistance)
    return v_td


@Source.make(name='AxionLab')
def baseband_axion_lab(
        n: int, fs: float, loc: float, total_power: float,
        f_axion: float, betarms: float = BETARMS, r: float = RSUN,
        resistance: float = 50):
    """Creates the time domain sampler for an axion signal corresponding to the
    laboratory frame, downconverted ideally to baseband at an offset given by
    `loc`."""
    func = get_dist_labframe(f_axion, betarms, r, loc)
    v_td = psd_sampler(n, fs, func, total_power, "random", resistance)
    return v_td


@Source.make(name='AxionGalaxyPeriodic')
def baseband_axion_galaxy_periodic(
        n: int, fs: float,
        period: float,
        loc: float, total_power: float,
        f_axion: float, betarms: float = BETARMS,
        initial_phase: float = 0, resistance: float = 50):
    """Returns a periodic sampler for baseband axion signal in galaxy frame."""
    nseg = int(np.floor(period*fs))
    func = get_dist_restframe(f_axion, betarms, loc=loc)
    v_td = psd_sampler(nseg, fs, func, total_power, initial_phase, resistance)
    return repeating_array(split_reverse_combine(v_td), n)


@Source.make(name='AxionLabPeriodic')
def baseband_axion_lab_periodic(
        n: int, fs: float,
        period: float,
        loc: float, total_power: float,
        f_axion: float, betarms: float = BETARMS, r: float = RSUN,
        initial_phase: float = 0, resistance: float = 50):
    """Returns a periodic sampler for baseband axion signal in galaxy frame."""
    nseg = int(np.floor(period*fs))
    func = get_dist_labframe(f_axion, betarms, r, loc)
    v_td = psd_sampler(nseg, fs, func, total_power, initial_phase, resistance)
    return repeating_array(split_reverse_combine(v_td), n)


if __name__ == "__main__":
    import doctest
    doctest.testmod()

import timeit

import matplotlib.pyplot as plt
import numpy as np
from numpy_groupies import aggregate
from chapp import ppopen
from chapp.blockcomb import BlockCombSimulator
from chapp.plotting import signoiseplot
import xarray as xr
from loguru import logger
from datargs import parse, argsclass, arg


@argsclass
class ProgOpts:
    nsim: int = 1000
    seed: int = 1234
    istart: int = 0
    saveevery: int = 100


opts = parse(ProgOpts)

nsim = opts.nsim

saveevery = opts.saveevery


SEED = opts.seed

istart = opts.istart


ppr = ppopen(
    "../hep1-2_pp1s6c.nc",
    name_map={"gmsqr": "gmsqr2"},
    chunks={"grunstep": 100},
    _bfield=6.965,
    _volume=3.103e-3,
)

shapefa = 5.9e9


blks = BlockCombSimulator.make(ppr, "../hep1-2_pp1s6c.blkc.nc", shapefa=shapefa)

# Preparing signal frequencies
gds = xr.open_dataset("../hep1-2_pp1s6c.blkc.nc")
# get ready for mindfunk
dfmagic = -3400  # fsc - 3400 gives the snr peaks.
fsc = ppr.delta.fsc.compute()
fpeaks_ = fsc.data + dfmagic
fvalleys_ = fsc.data + 2e3
fmin, fmax = gds.fbin[[0, -1]]
faxion_sections = np.arange(fmin + 1e6, fmax - 1e6, 1e6)
fpeakidx = np.digitize(fpeaks_, faxion_sections)
fvalleyidx = np.digitize(fvalleys_, faxion_sections)
fpeaks = aggregate(
    fpeakidx, fpeaks_, "first", fill_value=np.nan, size=len(faxion_sections) + 1
)[1:-1]
fvalleys = aggregate(
    fvalleyidx, fvalleys_, "first", fill_value=np.nan, size=len(faxion_sections) + 1
)[1:-1]


_fa_l = np.sort(np.hstack([fpeaks[0::2], fvalleys[1::2]]))
fa_l = _fa_l[np.where(~np.isnan(_fa_l))]

np.savetxt("testfa.csv", fa_l)


blks.prepare()

blks.set_seed(SEED)

for i in range(istart, istart + nsim // saveevery):
    for j in range(saveevery):
        gdsn_, gdsns_, gdsne_, gdsnse_ = blks.simulate_all(fa_l)
        if j == 0:
            gdsn = gdsn_
            gdsns = gdsns_
            gdsne = gdsne_
            gdsnse = gdsnse_
        else:
            gdsn = xr.concat([gdsn, gdsn_], dim="sim")
            gdsns = xr.concat([gdsns, gdsns_], dim="sim")
            gdsne = xr.concat([gdsne, gdsne_], dim="sim")
            gdsnse = xr.concat([gdsnse, gdsnse_], dim="sim")

    gdsn.to_netcdf(f"gds_n_{i}.nc")
    gdsns.to_netcdf(f"gds_ns_{i}.nc")
    gdsne.to_netcdf(f"gds_ne_{i}.nc")
    gdsnse.to_netcdf(f"gds_nse_{i}.nc")


# fi = 130000
# window = 20000
# mydelta = gds.delta_n.isel(fbin=slice(fi, fi + window))
# signoiseplot(mydelta.fbin.data, mydelta.data, bins=50)


# delta1 = blkc.generate_noise_delta().compute()
# delta2 = blks.generate_noise_delta(True).compute()

# fig, (ax1, ax2) = plt.subplots(nrows=2, sharex=True, sharey=True)
# delta1.sel(grunstep=0).plot(ax=ax1)
# delta2.sel(grunstep=0).plot(ax=ax2)

# plt.subplots_adjust(wspace=0.1)

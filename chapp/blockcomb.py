"""This module provides block combination procedures.

TODO
    - Check the dnu order in data when constructing objects.
"""
from io import StringIO
from pathlib import Path
from typing import Callable, Dict, Optional, Sequence

import attr
import numpy as np
import scipy.constants as cnst
import xarray as xr
from xarray.core.rolling import DataArrayCoarsen
from groundcontrol import units as un
from groundcontrol.declarators import from_json, setting, to_json
from loguru import logger
from numpy.random import Generator, default_rng
from numpy_groupies import aggregate_nb as aggregate
from sklearn.neighbors import KDTree

from chapp.physics import DEFAULT_AXIONDM_CONTEXT, axiondm_context
from chapp.preprocessors import PreprocessResult, calc_corr_uncorr_sigmas
from chapp.preprocessors.pp1 import apply_sg_filter


def drop_center_dnu(da: xr.DataArray, ndrop: int, fstep: float):
    """This actually does not drop but nan-ifies the center dnus."""
    nhalf = ndrop // 2
    # check whether this particular dropping method can be applied or not
    # np.all(da.sel(dnu=np.arange(-nhalf, nhalf + 1) * fstep).isnull()):
    dnu2drop = np.arange(-nhalf, nhalf + 1) * fstep
    dnu2keep = da.dnu.drop_sel(dnu=dnu2drop)
    keepmask = dnu2keep.reindex_like(da.dnu) == da.dnu
    return da.where(keepmask)


def _apply(fun: Callable, da: xr.DataArray):
    return xr.apply_ufunc(fun, da, dask="allowed")


def _check_dnu_symm(dnu):
    dnup = dnu.sel(dnu=slice(0, None)).data
    dnum = dnu.sel(dnu=slice(0, None, -1)).data
    symdnu = np.all(np.isclose(dnup, -dnum))
    return symdnu


@attr.frozen
class _SidebandCoarsen:
    _left: DataArrayCoarsen
    _right: DataArrayCoarsen

    def _apply(self, funname, *args, **kwargs):
        # Note: slice(None, 0) would also select the negative part, but the order
        # would be ascending.  For this case, I want descending order.
        left = getattr(self._left, funname)(*args, **kwargs)  # .sel(dnu=slice(None, 0))
        left = left.isel(dnu=slice(None, None, -1))  # Important!  Not, (0, None, -1) !!
        # If speed is a concern, use [..., ::-1] instead of the last selection.
        right = getattr(self._right, funname)(*args, **kwargs)
        return left.combine_first(right)

    def sum(self, *args, **kwargs):
        return self._apply("sum", *args, **kwargs)

    def mean(self, *args, **kwargs):
        return self._apply("mean", *args, **kwargs)

    def reduce(self, *args, **kwargs):
        return self._apply("reduce", *args, **kwargs)

    @classmethod
    def from_dataarray(cls, da, *args, **kwargs):
        right = da.sel(dnu=slice(0, None)).coarsen(*args, **kwargs)
        left = da.sel(dnu=slice(0, None, -1)).coarsen(*args, **kwargs)
        return cls(left, right)


@attr.define
class BlockCombParams:
    n_reb: int = setting(un.nounit, "Number of bins to rebin.")
    n_blk: int = setting(un.nounit, "Number of bins in a block.")
    ndrop: int = setting(
        un.nounit, "Number of bins to drop from the center before rebinning."
    )
    wf_fa: float = setting(
        un.hertz, "Frequency of the axion lineshape to be used in analysis."
    )
    rebin_sbsym: bool = setting(
        un.nounit, "If True, rebinning is done in a sideband symmetric way."
    )
    wfpath: Path = setting(un.nounit, "Path to the waveform bank.")
    mask_waveform_mirror: bool = setting(
        un.nounit,
        "Whether or not to mask the mirror part in the waveform, this is experimental"
        " but may become the default.",
        default=False,
    )

    @classmethod
    def from_json(cls, fp: [str, Path]):
        # We may do sanity check on values here.
        if isinstance(fp, str):
            return from_json(StringIO(fp), cls)
        else:
            return from_json(fp, cls)


@attr.define
class BlockCombinationProcessor:
    params: BlockCombParams
    ppr: Optional[PreprocessResult]
    adctx: axiondm_context = DEFAULT_AXIONDM_CONTEXT

    _ds: Optional[xr.Dataset] = None
    _delta_set: Optional[xr.DataArray] = None

    @property
    def fstep(self):
        return self.ppr.delta.attrs["_fstep"]

    @property
    def _fbin(self):
        # return self.ppr.pqa_mu.fbin  # This may contain NaN.
        delta = self.ppr.delta
        return delta.fsc + delta.dnu

    def prep_delta(self, delta, sigma_ss: Optional[xr.DataArray] = None):
        """Removes center `ndrop` bins by setting them to nan."""
        fstep = self.fstep

        delta_ = drop_center_dnu(delta, self.params.ndrop, fstep)

        if sigma_ss is None:
            logger.info("Calculating σ_ß.")
            sigma_ss = self.calc_sigma_ss()

        logger.info("Calculating δ_ß.")
        delta_ss = (delta_ + delta_.assign_coords(dnu=-delta_.dnu)) / sigma_ss

        logger.info("Rebinning δ_ß.")
        # Dividing by sqrt(n_reb) to account for the standard deviation
        delta_ss_r = self._coarsen(delta_ss).sum(skipna=False) / np.sqrt(
            self.params.n_reb
        )
        return delta_ss_r

    def prepare_waveform_data(
        self,
        dnua: np.ndarray,
        df: np.ndarray,
        mask: np.ndarray,
        maskmirror: bool = False,
    ):
        p_ = self.params
        wfda = xr.open_dataarray(p_.wfpath)
        logger.warning(
            "Rebinning waveform data by assuming it has the same resolution as the"
            " experiment data."
        )
        wfda_r = (
            (
                wfda.sel(nu_a=p_.wf_fa, method="nearest")
                .coarsen(df=p_.n_reb, coord_func="min", boundary="trim")
                .sum()
            )
            .rename("h1")
            .stack(aux=["dnu_a", "df"])
            .reset_index("aux")
            .reset_coords(["dnu_a", "df"])
        )
        coords = wfda_r[["dnu_a", "df"]].to_array().data.T

        if maskmirror:
            mdnua = -wfda_r.dnu_a
            mirrormask = (wfda_r.df > mdnua) & (wfda_r.df <= 2 * mdnua)
            h1vals = wfda_r["h1"].where(~mirrormask).data
            logger.info("Waveform mirror section is masked.")
        else:
            h1vals = wfda_r["h1"].data

        dnua_ = np.where(mask, -1, dnua)
        df_ = np.where(mask, -1, df)

        tree = KDTree(coords, leaf_size=2)
        points = np.vstack([dnua_, df_]).T
        h1idx = tree.query(points)[1]
        datah1 = np.where(mask, np.nan, np.squeeze(h1vals[h1idx]))
        dnua_q, df_q = np.squeeze(coords[h1idx]).T

        # Principle of least astonishment
        # the quantized arrays should contain same invalid numbers as the
        # non-quantized arrays.
        dnua_q_ = np.where(mask, dnua, dnua_q)
        df_q_ = np.where(mask, df, df_q)
        return datah1, dnua_q_, df_q_

    def prepare(self):
        ppr = self.ppr

        logger.info("Calculating σ_ß.")
        sigma_ss = self.calc_sigma_ss()
        # logger.debug(sigma_ss.sel(dnu=slice(-1e3, 1e3)))

        logger.info("Preparing δ_ß.")
        delta_ss_r = self.prep_delta(ppr.delta)

        gjsymdnu = _check_dnu_symm(ppr.gj.dnu)
        if not gjsymdnu:
            raise ValueError("G_J dataset has non-symmetric dnu coordinate.")

        logger.info("Assuming symmetry for gj.")
        gj = ppr.gj.interpolate_na(dim="dnu").interp_like(ppr.delta)
        gjsymm = (gj + gj.assign_coords(dnu=-gj.dnu)) / 2

        logger.info("Calculating C.")
        cfact = (gjsymm - 1) / gjsymm

        # Data arrays with non-stacked coordinates
        logger.info("Calculating R_ß.")
        r_ss = self.calc_r_ss(sigma_ss)

        # Rebin

        coarsen = self._coarsen
        n_reb = self.params.n_reb
        fstep_r = self.fstep * n_reb
        fbin = self._fbin

        logger.info("Rebinning.")
        fbin_r = coarsen(fbin).mean(skipna=False)
        # snrs add in quadrature
        r_ss_r = coarsen(r_ss).reduce(
            lambda arr, axis: np.sqrt(np.sum(arr ** 2, axis=axis)) / n_reb
        )
        # logger.debug(r_ss.dnu.sel(dnu=slice(0, None, -1)))
        # logger.debug(r_ss_r.dnu.data)

        cfact_r = coarsen(cfact).mean(skipna=False)

        # logger.debug(fbin.sizes)
        # logger.debug(cfact.sizes)
        # logger.debug(cfact_r.sizes)
        # logger.debug(fbin_r.sizes)
        # logger.debug(delta_ss_r.sizes)
        # logger.debug(r_ss_r.sizes)
        assert fbin.sizes == cfact.sizes
        assert cfact_r.sizes == fbin_r.sizes == delta_ss_r.sizes == r_ss_r.sizes

        logger.info("Flattening data and converting to simple arrays.")
        myds = xr.merge(
            [
                delta_ss_r.rename("delta"),
                r_ss_r.rename("snr"),
                cfact_r.rename("cfact"),
            ],
            join="exact",
        ).assign_coords(fbin=fbin_r)
        flatds = myds.stack(aux=["grunstep", "dnu"])
        fbin_rf = flatds.fbin.compute().data

        fmin, fmax = fbin_rf.min() - fstep_r / 2, fbin_rf.max() + fstep_r / 2

        n_blk = self.params.n_blk
        fstep_g = n_blk * fstep_r

        gfstepedges = np.arange(fmin, fmax, fstep_g)

        # This is the bin edge matrix for grand spectrum
        # Each row is an fstep_r shift of the previous row.
        gfbin_edge_mat = np.add.outer(np.arange(n_blk) * fstep_r, gfstepedges)

        logger.info("Starting to group bins into blocks..")
        gidx_mat = np.apply_along_axis(
            lambda bins, x, right: np.digitize(x, bins, right),
            axis=1,
            arr=gfbin_edge_mat,
            right=False,
            x=fbin_rf,
        )
        assert gidx_mat.shape[1] == len(fbin_rf)

        logger.info("Grouping finished.")
        # Rows of this matrix correspond to the rolling of the original
        # grouping.  If we have the following array:
        # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ...]
        # We create groups as follows:
        # Grouping 1:  [| 0, 1, 2, 3,| 4, 5, 6, 7,| 8, 9, 10, 11,| ...]
        # Grouping 2:  [0,| 1, 2, 3, 4,| 5, 6, 7, 8,| 9, 10, 11, ...]
        # Grouping 3:  [0, 1,| 2, 3, 4, 5,| 6, 7, 8, 9,| 10, 11, ...]
        # Edge vector of each group correpsond to a row in the gfbin_edge_mat.
        # I will refer to row axis as "ir".  There are total of n_blk
        # rolls, length of row axis is n_blk - 1.

        # Infinity is used to label the values fall outside the bins, +inf for
        # values higher than the rightmost bin, -inf for values lower than the
        # leftmost bin.
        infvec = np.repeat(np.infty, n_blk)[:, np.newaxis]
        # IMPORTANT: Labeling should be done with the leftmost edges.
        # That is why i am using `gfbin_edge_mat[..., :-1]`
        # This is how df in waveform bank is defined.
        gfbin_label_mat = np.hstack([-infvec, gfbin_edge_mat[..., :-1], infvec])

        # Number of groups per row
        n_gpr = gfbin_label_mat.shape[1]
        assert n_gpr == len(gfstepedges) + 1

        # Each row in GIDX_MAT is an independent indexer for the corresponding row.
        # When we flatten GIDX_MAT, we should add an offset to each row so the
        # indexers stay unique.
        # Example:
        #    GIDX_MAT = [[0, 1, 3, 4, 1, 2, 2, 5],   (0 and 5 are outside)
        #                [0, 2, 1, 3, 4, 1, 5, 2]]]
        #    GIDX = [0, 1, 3, 4, 1, 2, 2, 5, 0+6, 2+6, 1+6, 3+6, 4+6, 1+6, 5+6, 2+6]
        # Here, 6 is the total number of bins including the excess bins to the
        # left and right, so 4 real bins + 2 excess.

        # Technically we are free to process each row in gidx_mat separately.
        # Here, I am opting for flattening which makes it easier to do
        # indexing.

        gidx = gidx_mat.reshape(-1) + np.repeat(np.arange(n_blk), len(fbin_rf)) * n_gpr
        gfbin = gfbin_label_mat.reshape(-1)[
            gidx
        ]  # group frequency labels in grand spec.
        # Each group refers to a case where axion assumes a certain frequency
        # given given by a value in this array.

        # Tiling corresponds to for loop during processing here.
        # I am simply trading off time for memory.
        fsc_t = np.tile(flatds.fsc.data, n_blk)
        fbin_t = np.tile(fbin_rf, n_blk)
        dnua = gfbin - fsc_t  # Axion frequency - spectrum center (fp/2)
        df = fbin_t - gfbin  # Measurement frequency - axion frequency

        # These are the bins that fall outside the range for each roll.
        invalidbins = np.isinf(gfbin)

        logger.info("Mapping waveform shape to data.")
        h1, dnua_q, df_q = self.prepare_waveform_data(
            dnua, df, invalidbins, self.params.mask_waveform_mirror
        )

        logger.info("Merging the intermediate dataset.")
        idxds = xr.Dataset(
            {
                "gidx": (["ri", "mid"], gidx_mat),
                "sig": (["ri", "mid"], h1.reshape(gidx_mat.shape)),
                "dnua_q": (["ri", "mid"], dnua_q.reshape(gidx_mat.shape)),
                "df_q": (["ri", "mid"], df_q.reshape(gidx_mat.shape)),
                "gfbin": (["ri", "mid"], gfbin.reshape(gidx_mat.shape)),
            }
        )

        dds = xr.merge([myds, idxds], join="outer", combine_attrs="drop")
        parstr = to_json(self.params, pretty=False)
        dds.attrs["params"] = parstr
        dds.attrs["n_gpr"] = n_gpr
        dds.attrs["n_blk"] = n_blk
        dds.attrs["fstep"] = self.fstep
        dds.attrs["fstep_r"] = fstep_r
        dds.attrs["fstep_g"] = fstep_g
        self._ds = dds

    def save_prep(self, path: Path, comment: str = "", mode="a"):
        return self._ds.assign_attrs(comment=comment).to_netcdf(
            path, mode=mode, group="prep"
        )

    def _coarsen(self, da: xr.DataArray):
        n_reb = self.params.n_reb
        if self.params.rebin_sbsym:
            logger.debug("sideband coarsen.")
            return _SidebandCoarsen.from_dataarray(
                da, dnu=n_reb, boundary="trim", coord_func="mean"
            )
        else:
            return da.coarsen({"dnu": n_reb}, boundary="trim", coord_func="mean")

    def calc_signal_power(self) -> xr.DataArray:
        ppr = self.ppr
        cavds = ppr.cavds
        return self.adctx.signal_power(
            self._fbin,
            cavds.fcav,
            cavds.q_l,
            cavds.beta,
            cavds.formfactor,
            ppr.bfield,
            cavds.volume,
        )

    def calc_noise_power(self):
        return cnst.Boltzmann * self.fstep * self.ppr.tsys

    def calc_sigma_ss(self, drop_center: bool = True):
        """Dropping center ensures that SNR at the center bins are NaN as well, as they
        should not contribute to the final SNR."""
        onesided = _apply(np.sqrt, 2 * (1 + self.ppr.gmsqr))
        twosided = (
            (
                onesided.assign_coords(dnu=-onesided.dnu)
                .drop_sel(dnu=0)
                .combine_first(onesided)
                .interpolate_na(dim="dnu", method="spline", s=0)
            )
            .interp_like(self.ppr.delta)
            .rename("sigma")  # important
        )

        if drop_center:
            return drop_center_dnu(twosided, self.params.ndrop, self.fstep)
        else:
            return twosided

    def calc_r_ss(self, sigma_ss: xr.DataArray, r_da: Optional[xr.DataArray] = None):
        if r_da is None:
            r_da = self.calc_r_da()
        return r_da * 2 / sigma_ss

    def calc_r_da(
        self,
        sigpow: Optional[xr.DataArray] = None,
        noisepow: Optional[xr.DataArray] = None,
    ):
        if sigpow is None:
            sigpow = self.calc_signal_power()

        if noisepow is None:
            noisepow = self.calc_noise_power()

        sqrt_navg = _apply(np.sqrt, self.ppr.navg)
        return sqrt_navg * sigpow / noisepow

    def get_grouping_dataset(self, fancy_dims: bool = False):
        ds = self._ds[["gidx", "sig", "dnua_q", "df_q", "gfbin"]]
        if not fancy_dims:
            return ds
        else:
            n_gpr = self._ds.attrs["n_gpr"]
            n_blk = self._ds.dims["ri"]
            n_bins = self._ds.dims["mid"]
            grunstep = np.tile(
                self._ds.grunstep.broadcast_like(self._ds.dnu)
                .transpose("grunstep", "dnu")
                .data.reshape(-1),
                n_blk,
            )
            # run = np.tile(self._ds.run.data.reshape(-1), n_blk)
            # runstep = np.tile(self._ds.runstep.data.reshape(-1), n_blk)
            # fsc = np.tile(self._ds.fsc.data, n_blk)
            gidx_g = (
                ds.gidx.data.reshape(-1) + np.repeat(np.arange(n_blk), n_bins) * n_gpr
            )
            dss = ds.stack(aux=["ri", "mid"]).assign_coords(
                grunstep=("aux", grunstep),
                run=("grunstep_", self._ds.run.data),
                runstep=("grunstep_", self._ds.runstep.data),
                fsc=("grunstep_", self._ds.fsc.data),
                gidx_g=("aux", gidx_g),
            )

            return dss.set_index(aux=["gfbin", "grunstep", "df_q"])

    def get_prepared_dataset(self):
        return self._ds[["delta", "snr", "cfact"]]

    def process(self, with_delta_ss: Optional[xr.DataArray] = None):
        ds = self.get_prepared_dataset()
        gidxds = self.get_grouping_dataset()

        logger.info("Proceeding delayed computations if chunking was used.")
        csnr = (0.5 * (1 + ds.cfact) * ds.snr).compute()
        delta = ds.delta.compute() if with_delta_ss is None else with_delta_ss.compute()

        # TODO: Check dimension ordering to be 'grunstep', 'dnu'
        delta_flat = delta.data.reshape(-1)
        csnr_flat = csnr.data.reshape(-1)

        # This is groups-per-row.  Remember each row is a fstep_r shift of previous one.
        n_gpr = ds.attrs["n_gpr"]
        n_valid_per_row = n_gpr - 2  # removing infinite bins

        nrow = gidxds.dims["ri"]

        snr_g = np.empty((nrow, n_valid_per_row), dtype=float)
        delta_g = np.empty((nrow, n_valid_per_row), dtype=float)
        fbin_g = np.empty((nrow, n_valid_per_row), dtype=float)

        for rid, rowds in gidxds.groupby("ri"):
            logger.info(f"Processing roll {rid+1}/{nrow}.")
            csnrsig = csnr_flat * rowds.sig.data
            snrsqred = csnrsig ** 2
            # snrxdelta = snrsqred * delta_flat
            snrxdelta = csnrsig * delta_flat

            gidx = rowds.gidx.data
            gsnr = np.sqrt(
                aggregate(gidx, snrsqred, func="nansum", size=n_gpr, fill_value=np.nan)
            )
            gdelta = aggregate(
                gidx, snrxdelta, func="nansum", size=n_gpr, fill_value=np.nan
            )
            gfbin = aggregate(
                gidx, rowds.gfbin, func="first", size=n_gpr, fill_value=np.nan
            )

            # Note that we forced the size to contain bin values corresponding to -inf
            # and +inf bins (outside bins).  We can cut them here from further
            # propagation.
            snr_g[rid, :] = gsnr[1:-1]
            delta_g[rid, :] = gdelta[1:-1]
            fbin_g[rid, :] = gfbin[1:-1]

        logger.info("Flattening the processed data.")
        # Flattening in a way to ensure sortedness
        snr_g_flat = snr_g.transpose().reshape(-1)
        delta_g_flat = delta_g.transpose().reshape(-1)
        fbin_g_flat = fbin_g.transpose().reshape(-1)

        delta_n_g_flat = delta_g_flat / snr_g_flat

        logger.info("Constructing the grand spectrum.")
        gds = xr.Dataset(
            {
                "snr": ("fbin", snr_g_flat),
                "delta": ("fbin", delta_g_flat),
                "delta_n": ("fbin", delta_n_g_flat),
            },
            coords={"fbin": ("fbin", fbin_g_flat)},
        ).assign_attrs(params=ds.attrs["params"])
        return gds

    @classmethod
    def make(
        cls,
        ppr: PreprocessResult,
        params: BlockCombParams,
        axiondmctx: axiondm_context = DEFAULT_AXIONDM_CONTEXT,
    ):
        return cls(params, ppr, axiondmctx)

    @classmethod
    def from_prep(
        cls, path: Path, axiondmctx: axiondm_context = DEFAULT_AXIONDM_CONTEXT
    ):
        ds = xr.open_dataset(path, group="prep")
        params = BlockCombParams.from_json(ds.attrs["params"])
        return cls(params, None, axiondmctx, ds=ds)


@attr.define
class BlockCombSimulator:
    ppr: PreprocessResult
    blc: BlockCombinationProcessor

    prepds: Optional[xr.Dataset] = None
    prepds2: Optional[xr.Dataset] = None
    shapefa: Optional[float] = None
    _rng: Generator = attr.Factory(default_rng)
    _blfitparams: Optional[Dict[str, float]] = None
    _pqa_mu: Optional[xr.DataArray] = None
    _waveform: Optional[np.ndarray] = None

    # HACK!!! FIX ASAP
    _signal: Optional[xr.DataArray] = None

    def set_seed(self, seed):
        self._rng = default_rng(seed)

    def prepare(self):
        ppr = self.ppr
        gmsqr = ppr.gmsqr

        logger.info("Preparing gj.")
        gj = ppr.gj
        gjsymdnu = _check_dnu_symm(gj.dnu)
        if not gjsymdnu:
            raise ValueError("G_J dataset has non-symmetric dnu coordinate.")

        gjsymm = ((gj + gj.assign_coords(dnu=-gj.dnu)) / 2).interpolate_na(dim="dnu")

        logger.info("Calculating C.")
        cfact = ((gjsymm - 1) / gjsymm).interp_like(gmsqr)

        logger.info("Preparing uncorrelated and correlated sigma data.")
        # NOTE! gmsqr is half spectrum.
        sigma_c_sqr_, sigma_u_sqr_ = calc_corr_uncorr_sigmas(gmsqr, cfact)
        sigma_c_, sigma_u_ = _apply(np.sqrt, sigma_c_sqr_), _apply(
            np.sqrt, sigma_u_sqr_
        )

        # COMPUTE IS IMPORTANT TO BE HERE
        onesided = xr.Dataset({"sigma_c": sigma_c_, "sigma_u": sigma_u_}).compute()
        twosided = (
            onesided.assign_coords(dnu=-onesided.dnu)
            .drop_sel(dnu=0)
            .combine_first(onesided)
        )
        twosided1 = twosided.reindex(dnu=ppr.delta.dnu).interpolate_na(
            dim="dnu", method="spline", s=0
        )

        # for bl removal we need different dnu
        logger.info("Preparing stuff for baseline removal related simulations.")
        span = ppr.delta.dnu[[0, -1]].diff("dnu").item()
        # sgfilterwindow = max(
        #     self.ppr.params.pqa_prefilt["sg_win"], self.ppr.params.pqa_sg_win
        # )
        sgfilterwindow = self.ppr.params.pqa_sg_win

        origspan = span + sgfilterwindow
        fstep = ppr.delta.dnu[[0, 1]].diff("dnu").item()
        logger.warning("Assuming odd number of bins in the original spectrum.")
        n = int(origspan // fstep)
        if (n % 2) == 0:
            n += 1
        dnuforblremoval = np.linspace(-origspan / 2, origspan / 2, n)
        twosided2 = twosided.reindex(dnu=dnuforblremoval).interpolate_na(
            dim="dnu", method="spline", s=0
        )
        logger.info(f"The dnu coordinate for baseline removal has {n} points.")

        self.prepds = twosided1
        self.prepds2 = twosided2

        logger.info("Preparing extrapolated pqa_mu for baseline embedding.")
        # TODO: Add this to prepds2 instead of _pqa_mu
        self._pqa_mu = self.ppr.pqa_mu.interp_like(
            self.prepds2, kwargs={"fill_value": "extrapolate"}
        ).compute()
        self.prepds["sqrt_navg"] = _apply(np.sqrt, ppr.navg)

        logger.info("Calculating per-runstep SNR to be used in signal injections.")
        snrda = self.blc.calc_r_da().compute()

        logger.info("Extending SNR estimation frequency range by extrapolation.")
        snrda2 = snrda.interp_like(self.prepds2, kwargs={"fill_value": "extrapolate"})

        assert self.prepds.sizes == snrda.sizes
        assert self.prepds2.sizes == snrda2.sizes
        self.prepds["snr"] = snrda
        self.prepds2["snr"] = snrda2

        logger.info("Calculating sigma_ss.")
        self.prepds["sigma_ss"] = self.blc.calc_sigma_ss()

        shapefa = self.shapefa
        if shapefa is not None:
            logger.info("Generating template waveform.")
            actx = self.blc.adctx
            wflen = 1001  # "just long enough sequence"
            wfdnu = actx.mkwfdomain(wflen, self.blc.fstep)
            self._waveform = actx.get_waveform_labframe(shapefa, wfdnu)

        pqa_prefilt = ppr.params.pqa_prefilt
        if pqa_prefilt is None:
            prewin = prepor = coarsen = None
        else:
            prewin = pqa_prefilt["sg_win"]
            prepor = pqa_prefilt["sg_porder"]
            coarsen = pqa_prefilt["coarsen"]

        self._blfitparams = {
            "sg_win": ppr.params.pqa_sg_win,
            "sg_porder": ppr.params.pqa_sg_porder,
            "pre_sg_win": prewin,
            "pre_sg_porder": prepor,
            "pre_coarsen": coarsen,
        }

        logger.info("Preparations finished.")

    def embed_baseline(self, delta: xr.DataArray):
        pqa_mu = self._pqa_mu
        sqrtnavg = self.prepds.sqrt_navg
        return (delta / sqrtnavg + 1) * pqa_mu

    def remove_baseline(self, pqa: xr.DataArray, with_ndrop: Optional[int] = None):
        """Arguments:
        pqa:  The power spectrum "with" the baseline.
        with_ndrop:  Drop this amount of bins from the center instead of the number
            used in the preprocess.
        """
        fstep = self.ppr.delta.attrs["_fstep"]
        ndrop = self.ppr.params.delta_ndrop if with_ndrop is None else with_ndrop
        nhalf = ndrop // 2
        dnu2drop = np.arange(-nhalf, nhalf + 1) * fstep

        # this is necessary due to a minor bug in apply_sg_filter
        pqa = pqa.assign_coords(grunstep=pqa.grunstep)
        logger.debug("applying filter as in preprocess.")
        fit = apply_sg_filter(
            pqa, primary_dim="grunstep", dnu2drop=dnu2drop, **self._blfitparams
        )
        logger.debug("computing delta.")
        delta = (pqa / fit - 1) * self.prepds.sqrt_navg
        return delta

    def generate_noise_delta(self, bl_removal: bool = False, noembed: bool = False):
        """
        Args:
            bl_removal:  Prepares and performs baseline removal on the generated dataset.
            noembed:  If this and `bl_removal`are both `True` no embedding and
                deembedding takes place, but returned dataset is prepared for the procedure.
                Use this if you want to embed/deembed later.

        """

        # logger.debug('preparing')
        rng = self._rng
        ppr = self.ppr
        delta = ppr.delta
        grunstep = delta.grunstep.data

        if bl_removal:
            prepds = self.prepds2
        else:
            prepds = self.prepds
        sigma_u = prepds.sigma_u.data
        sigma_c = prepds.sigma_c.data

        ndnu = prepds.sizes["dnu"]
        ngrunstep = len(grunstep)
        assert (ndnu % 2) == 1

        logger.debug("generating x_u")
        x_u = sigma_u * rng.standard_normal((ngrunstep, ndnu))

        logger.debug("generating xn_c_right")
        xn_c_right = rng.standard_normal((ngrunstep, ndnu // 2))
        logger.debug("generating xn_c_center")
        xn_c_center = rng.standard_normal((ngrunstep, 1))
        logger.debug("prepping xn_c_left")
        xn_c_left = np.fliplr(xn_c_right)

        logger.debug("combining x_c")
        x_c = sigma_c * np.hstack([xn_c_left, xn_c_center, xn_c_right])

        x = x_u + x_c

        if delta.chunks is None:
            chunks = {}
        else:
            chunks = {"grunstep": delta.chunksizes["grunstep"][0]}

        logger.debug("reindexing delta as a template")
        # this actually can be moved to preparations
        template = delta.compute().reindex(dnu=prepds.dnu)
        logger.debug("constructing and chunking dataarray")
        delta_sim = template.copy(deep=False, data=x).chunk(chunks)
        if bl_removal and (not noembed):
            logger.debug("Embedding baseline.")
            pqa_sim = self.embed_baseline(delta_sim)
            logger.debug("Removing baseline.")
            delta_sim_blrem = self.remove_baseline(pqa_sim)
            return delta_sim_blrem
        else:
            return delta_sim

    def generate_signal_delta(
        self,
        fa_l: Sequence[float],
        bl_removal: bool = False,
        noembed: bool = False,
        with_snr: Optional[xr.DataArray] = None,
    ):
        waveform = self._waveform
        if bl_removal:
            prepds = self.prepds2
        else:
            prepds = self.prepds
        ngrunstep = prepds.sizes["grunstep"]
        ndnu = prepds.sizes["dnu"]

        sigmat = np.zeros((ngrunstep, ndnu))

        step = self.blc.fstep
        for fa in fa_l:
            mask = np.abs(prepds.fbin.data - fa) < step
            idx = np.argwhere(mask)

            # Can np.put_along_axis be used to eliminate this loop?
            for grsid, dnuid in idx:
                indices = np.arange(dnuid, dnuid + len(waveform))
                np.put(sigmat[grsid], indices, waveform, mode="clip")

        delta = self.ppr.delta
        if delta.chunks is None:
            chunks = {}
        else:
            chunks = {"grunstep": delta.chunksizes["grunstep"][0]}

        logger.debug("reindexing delta as a template")
        template = prepds.snr
        logger.debug("constructing and chunking dataarray")
        sigda_ = template.copy(deep=False, data=sigmat)  # .chunk(chunks)

        if with_snr is not None:
            snr = prepds.snr
        else:
            snr = prepds.snr
        delta_sim = (
            (sigda_ * snr)
            .pipe(lambda da: da + da.assign_coords(dnu=-da.dnu))
            .chunk(chunks)
            .assign_coords(fbin=sigda_.fbin)
        )
        # we need to reassign fbin because its dropped during
        # sideband summation (xarray drops to resolve ambiguity)

        if bl_removal and (not noembed):
            logger.debug("Embedding baseline.")
            pqa_sim = self.embed_baseline(delta_sim)
            logger.debug("Removing baseline.")
            delta_sim_blrem = self.remove_baseline(pqa_sim)
            return delta_sim_blrem
        else:
            return delta_sim

    def simulate(
        self,
        bl_removal: bool = False,
        fa_l: Sequence[float] = (),
        scale_signal: float = 1,
        cachesignal: bool = True,
    ):
        injectaxion = fa_l != ()
        logger.info("Generating noise.")
        delta = self.generate_noise_delta(bl_removal, noembed=injectaxion)
        sigma_ss = self.prepds["sigma_ss"]

        if bl_removal and injectaxion:
            if (self._signal is None) or (not cachesignal):
                logger.info("Generating signal.")
                sig_ = self.generate_signal_delta(fa_l, bl_removal, noembed=injectaxion)
                self._signal = sig_
            elif cachesignal:
                sig_ = self._signal
            sig = sig_ * scale_signal
            delta_ = delta + sig
        else:
            delta_ = delta

        if bl_removal and injectaxion:
            logger.debug("Embedding baseline.")
            pqa_sim = self.embed_baseline(delta_)
            logger.debug("Removing baseline.")
            delta__ = self.remove_baseline(pqa_sim)
        else:
            delta__ = delta_

        logger.info("Preparing delta_ss.")
        delta_ss = self.blc.prep_delta(delta__, sigma_ss)

        logger.info("Processing.")
        gds = self.blc.process(with_delta_ss=delta_ss)
        return gds

    def simulate_all(
        self, fa_l: Sequence[float], scale_signal: float = 1, cachesignal: bool = True
    ):
        """Returns 4 processed grand spectra (noise data used is the same in all):

        gds_n: One from only noise spectra.
        gds_ns: One from noise + signal.
        gds_ne: One from noise, embedded and removed baseline.
        gds_nse: One from noise + signal, embedded and removed baseline.

        """
        sigma_ss = self.prepds["sigma_ss"]

        logger.info("Generating noise.")
        # this is indexed for baseline removal, therefore it has more bins
        delta_noise_ = self.generate_noise_delta(True, noembed=True)

        # this one is indexed for after baseline removal (less bins)
        delta_noise = delta_noise_.reindex_like(self.prepds.snr)

        if (self._signal is None) or (not cachesignal):
            logger.info("Generating signal.")
            sig_ = self.generate_signal_delta(fa_l, True, noembed=True)
            self._signal = sig_
        elif cachesignal:
            sig_ = self._signal
        sig = sig_ * scale_signal

        # For baseline removal
        delta_signoise_ = delta_noise_ + sig

        # For direct use
        delta_signoise = delta_signoise_.reindex_like(self.prepds.snr)

        logger.info("Embedding baseline on sig+noise.")
        pqa_sim_noise = self.embed_baseline(delta_noise_)
        logger.info("Removing baseline from sig+noise.")
        delta_noise_deembed = self.remove_baseline(pqa_sim_noise)

        logger.info("Embedding baseline on sig+noise.")
        pqa_sim_signoise = self.embed_baseline(delta_signoise_)
        logger.info("Removing baseline from sig+noise.")
        delta_signoise_deembed = self.remove_baseline(pqa_sim_signoise)

        logger.debug("Preparing delta_ss_noise.")
        delta_ss_noise = self.blc.prep_delta(delta_noise, sigma_ss)
        logger.info("Processing gds_n.")
        gds_n = self.blc.process(with_delta_ss=delta_ss_noise)

        logger.debug("Preparing delta_ss_signoise.")
        delta_ss_signoise = self.blc.prep_delta(delta_signoise, sigma_ss)
        logger.info("Processing gds_ns.")
        gds_ns = self.blc.process(with_delta_ss=delta_ss_signoise)

        logger.debug("Preparing delta_ss_noise_deembed.")
        delta_ss_noise_deembed = self.blc.prep_delta(delta_noise_deembed, sigma_ss)
        logger.info("Processing gds_ne.")
        gds_ne = self.blc.process(with_delta_ss=delta_ss_noise_deembed)

        logger.debug("Preparing delta_ss_signoise_deembed.")
        delta_ss_signoise_deembed = self.blc.prep_delta(
            delta_signoise_deembed, sigma_ss
        )
        logger.info("Processing gds_nse.")
        gds_nse = self.blc.process(with_delta_ss=delta_ss_signoise_deembed)

        return gds_n, gds_ns, gds_ne, gds_nse

    @classmethod
    def make(
        cls, ppr: PreprocessResult, blkcomb_prep: Path, shapefa: Optional[float] = None
    ):
        blc = BlockCombinationProcessor.from_prep(blkcomb_prep)
        blc.ppr = ppr
        return cls(ppr, blc, shapefa=shapefa)

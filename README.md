# CHAPP is a Haloscope Analysis Programming Package (CHAPP)
## What is it?
CHAPP is a software package that aims to collect Axion Haloscope related
analysis strategies and tools into a single library that is reusable and
extensible.

## Contributing
Git workflow and how to send requests will be updated when the project gets a
bit more mature.

### Language & Style
The project's overall language is Python. For styling the code in the project
should follow the Google's python style and language guidelines that can be
found [here][1]. Also, try to use docstring compatible commenting if possible.
An example how to do that can be found [here][2].  Also, it is good practice to
use explicit typing wherever possible.  If using the typing style, no need to
provide types in docstrings.

For starters, at least make sure that you do the following:
* Always use 4 spaces for indentation. Make sure that you understand what your
  particular editor does when you press tab.
* No line should exceed 79 characters.
* Provide explanation for functions/methods unless the naming is very
  straightforward.

### attrs
`attrs` is a package that lets you write concise and self descriptive programs
with LESS typing and almost ZERO computational overhead.  I try to use it
whenever I need to use classes.  If you encounter some related code that you
want to change, you may get used to how it works a bit by looking at the 
examples provided [here][5]

### Doctests
Doctests are little snippets of code that you put in your source files, that are
supposed to be a working example of whatever your code is supposed to do.   They
are easy to do because you need to write something to test your code anyways, so
instead of doing that in a python shell, you just write it inside your source
and then run (for example):
```bash
python3 -m doctest chapp/sim/sources.py
```
This runs the example snippets contained in the file chapp/sim/sources.py, and
produces a simple result for you to see if any of the line in the snippet fails.

While doctests can't replace some rigorous testing covering corner cases, it's
fast and simple to write.  Usually, we don't have time to write tests for
everything we do, but we try them anyways.  If you write your `trials` as
doctests, then other people can try and learn from those examples this way.
See [this][3] for more information on how to use `doctest` module.

### Unittests
Unit tests are for more rigorous testing.  It's not required to write these for
whatever you are contributing, but you should conform to some standards if you
are choose to write your unit tests for your module.  Check [this][4] site for
examples of usage.

[1]: https://google.github.io/styleguide/pyguide.html
[2]: http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html
[3]: https://pymotw.com/3/doctest/
[4]: https://pymotw.com/3/unittest/index.html#module-unittest
[5]: https://www.attrs.org/en/stable/ 

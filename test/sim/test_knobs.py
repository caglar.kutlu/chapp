import unittest

from chapp.sim.analyzer import Knobs, ModernKnobs, BasebandKnobs
from chapp.sim.windowing import windows


class TestKnobs(unittest.TestCase):
    def setUp(self):
        self.Knobs = Knobs

    def test_span_center_creation_basic(self):
        _span, _center = 1e6, 2e6
        knob = self.Knobs.create(span=_span, center_frequency=_center)
        span = knob.span
        center = knob.center_frequency
        self.assertEqual(span, _span)
        self.assertEqual(center, _center)

    def test_start_stop_creation_basic(self):
        _start, _stop = 3e6, 5e6
        knob = self.Knobs.create(
                start_frequency=_start,
                stop_frequency=_stop)
        start = knob.start_frequency
        stop = knob.stop_frequency
        self.assertEqual(start, _start)
        self.assertEqual(stop, _stop)

    def test_start_only_creation(self):
        _start = 2e6
        knob = self.Knobs.create(start_frequency=_start)
        start = knob.start_frequency
        self.assertNotEqual(knob, None)
        self.assertEqual(start, _start)


class TestModernKnobs(TestKnobs):
    def setUp(self):
        self.Knobs = ModernKnobs

    def test_auto_overlap_value(self):
        knobs: ModernKnobs = self.Knobs.create()
        knobs.set_auto_overlap(True)
        knobs.window = windows.BH92
        current_overlap = knobs.overlap
        knobs.window = windows.Rectangular
        new_overlap = knobs.overlap
        # Check if it changes
        self.assertNotEqual(new_overlap, current_overlap)

        # Turning off overlap, last window was Rectangular
        last_overlap = new_overlap
        knobs.set_auto_overlap(False)
        knobs.window = windows.BH92
        self.assertEqual(knobs.overlap, last_overlap)

    def test_auto_overlap_rectangular(self):
        # Check if rectangular has 0 overlap as wanted
        knobs: ModernKnobs = self.Knobs.create()
        knobs.set_auto_overlap(True)
        knobs.window = windows.BH92
        # current_overlap = knobs.overlap
        knobs.window = windows.Rectangular
        new_overlap = knobs.overlap
        # Check if it changes
        self.assertEqual(new_overlap, 0)

    def test_auto_swt_value(self):
        _start, _stop = 1e6, 2e6
        knob = self.Knobs.create(
                rbw=100, start_frequency=_start,
                stop_frequency=_stop)
        window = knob.window

        rbwset = 50
        knob.rbw = rbwset
        # 1/rbwset is rectangular window, lower than that doesn't make sense.
        swt_expected = max(window.n3dbbw/rbwset, 1/rbwset)
        # self.assertAlmostEqual(knob.swt, swt_expected, delta=1e-10)
        self.assertEqual(knob.swt, swt_expected)

        rbwset = 97.58392
        knob.rbw = rbwset
        swt_expected = max(window.n3dbbw/rbwset, 1/rbwset)
        # self.assertAlmostEqual(knob.swt, swt_expected, delta=1e-10)
        self.assertEqual(knob.swt, swt_expected)

    def test_start_stop_creation(self):
        _start, _stop = 3e6, 5e6
        _span = _stop - _start
        _cf = _start + _span/2
        knob = self.Knobs.create(
                start_frequency=_start,
                stop_frequency=_stop)
        start = knob.start_frequency
        stop = knob.stop_frequency
        span = knob.span
        cf = knob.center_frequency
        self.assertEqual(start, _start)
        self.assertEqual(stop, _stop)
        self.assertEqual(span, _span)
        self.assertEqual(cf, _cf)

    def test_start_span_creation(self):
        _start = 2e6
        _span = 1e6
        _stop = _start + _span
        _cf = _start + _span/2
        knob = self.Knobs.create(start_frequency=_start, span=_span)
        start = knob.start_frequency
        stop = knob.stop_frequency
        span = knob.span
        cf = knob.center_frequency
        self.assertEqual(start, _start)
        self.assertEqual(stop, _stop)
        self.assertEqual(span, _span)
        self.assertEqual(cf, _cf)


class TestBasebandKnobs(unittest.TestCase):
    def setUp(self):
        self.Knobs = BasebandKnobs

    def test_span_sets_start_stop(self):
        span = 100e3
        knobs = self.Knobs.create(span=span)
        truth_start = -knobs.rbw/2  # left bin edge for DC.
        truth_stop = truth_start + span
        truth_center = span/2
        self.assertEqual(truth_start, knobs.start_frequency)
        self.assertEqual(truth_stop, knobs.stop_frequency)
        self.assertEqual(truth_center, knobs.center_frequency)

    def test_swt_becomes_auto_upon_set(self):
        knobs = self.Knobs.create(is_auto_swt=True)
        knobs.swt = 0.1
        self.assertFalse(knobs._is_auto_swt)


if __name__ == "__main__":
    unittest.main()

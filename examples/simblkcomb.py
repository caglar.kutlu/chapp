import timeit

import matplotlib.pyplot as plt
import numpy as np
from chapp import ppopen
from chapp.blockcomb import BlockCombSimulator
from chapp.plotting import signoiseplot
from loguru import logger

ppr = ppopen(
    "../preprocess/hep1-2_pp1s6.nc",
    name_map={"gmsqr": "gmsqr2"},
    chunks={"grunstep": 100},
)


blks = BlockCombSimulator.make(ppr, "../preprocess/hep1-2_pp1s6.blkc.nc")

blks.prepare()

blks.set_seed(1234)
gds1 = blks.simulate(True)
blks.set_seed(1234)
gds2 = blks.simulate()

gds1.to_netcdf('gds_sim_blrem.nc')
gds2.to_netcdf('gds_sim.nc')

# fi = 130000
# window = 20000
# mydelta = gds.delta_n.isel(fbin=slice(fi, fi + window))
# signoiseplot(mydelta.fbin.data, mydelta.data, bins=50)


# delta1 = blkc.generate_noise_delta().compute()
# delta2 = blks.generate_noise_delta(True).compute()

# fig, (ax1, ax2) = plt.subplots(nrows=2, sharex=True, sharey=True)
# delta1.sel(grunstep=0).plot(ax=ax1)
# delta2.sel(grunstep=0).plot(ax=ax2)

# plt.subplots_adjust(wspace=0.1)

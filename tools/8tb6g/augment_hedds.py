"""A script that augments an HED with the following:
- Linear scale variants of:
    - power_w for PowSpect.  power_w = 10**((power-30)/10)
    - spar_mag_l for SPar1Spect.  spar_mag_l = 10**(spar_mag/10)
- Averaged aggregates of:
    - power_w -> power_wm
    - spar_mag_l -> spar_mag_lm

"""
from pathlib import Path
from capphed.ncutils import copyvar, nctraverse, copyattrs
import netCDF4 as nc
import numpy as np
from groundcontrol.util import dbm2watt, db2lin_pow
from datargs import parse, arg, argsclass


@argsclass(description=__doc__)
class ProgOpts:
    infile: Path = arg(True)
    only_scale: bool = arg(default=False, help="Do not aggregate rep, only scale.")


args = parse(ProgOpts)

ds = nc.Dataset(args.infile, "a")


VAREXISTERR = "NetCDF: String match to name in use"


def _agg(var):
    name = var.name
    ancvarnames = var.ancillary_variables.split()
    base = var.group()

    var_findex = var.dimensions.index("frequency")
    var_rindex = var.dimensions.index("rep")

    nobsvar = None
    for ancvarname in ancvarnames:
        if (ancvar := base[ancvarname]).standard_name == "number_of_observations":
            nobsvar = ancvar
            break

    if nobsvar is None:
        raise KeyError("No variable found for number of observations per repetition.")

    nobsdims = tuple((dimstr for dimstr in var.dimensions if dimstr != "frequency"))
    if nobsdims != nobsvar.dimensions:
        raise ValueError(
            f"The order of dimensions for '{name}' and '{nobsvar.name}' do not match."
        )

    # this is for automatic numpy broadcasting
    nobsarr = np.moveaxis(nobsvar[:][..., np.newaxis], -1, var_findex)
    nobs_stot = np.sum(nobsarr, axis=var_rindex)
    newdims = tuple((dimstr for dimstr in var.dimensions if dimstr != "rep"))

    newname = f"{name}m"
    try:
        newvar = base[newname]
        print(f"Overwriting existing {newname}.")
    except IndexError:
        chunksizes = var.chunking()
        endian = var.endian()
        filters = var.filters()
        datatype = var.datatype

        if chunksizes == "contiguous":
            chunksizes = None
            contiguous = True
        else:
            contiguous = False
            chunksizes.pop(var_rindex)

        try:
            fill_value = var.getncattr("_FillValue")
        except AttributeError:
            fill_value = None

        newvar: nc.Variable = base.createVariable(
            newname,
            datatype,
            newdims,
            **filters,
            chunksizes=chunksizes,
            contiguous=contiguous,
            endian=endian,
            fill_value=fill_value,
        )
        copyattrs(var, newvar)

    varr = var[...]
    summed = np.sum(varr * nobsarr, axis=var_rindex)
    newvar[:] = summed / nobs_stot
    return newvar


def _ps2lin(ds: nc.Group):
    power_dbm = ds["power"]
    try:
        power_w = ds["power_w"]
    except IndexError:
        power_w = copyvar(power_dbm, "power_w", with_values=False)
    power_w[...] = dbm2watt(power_dbm[...])
    power_w.units = "W"
    return power_w


def _sp1s2lin(ds: nc.Group):
    spar_db = ds["spar_mag"]
    try:
        spar_l = ds["spar_mag_l"]
    except IndexError:
        spar_l = copyvar(spar_db, "spar_mag_l", with_values=False)
    spar_l[...] = db2lin_pow(spar_db[...])
    spar_l.units = "W/W"
    return spar_l


for base, grp in nctraverse(ds):
    if grp.name.startswith("PowSpect"):
        print(f"Processing {base/grp.name}.")
        var = _ps2lin(grp)
    elif grp.name.startswith("SPar1Spect"):
        print(f"Processing {base/grp.name}.")
        var = _sp1s2lin(grp)
    else:
        continue

    if not args.only_scale:
        print(f"Aggregating {var.name}.")
        _agg(var)

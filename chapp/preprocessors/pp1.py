"""A preprocessor for CAPP8TB6G data.  A preprocessor is responsible for generating a
dataset containing all information necessary for constructing a grand spectrum for
further analysis.

Preprocessor Name: PP1
"""
from typing import (
    Dict,
    Literal,
    Union,
    Sequence,
    Optional,
    Iterator,
    List,
    Callable,
    TypeVar,
    Tuple,
    ClassVar,
)
from pathlib import Path
import os
import sys
from io import StringIO
from itertools import chain

import attr
from datargs import argsclass, arg, parse
import xarray as xr
import pandas as pd
from loguru import logger
import numpy as np
from scipy import constants as cnst
import netCDF4 as nc

from groundcontrol.analysis.nt import noise_psd
from groundcontrol.declarators import setting, to_json, from_json, declarative
from groundcontrol.util import db2lin_pow, dbm2watt
from groundcontrol import units as un
from chapp.hed import HEDDataset
from .protocols import PreprocessResult as _PRProtocol
from .protocols import PreprocParameters as _PRParamProtocol
from xrscipy.signal.utils import get_sampling_step


_source_default = "chapp.preprocessors.pp1"
_source = __name__ if __name__ != "__main__" else _source_default


_STAMP = dict(
    institution="IBS-CAPP",
    Conventions="CAPP-HED-0.1",
    source=_source,
    description=__doc__,
)


def drop_center_dnu(da: xr.DataArray, ndrop: int, fstep: float):
    nhalf = ndrop // 2
    # check whether this particular dropping method can be applied or not
    # np.all(da.sel(dnu=np.arange(-nhalf, nhalf + 1) * fstep).isnull()):
    dnu2drop = np.arange(-nhalf, nhalf + 1) * fstep
    dnu2keep = da.dnu.drop_sel(dnu=dnu2drop)
    keepmask = dnu2keep.reindex_like(da.dnu) == da.dnu
    return da.where(keepmask)


def pump_leak(dnu, pchi0, feps, fstep):
    x = (dnu + feps) / fstep
    return pchi0 * np.sinc(x) ** 2


def lorentzian(dnu, dfc, bw):
    return (1 + (2 * (dnu - dfc) / bw) ** 2) ** (-1)


def spar_wo_fitfun(dnu, dfc, bw, a0, a1):
    return (a0 + a1 * dnu) * lorentzian(dnu, dfc, bw)


class ProcessorError(Exception):
    pass


class AlignmentError(ProcessorError):
    pass


class UnitError(ProcessorError):
    pass


# _T = TypeVar("_T", xr.DataArray, xr.Dataset)
_T = Union[xr.DataArray, xr.Dataset]


@attr.define
class PreprocessResult(_PRProtocol):
    delta: xr.DataArray
    pqa_mu: xr.DataArray
    pbl_mu: xr.DataArray
    gj: xr.DataArray
    tsys: xr.DataArray
    cavds: xr.Dataset = attr.ib()  # fcav, q0, ql, beta, ff
    gmsqr: xr.DataArray
    peps0: xr.DataArray
    plfit: xr.Dataset
    navg: xr.DataArray
    bfield: xr.DataArray
    params: "PreprocParameters"
    refname: str = "PP1"

    dsnames: ClassVar = tuple(
        "delta pqa_mu pbl_mu gj tsys cavds gmsqr peps0 plfit navg bfield".split()
    )

    def map(
        self,
        fun: Callable[..., _T],
        exclude: Tuple[str] = (),
        include_only: Tuple[str] = (),
        *args,
        **kwargs,
    ) -> "PreprocessResult":
        if exclude != () and include_only != ():
            raise ValueError(
                "Provide either 'exclude' or 'include_only' arguments, not both."
            )

        def maybeapply(name):
            var = getattr(self, name)
            if name in exclude:
                return var
            if include_only == () or (name in include_only):
                return fun(var, *args, **kwargs)

        return PreprocessResult(
            delta=maybeapply("delta"),
            pqa_mu=maybeapply("pqa_mu"),
            pbl_mu=maybeapply("pbl_mu"),
            gj=maybeapply("gj"),
            tsys=maybeapply("tsys"),
            cavds=maybeapply("cavds"),
            gmsqr=maybeapply("gmsqr"),
            peps0=maybeapply("peps0"),
            plfit=maybeapply("plfit"),
            navg=maybeapply("navg"),
        )

    def _get_as_dataset(self, name: str, with_name: Optional[str] = None):
        obj: (xr.DataArray | xr.Dataset) = getattr(self, name)
        if isinstance(obj, xr.Dataset):
            return obj
        daname = name if with_name is None else with_name
        return obj.to_dataset(name=daname)

    def to_netcdf(self, path: Path, group: str = ".") -> List[str]:
        names = [name for name in self.to_netcdf_iter(path, group)]
        return names

    def to_netcdf_iter(self, path: Path, group: str) -> Iterator[str]:
        logger.info(f"Saving to netCDF file {path.as_posix()} under '{group}'.")
        logger.info(
            "This may take some time if data is chunked, as data will mainly be"
            " processed while being written to the file."
        )
        group = Path(group)
        mode = "a" if path.exists() else "w"
        name = "delta"
        yield name
        logger.info(f"Writing {name}.")
        self._get_as_dataset("delta").to_netcdf(
            path, group=(group / name).as_posix(), mode=mode
        )
        for name in self.dsnames:
            if name == "delta":
                # we already processed delta
                continue
            logger.info(f"Writing {name}.")
            yield name
            self._get_as_dataset(name).to_netcdf(
                path, group=(group / name).as_posix(), mode="a"
            )

    def close(self):
        for name in self.dsnames:
            try:
                getattr(self, name).close()
            except AttributeError:
                # Some may be `None`s
                pass

    @cavds.validator
    def _validate_cavds(self, attrib, value: xr.Dataset):
        wanted = set("fcav q_l beta formfactor".split())
        whatwehave = set(value.variables)
        if not wanted.issubset(whatwehave):
            raise ProcessorError(
                f"Cavity dataset is missing variables. ({wanted - whatwehave})"
            )

    @classmethod
    def from_xarray_datasets(
        cls, datasets: Dict[str, xr.Dataset], params: "PreprocParameters"
    ):
        dsiter = datasets.items()
        if "bfield" not in datasets:
            print("[WARNING] B-field not provided.")
            bfield = xr.DataArray(np.nan, name="bfield").to_dataset()

            dsiter = chain((("bfield", bfield),), dsiter)

        # TODO: Check cavds too.

        pr = cls(
            **{k: v[k] if k not in ["plfit", "cavds"] else v for k, v in dsiter},
            params=params,
        )
        return pr

    @classmethod
    def from_path(
        cls,
        path: Path,
        group=".",
        chunks=None,
        sel: Optional[Dict[str, Union[slice, int, Sequence]]] = None,
        name_map: Optional[Dict[str, str]] = None,
        _bfield: Optional[float] = None,
        _volume: Optional[float] = None,
    ):
        altnames = {} if name_map is None else name_map
        altnames_rev = {v: k for k, v in altnames.items()}

        ds_d = {}
        with nc.Dataset(path) as ncds:
            if group != ".":
                params_s = ncds[group].preproc_params
            else:
                params_s = ncds.preproc_params
            params = PreprocParameters.from_json(params_s)

        if group == ".":
            grp = ncds
        else:
            grp = ncds[group]
        for dsname in grp.groups.keys():
            if dsname in altnames_rev:
                targetname = altnames_rev[dsname]
            elif dsname in cls.dsnames:
                targetname = dsname
            else:
                logger.warning(f"Ignoring unknown dataset ({dsname}).")
                continue

            ds = xr.open_dataset(
                path, group=(Path(group) / dsname).as_posix(), chunks=chunks
            )
            if sel is not None:
                if dsname == 'bfield':
                    continue
                ds = ds.sel(**sel)
            if "run" in ds.coords:
                # xarray reads string as "object"
                # this is a quickfix for that.
                ds.coords["run"] = ds.run.astype(str)
            ds_d[targetname] = ds

        if _bfield is not None:
            print(
                f"[WARNING] Using bfield as {_bfield}, ignoring what's inside the"
                " 'bfield' group."
            )
            ds_d["bfield"] = xr.DataArray(_bfield, name="bfield").to_dataset()
        if _volume is not None:
            print(f"[WARNING] Using volume as {_volume}, ignoring what's inside cavds.")
            ds_d["cavds"]["volume"] = _volume
        return cls.from_xarray_datasets(ds_d, params=params)


@declarative
class PreprocParameters(_PRParamProtocol):
    pbl_sg_win: float = setting(
        un.hertz, description="PowSpect-BASELINE SG Filter Window Frequency Span."
    )
    pbl_sg_porder: int = setting(
        description="PowSpect-BASELINE SG Filter Polynomial Order."
    )
    pbl_sg_ndrop: int = setting(
        description=(
            "Number of bins around the center bin to drop before applying SG filter "
            "to PowSpect-BASELINE.  Usually 1 is enough."
        ),
        default=1,
    )
    pqa_sg_win: float = setting(
        un.hertz, description="PowSpect-QACTIVE SG Filter Window Frequency Span."
    )
    pqa_sg_porder: int = setting(
        description="PowSpect-QACTIVE SG Filter Polynomial Order."
    )
    delta_ndrop: int = setting(
        description=(
            "Number of bins around the center bin to drop from axion sensitive spectra."
        ),
    )
    plfit_ndrop: int = setting(
        description=(
            "Number of bins around the center bin to drop when SG filtering for "
            " artifact removal."
        )
    )
    plfit_params: Dict[
        Literal["pchi0", "feps"], Dict[Literal["value", "min", "max"], float]
    ] = setting(description="Parameters to use in the pump leak fit.")

    sboa_sg_win: float = setting(
        un.hertz, description="SPar1Spect-BO-ACTIVE SG Filter Window Frequency Span."
    )
    sboa_sg_porder: int = setting(
        description="SPar1Spect-BO-ACTIVE SG Filter Polynomial Order."
    )
    sboa_sg_ndrop: int = setting(
        description=(
            "Number of bins around the center bin to drop before applying SG filter "
            "to SPar1Spect-BO-ACTIVE.  Usually 1 is enough for dropping the degenerate "
            "bin."
        ),
        default=1,
    )
    sbobl_sg_win: float = setting(
        un.hertz, description="SPar1Spect-BO-BASELINE SG Filter Window Frequency Span."
    )
    sbobl_sg_porder: int = setting(
        description="SPar1Spect-BO-BASELINE SG Filter Polynomial Order."
    )
    gammasqr_avg_window: Optional[float] = setting(
        description=(
            "Frequency window to use when estimating gamma_c^2"
            "from power spectra.  If not given, a window will be chosen "
            "to approximately divide span into 10 equal sections."
        ),
        default=None,
    )
    wofit_params: Dict[
        Literal["dfc", "bw", "a0", "a1"], Dict[Literal["value", "min", "max"], float]
    ] = setting(
        description=(
            "Parameters to use when applying a fit to SPar1Spect-WO dataset.  The model"
            " function used for fit is: '(a0 + a1 * dnu) * lorentzian(dnu, dfc, bw)'"
        )
    )
    q0_dataset: Dict[Literal["path", "group"], str] = setting(
        description="Path to the Q0 measurement nc file."
    )
    formfactor_dataset: Dict[Literal["path", "group"], str] = setting(
        description="Path to form factor nc file."
    )
    pqa_prefilt: Optional[Dict] = setting(description="Prefilter parameters.")

    def fill_gammasqr_window_default(self, nbins: int, fstep: float):
        self.gammasqr_avg_window = (nbins // 10) * fstep
        return self.gammasqr_avg_window


DEFAULT_PARAMS = PreprocParameters(
    pbl_sg_win=185e3,
    pbl_sg_porder=3,
    pqa_sg_win=185e3,
    pqa_sg_porder=5,
    delta_ndrop=3,
    plfit_ndrop=7,
    plfit_params={
        "pchi0": dict(value=1, min=0.5, max=2),
        "feps": dict(value=7, min=-15, max=15),
    },
    sboa_sg_win=300e3,
    sboa_sg_porder=3,
    sboa_sg_ndrop=1,
    sbobl_sg_win=300e3,
    sbobl_sg_porder=3,
    wofit_params={
        "dfc": dict(value=0, min=-30e3, max=30e3),
        "bw": dict(value=400e3, min=200e3, max=600e3),
        "a0": dict(value=0.01, min=0.005, max=0.05),
        "a1": dict(value=-5e-8, min=-9e-8, max=9e-8),
    },
    q0_dataset={"path": os.environ.get("Q0DATA", ""), "group": "QMeas-LT"},
    formfactor_dataset={"path": os.environ.get("FFDATA", ""), "group": ""},
    pqa_prefilt=None,
    # pqa_prefilt=dict(coarsen=100, sg_win=150e3, sg_porder=4)
)


def apply_sg_filter(
    pqa: xr.DataArray,
    sg_win,
    sg_porder,
    pre_sg_win=None,
    pre_sg_porder=None,
    pre_coarsen=None,
    dnu2drop=None,
    primary_dim="runstep",
):
    span = pqa.dnu[[0, -1]].diff(dim="dnu").item()

    if dnu2drop is not None:
        dnu2keep = pqa.dnu.drop_sel(dnu=dnu2drop)
        keepmask = dnu2keep.reindex_like(pqa.dnu) == pqa.dnu
        pqa = pqa.where(keepmask).interpolate_na("dnu")
    else:
        pqa = pqa

    if (pre_sg_win is not None) or (pre_sg_porder is not None):
        prefpar = {
            "sg_win": pre_sg_win,
            "sg_porder": pre_sg_porder,
            "coarsen": pre_coarsen,
        }
        logger.info(f"Applying prefiltering ({prefpar}).")
        pqa_avg = (
            pqa.drop_vars(["run", "fbin", "fsc"], errors="ignore")
            .coarsen({primary_dim: prefpar["coarsen"]}, boundary="pad")
            .mean()
        )
        pqa_avg_filt = (
            pqa_avg.filt.savgol(
                dim="dnu",
                window_length=prefpar["sg_win"],
                polyorder=prefpar["sg_porder"],
                dask="parallelized",
            )
            .reindex_like(pqa, method="nearest")
            .assign_coords(pqa.coords)
        )
        pqa_prefilt = pqa / pqa_avg_filt
    else:
        pqa_prefilt = pqa
        pqa_avg_filt = 1

    newspan = span - sg_win
    pqa_mu = pqa_prefilt.filt.savgol(
        window_length=sg_win,
        polyorder=sg_porder,
        dim="dnu",
        dask="parallelized",
    )
    return (
        (pqa_mu * pqa_avg_filt)
        .assign_attrs(dict(pqa.attrs, long_name="$\\hat{\\mu}ˆ\\mathrm{Q}$"))
        .sel(dnu=slice(-newspan / 2, newspan / 2))
    )


@attr.define
class PP1ProcAgent:
    hedds: HEDDataset
    masterdf: pd.DataFrame
    pqa: xr.DataArray
    pbl: xr.DataArray
    sboa: xr.DataArray
    sbobl: xr.DataArray
    swobl: xr.DataArray
    scobl: xr.DataArray
    psbc: Optional[xr.DataArray]
    params: PreprocParameters
    navg: int
    nrep: int
    _chunks: int

    def process_scale(self):
        das = []
        for aname in "pqa pbl sboa sbobl swobl scobl".split():
            da = getattr(self, aname)
            units = da.attrs["units"]
            if units == "dBm":
                newunits = "W"
                newda = xr.apply_ufunc(dbm2watt, da, dask="allowed")
                newda.attrs = dict(da.attrs, units=newunits)
            elif units == "dB":
                newunits = "W/W"
                newda = xr.apply_ufunc(db2lin_pow, da, dask="allowed")
                newda.attrs = dict(da.attrs, units=newunits)
            elif units in ["W", "W/W"]:
                newda = da
            else:
                raise UnitError(f"Unrecognized units. ({units})")

            das.append(newda)
        return das

    def _get_dnu2drop(self, n: int, fstep: float):
        nhalf = n // 2
        return np.arange(-nhalf, nhalf + 1) * fstep

    def process_pbl_mu(self, pbl: xr.DataArray):
        span = pbl.dnu[[0, -1]].diff(dim="dnu").item()
        dnu2drop = self._get_dnu2drop(self.params.pbl_sg_ndrop, pbl.attrs["_fstep"])
        newspan = span - self.params.pbl_sg_win
        pbl_mu = (
            pbl.drop_sel(dnu=dnu2drop)
            .reindex_like(pbl)
            .interpolate_na(dim="dnu")
            .filt.savgol(
                dim="dnu",
                window_length=self.params.pbl_sg_win,
                polyorder=self.params.pbl_sg_porder,
                dask="parallelized",
            )
            .assign_attrs(dict(pbl.attrs, long_name="$\\hat{\\mu}^\\mathrm{BL}$"))
            .sel(dnu=slice(-newspan / 2, newspan / 2))
        )
        return pbl_mu

    def process_lo_leakage(self, pbl: xr.DataArray, pbl_mu: xr.DataArray):
        p_eps_plus_bg = pbl.sel(dnu=[0])
        p_bg = pbl_mu.sel(dnu=[0])
        p_eps0 = p_eps_plus_bg - p_bg

        p_eps = p_eps0.reindex_like(pbl, fill_value=0).assign_attrs(
            dict(
                long_name="$P_\\epsilon$",
                comment="Power excess at the center bin due to LO leakage.",
                units=pbl.attrs["units"],
            )
        )
        return p_eps0.squeeze(drop=True), p_eps

    def process_pump_leakage(self, pqa: xr.DataArray):
        """Returns:
        ds_plfit:  A dataset containing pchi0 and feps estimations per runstep.
        p_chi: Power spectrum due to pump leak, i.e. the pump leak spectrum function
        is evaluated with the parameters in ds_plfit.
        """
        fstep = pqa.attrs["_fstep"]

        def pump_leak(dnu, pchi0, feps):
            x = (dnu + feps) / fstep
            return pchi0 * np.sinc(x) ** 2

        dnu2drop = self._get_dnu2drop(self.params.plfit_ndrop, fstep)
        pqa_f = (
            pqa.drop_sel(dnu=dnu2drop)
            .reindex_like(pqa)  # replace dropped dnu with nan
            .interpolate_na(dim="dnu")
            .pipe(self.process_pqa_mu, dropninterp=False)
        )
        pqa_nobg = pqa - pqa_f
        # We need to apply prescaling to make curve_fit happy
        # this is simply a workaround for avoiding the numeric tolerance into the
        # curve_fit algorithm.

        da = pqa_nobg.sel(dnu=dnu2drop)

        # Simplest choice of scaling is the initial guess of pchi0 parameter.
        fitparams = self.params.plfit_params
        _feps0 = fitparams["feps"]["value"]
        _pchi0 = da.sel(dnu=0).mean().compute().item() / pump_leak(0, 1, _feps0)
        scaler = _pchi0

        # Scaled da
        da_s = da / scaler

        # scaled parameter guess
        p0 = {}
        bounds = {}

        for k, v in fitparams.items():
            p0[k] = v["value"]
            bounds[k] = [v["min"], v["max"]]

        da_f = da_s.curvefit("dnu", pump_leak, p0=p0, bounds=bounds)
        da_popt = da_f.curvefit_coefficients
        da_pcov = da_f.curvefit_covariance
        da_pcov.loc[{"cov_i": "pchi0"}] *= scaler
        da_pcov.loc[{"cov_j": "pchi0"}] *= scaler
        da_popt.loc[{"param": "pchi0"}] *= scaler
        da_d = {}

        for name in da_f.param.data:
            parval = da_popt.sel(param=name).drop_vars("param")
            parerr = np.sqrt(
                da_pcov.sel(cov_i=name, cov_j=name).drop_vars(["cov_i", "cov_j"])
            )
            parval.name = name
            parerr.name = f"{name}_err"

            da_d[parval.name] = parval
            da_d[parerr.name] = parerr

        ds_plfit = xr.Dataset(da_d)
        ds_plfit.pchi0.attrs = dict(
            pqa.attrs.copy(),
            long_name="$Pˆ{\\chi 0}$",
            comment="Excess power due to JPA pump down-mixing.",
            ancillary_variable=ds_plfit.pchi0_err.name,
        )
        ds_plfit.pchi0_err.attrs = dict(
            units=pqa.attrs["units"],
            standard_name="standard_error",
            standard_name_mod_args="statistical",
        )
        ds_plfit.feps.attrs = dict(
            units=pqa.dnu.attrs["units"],
            long_name="$f_{\\epsilon}$",
            comment="Offset of LO from $f_{sc}$, i.e. $f_\\epsilon = f_{LO} - f_{sc}$",
        )
        ds_plfit.feps_err.attrs = dict(
            units=ds_plfit.feps.attrs["units"],
            standard_name="standard_error",
            standard_name_mod_args="statistical",
        )

        # CAN BE PARALLELIZED
        p_chi = pump_leak(
            *map(
                lambda d: d.broadcast_like(pqa),
                [pqa.dnu, ds_plfit.pchi0, ds_plfit.feps],
            )
        ).assign_attrs(ds_plfit.pchi0.attrs.copy())

        return ds_plfit, p_chi

    def process_pqa_mu(self, pqa_clean: xr.DataArray, dropninterp=True):
        span = pqa_clean.dnu[[0, -1]].diff(dim="dnu").item()

        if dropninterp:
            dnu2drop = self._get_dnu2drop(
                self.params.delta_ndrop, pqa_clean.attrs["_fstep"]
            )
            pqa = (
                pqa_clean.drop_sel(dnu=dnu2drop)
                .reindex_like(pqa_clean)
                .interpolate_na("dnu")
            )
        else:
            pqa = pqa_clean

        pars = self.params

        if self.params.pqa_prefilt is not None:
            prefpar = pars.pqa_prefilt
            logger.info(f"Applying prefiltering ({prefpar}).")
            pqa_avg = pqa.coarsen(runstep=prefpar["coarsen"], boundary="pad").mean()
            pqa_avg_filt = (
                pqa_avg.filt.savgol(
                    dim="dnu",
                    window_length=prefpar["sg_win"],
                    polyorder=prefpar["sg_porder"],
                    dask="parallelized",
                )
                .reindex_like(pqa, method="nearest")
                .assign_coords(pqa.coords)
            )
            pqa_prefilt = pqa / pqa_avg_filt
        else:
            pqa_prefilt = pqa
            pqa_avg_filt = 1

        newspan = span - self.params.pqa_sg_win
        pqa_mu = pqa_prefilt.filt.savgol(
            window_length=self.params.pqa_sg_win,
            polyorder=self.params.pqa_sg_porder,
            dim="dnu",
            dask="parallelized",
        )
        return (
            (pqa_mu * pqa_avg_filt)
            .assign_attrs(dict(pqa_clean.attrs, long_name="$\\hat{\\mu}ˆ\\mathrm{Q}$"))
            .sel(dnu=slice(-newspan / 2, newspan / 2))
        )

    def process_gj(self, sboa, sbobl):
        dnu2drop = self._get_dnu2drop(self.params.sboa_sg_ndrop, sboa.attrs["_fstep"])
        sboa_f = (
            sboa.drop_sel(dnu=dnu2drop)
            .reindex_like(sboa)
            .interpolate_na(dim="dnu")
            .filt.savgol(
                window_length=self.params.sboa_sg_win,
                polyorder=self.params.sboa_sg_porder,
                dask="parallelized",
                dim="dnu",
            )
            .assign_attrs(dict(sboa.attrs, comment="SGFILT(BO-ACTIVE)"))
        )
        sbobl_f = (
            sbobl.reindex_like(  # no dropping since no jpa
                sboa
            )  # maybe raise error if not already same index?
            .interpolate_na(dim="dnu")
            .filt.savgol(
                window_length=self.params.sbobl_sg_win,
                polyorder=self.params.sbobl_sg_porder,
                dask="parallelized",
                dim="dnu",
            )
            .assign_attrs(dict(sboa.attrs, comment="SGFILT(BO-BASELINE)"))
        )
        span = sboa.dnu[[0, -1]].diff(dim="dnu").item()
        newspan = span - max(self.params.sboa_sg_win, self.params.sbobl_sg_win)

        gjda = (sboa_f / sbobl_f).sel(dnu=slice(-(newspan / 2), newspan / 2))
        gjda.attrs = dict(
            long_name="$G_J$",
            units="W/W",
            comment="SGFILT(BO-ACTIVE)/SGFILT(BO-BASELINE)",
        )

        return gjda

    def process_delta(self, pqa_clean: xr.DataArray, pqa_mu):
        navg = self.navg
        # pqa_mu may have less usable bins, most likely due to filtering.
        da = pqa_clean.reindex_like(pqa_mu)
        dlt = ((da / pqa_mu - 1) * np.sqrt(navg)).assign_attrs(
            dict(
                long_name="$\\delta$",
                comment="Normalized power excess.",
                _fstep=pqa_clean.attrs["_fstep"],
                _navg=navg,
            )
        )
        return dlt

    def process_gammasqr_from_delta(self, delta: xr.DataArray, window: float):
        fstep = delta.attrs["_fstep"]
        nwindow = int(window // fstep)

        dnu2drop = self._get_dnu2drop(self.params.delta_ndrop, fstep)
        da = delta.drop_sel(dnu=dnu2drop)
        right = da.sel(dnu=slice(0, None))
        left = da.sel(dnu=slice(0, None, -1)).assign_coords(dnu=right.dnu)
        sbc_ds = xr.Dataset({"right": right, "left": left})

        ntotrim = sbc_ds.dnu.size % nwindow
        logger.warning(
            f"{ntotrim} bins from each side of the "
            "spectrum will be trimmed during covariance estimation."
        )
        coarsened = sbc_ds.coarsen(dnu=nwindow, boundary="trim").construct(
            dnu=["dnu_cell", "segbin"]
        )

        cov_l = []
        for f, ds in coarsened.groupby("dnu_cell"):
            right = ds.right
            left = ds.left
            cov_l.append(xr.cov(right, left, dim="segbin", ddof=1))

        gammasqravg = (
            xr.concat(cov_l, dim="dnu_cell")
            .rename(dnu_cell="dnu")
            .assign_coords(dnu=coarsened.dnu.mean(dim="segbin").data, fsc=delta.fsc)
        )
        return gammasqravg

    def process_gammasqr_from_psbc(self, psbc: xr.DataArray, pqa_mu: xr.DataArray):
        muplus = pqa_mu.sel(dnu=slice(0, None))
        muminus = pqa_mu.sel(dnu=slice(0, None, -1)).assign_coords(dnu=muplus.dnu)
        da = psbc.reindex_like(muplus)

        gammasqr = da / (muplus * muminus)
        return gammasqr

    def process_wofit(self, swobl: xr.DataArray):
        fitparams = self.params.wofit_params
        p0 = {}
        bounds = {}
        for k, v in fitparams.items():
            p0[k] = v["value"]
            bounds[k] = [v["min"], v["max"]]

        da_f = swobl.curvefit("dnu", spar_wo_fitfun, p0=p0, bounds=bounds)
        da_f.runstep.attrs = swobl.runstep.attrs.copy()
        da_f.fsc.attrs = swobl.fsc.attrs.copy()

        params_of_interest = ["dfc", "bw"]
        longname_map = {"dfc": "$\\Delta f_c$", "bw": "$B_c$"}
        comment_map = {"dfc": "dfc = fc - fsc", "bw": "Cavity 3 dB bandwidth."}

        da_d = {}

        for param in params_of_interest:
            dspar = da_f.sel(param=param, cov_i=param, cov_j=param).drop(
                ["param", "cov_i", "cov_j"]
            )
            coeffda = dspar.curvefit_coefficients
            coeffda.name = param
            coeffda.attrs["units"] = swobl.dnu.attrs["units"]
            coeffda.attrs["long_name"] = longname_map[param]
            coeffda.attrs["comment"] = comment_map[param]
            stdevda = xr.apply_ufunc(np.sqrt, dspar.curvefit_covariance, dask="allowed")
            stdevda.name = f"{param}_err"
            stdevda.attrs["standard_name"] = "standard_error"
            stdevda.attrs["standard_name_mod_args"] = "statistical"

            coeffda.attrs["ancillary_variable"] = stdevda.name

            da_d[coeffda.name] = coeffda
            da_d[stdevda.name] = stdevda

        ds = xr.Dataset(da_d)
        ds["fcav"] = ds.dfc + ds.fsc
        ds.fcav.attrs = ds.dfc.attrs
        ds.fcav.attrs["long_name"] = "$f_c$"
        ds.fcav.attrs["comment"] = "Cavity resonance frequency."
        ds["q_l"] = ds.fcav / ds.bw
        ds.q_l.attrs = {
            "long_name": "$Q_L$",
            "comment": "Loaded quality factory of the cavity.",
        }
        ds["q_l_err"] = ds.q_l * xr.apply_ufunc(
            np.sqrt,
            (ds.dfc_err / ds.fcav) ** 2 + (ds.bw_err / ds.bw) ** 2,
            dask="allowed",
        )
        ds.q_l_err.attrs = {
            k: v
            for k, v in ds.bw_err.attrs.items()
            if k in ["standard_name", "standard_name_mod_args"]
        }
        return ds

    def process_beta(self, wofit_ds: xr.Dataset):
        dsql = wofit_ds[["fcav", "q_l", "q_l_err"]].squeeze().swap_dims(runstep="fcav")

        path = self.params.q0_dataset["path"]
        group = self.params.q0_dataset["group"]
        dsq0 = (
            xr.open_dataset(path, group=group)[["q0", "q0_err"]]
            .rename(frequency="fcav")
            .interp_like(dsql)
        )

        b = dsq0.q0 / dsql.q_l - 1
        b_err = (
            np.sqrt((dsql.q_l_err / dsql.q_l) ** 2 + (dsq0.q0_err / dsq0.q0) ** 2) * b
        )
        b_ds = (
            xr.Dataset({"beta": b, "beta_err": b_err})
            .swap_dims(fcav="runstep")
            .reset_coords("fcav")
        ).assign_attrs(
            comment=(
                "Estimated using Q0 data within "
                f"{path}::{group}(source: {dsq0.attrs['source']})."
            )
        )
        return b_ds

    def process_formfactor(self, fsc: xr.DataArray):
        path = self.params.formfactor_dataset["path"]
        group = self.params.formfactor_dataset["group"]
        daff = xr.open_dataset(path, group=group)
        try:
            return daff.interp(frequency=fsc).drop("frequency")
        except ValueError as e:
            e.args[0].startswith("Dimensions {'frequency'} do not exist.")
            return daff.interp(f=fsc).drop("f")

    def process_temperatures(self):
        # hardcoding some stuff here, to be fixed later.
        auxds = self.hedds.get_dataset("AuxTable", dnu_mode=None, chunks=self._chunks)

        tmean = ((auxds.t_mc2 + auxds.t_cav2) / 2).mean().compute().item()
        tstd = np.sqrt(auxds.t_mc2.var() + auxds.t_cav2.var()).compute().item()

        if tstd == 0:
            logger.warning("Temperature variance is 0.")

        if tmean > 50e-3:
            logger.warning(
                f"Sample mean for temperature records is high. ({tmean*1e3:.1f} mK)"
            )
        if tstd > 5e-3:
            logger.warning(
                f"Sample std for temperature records is high. ({tstd*1e3:.1f} mK)"
            )

        return tmean, tstd

    def process_ntmeas(self, fbin):
        ntmeas = self.hedds.get_ntmeas_baseline_dataset()
        tn_weights = 1 / ntmeas.tn_err**2
        gain_weights = 1 / ntmeas.gain_err**2
        tn = ntmeas.tn.weighted(tn_weights).mean(dim="rep", keep_attrs=True)
        gain = ntmeas.gain.weighted(gain_weights).mean(dim="rep", keep_attrs=True)

        tn_err = 1 / tn_weights.sum(dim="rep")
        gain_err = 1 / gain_weights.sum(dim="rep")
        return (
            xr.merge([tn, tn_err, gain, gain_err])
            .interp(frequency=fbin, kwargs=dict(bounds_error=True))
            .drop("frequency")
            .assign_attrs(ntmeas.attrs)
        )  # we use dnu here

    def process_tsys_spectra_comparison(self, pqa_mu, pbl_mu, gj, tmean):
        ntmeas = self.process_ntmeas(pqa_mu.fbin)
        logger.info(
            f"Using {ntmeas.attrs['title']} as reference noise temperature measurement."
        )

        zet = pqa_mu / pbl_mu
        try:
            gjre = gj.interp_like(zet, kwargs=dict(bounds_error=True))
        except ValueError as e:
            raise ProcessorError(
                "SPar1Spect-BO measurements have smaller span than PowSpect"
                " measurements.  Something must be wrong."
            ) from e
        rcomp = zet / gjre
        tnf = noise_psd(tmean, pqa_mu.fbin) / cnst.Boltzmann

        tsys = rcomp * (tnf + ntmeas.tn)
        return tsys.assign_attrs(
            long_name="$ T_{sys}$",
            units=ntmeas.tn.attrs["units"],
            comment="Estimated via spectra comparison.",
        )

    def process(self) -> PreprocessResult:
        # Process scale and aggregate rep dimension
        logger.info("Processing scale factor and aggregating reps.")
        pqa, pbl, sboa, sbobl, swobl, scobl = map(
            lambda ds: ds.mean(dim="rep", keep_attrs=True), self.process_scale()
        )
        if self.psbc is not None:
            psbc = self.psbc.mean(dim="rep", keep_attrs=True)

        logger.info("Estimating binwise μ in the reference spectrum.")
        pbl_mu = self.process_pbl_mu(pbl)

        logger.info("Estimating LO leakage from the reference spectrum.")
        p_eps0, p_eps = self.process_lo_leakage(pbl, pbl_mu)

        pqa_noeps = (pqa - p_eps).assign_attrs(pqa.attrs.copy())

        logger.info("Estimating JPA pump leakage.")
        ds_plfit, p_chi = self.process_pump_leakage(pqa_noeps)

        pqa_clean = (pqa_noeps - p_chi).assign_attrs(pqa.attrs.copy())

        logger.info("Estimating binwise μ.")
        pqa_mu = self.process_pqa_mu(pqa_clean)

        logger.info("Estimating gain change factor with respect to reference state.")
        gj = self.process_gj(sboa, sbobl)

        logger.info("Estimating δ (normalized power excess).")
        delta = self.process_delta(pqa_clean, pqa_mu)

        if self.psbc is None:
            logger.info("Estimating the sideband correlation from δ.")
            gammasqr = self.process_gammasqr_from_delta(
                delta, self.params.gammasqr_avg_window
            )
        else:
            logger.info(
                "Estimating the sideband correlation from sideband covariance spectrum."
            )
            gammasqr = self.process_gammasqr_from_psbc(psbc, pqa_mu)

        logger.info(
            "Estimating cavity resonance characteristics by fitting a lorentzian model"
            " function."
        )
        wofit_ds = self.process_wofit(swobl)

        _q0path = self.params.q0_dataset["path"]
        _q0group = self.params.q0_dataset["group"]
        logger.info(
            f"Estimating β using apriori Q0 measurements({_q0path}::{_q0group})."
        )
        beta_ds = self.process_beta(wofit_ds)

        _ffpath = self.params.formfactor_dataset["path"]
        _ffgroup = self.params.formfactor_dataset["group"]
        logger.info(
            f"Interpolating form factor ({_ffpath}::{_ffgroup}) at cavity resonance"
            " frequencies."
        )
        ff_ds = self.process_formfactor(wofit_ds.fsc)

        tmean, tstd = self.process_temperatures()
        logger.info(
            "Sample mean and standard deviation of all cavity and MC temperatures are"
            f" {tmean*1e3:.1f} ({tstd*1e3:.2f}) mK."
        )

        logger.info("Estimating system noise temperature using spectra comparison.")
        tsys = self.process_tsys_spectra_comparison(pqa_mu, pbl_mu, gj, tmean)

        logger.info("Merging cavity related estimation data.")
        cavds = xr.merge(
            [
                wofit_ds,
                beta_ds,
                ff_ds.c.assign_attrs(dict(ff_ds.c.attrs, **ff_ds.attrs)).rename(
                    "formfactor"
                ),
            ]
        )
        pr = PreprocessResult(
            delta,
            pqa_mu,
            pbl_mu,
            gj,
            tsys,
            cavds,
            gammasqr,
            p_eps0,
            ds_plfit,
            bfield=xr.DataArray(np.nan, name="bfield"),
            params=self.params,
            navg=xr.DataArray(self.navg, name="navg"),
        )
        return pr

    @staticmethod
    def sanitize_sp1s(sp1s: xr.DataArray, tolerance: float = 1e-3):
        """Due to a minor bug in the script, some SPar1Spect spectra have their center
        frequency bin located at a small offset from f_sc.  The misalignment is on the
        order of < 100 Hz.  Since frequency bandwidth is usually much larger than this
        misalignment, we can simply interpolate the result to a common dnu domain
        without introducing significant error.

        Arguments:
            sp1s: The SPar1Spect measurement dataset.
            tolerance: max(abs((f_center - f_sc)))/span must be lower than this number.
                Otherwise the function will raise a AlignmentError.
        """
        if sp1s.attrs["_sanity"] == "inconsistent_dnu":
            maks = (
                xr.apply_ufunc(
                    np.abs,
                    (sp1s.fbin.median(dim="frequency") - sp1s.fsc),
                    dask="allowed",
                )
                .max()
                .compute()
                .item()
            )
            span = (
                (sp1s.dnu.isel(runstep=0, frequency=[0, -1]).diff(dim="frequency"))
                .compute()
                .item()
            )
            logger.info(f"Max misalignment/span: ({maks}/{span}) ")
            if (maks / span) > tolerance:
                print(maks, span, maks / span, tolerance)
                raise AlignmentError(
                    "Some spectra have their center frequencies far away from f_sc."
                    f" max{{f_center - f_sc}} = {maks:.1f} Hz and span={span:.1f} Hz.",
                    maks,
                )
            else:
                nbins = sp1s.frequency.size
                domain = np.linspace(-span / 2, span / 2, nbins)
                # parallelize?
                return sp1s.groupby("runstep").map(
                    lambda da: da.swap_dims(frequency="dnu").interp(
                        dnu=domain, kwargs=dict(fill_value="extrapolate")
                    )
                )
        else:
            return sp1s

    @staticmethod
    def get_sanitized_dataset(
        hedds, name, chunks, tolerance: float = 1e-3, sel_runstep=slice(0, None)
    ):
        ds = hedds.get_dataset(
            name, chunks=chunks, fail_if_inconsistent=False, sel_runstep=sel_runstep
        )
        try:
            if ds.attrs["_sanity"] == "inconsistent_dnu":
                logger.warning(f"Dealing with inconsistent dnu on {name}.")
        except KeyError:
            pass
        sanitds = PP1ProcAgent.sanitize_sp1s(ds, tolerance=tolerance)
        return sanitds

    @staticmethod
    def _check_sg_feasible(ds, window, porder):
        # This check is the result of empirical investigation.
        # SG filter seem to give meaningless results for the cases where this test
        # fails.
        span = ds.dnu[[0, -1]].diff(dim="dnu").item()
        if porder / (span / window) >= 0.95:
            dsname = ds.attrs.get("name", "")
            window_upper = 0.95 * span / porder
            raise ProcessorError(
                f"SG parameters for {dsname} are unstable. Try reducing the window"
                f" to below {window_upper:.1f} Hz."
            )

    @classmethod
    def from_hedds(
        cls,
        hedds: HEDDataset,
        params: PreprocParameters,
        chunks: int,
        skip_sanitization: bool = False,
        sel_runstep: Optional = slice(0, None),
        ignore_psbcov: bool = False,
    ):
        pqa_ds = hedds.get_dataset(
            "PowSpect-QACTIVE", chunks=chunks, sel_runstep=sel_runstep
        )
        cls._check_sg_feasible(pqa_ds, params.pqa_sg_win, params.pqa_sg_porder)
        pbl_ds = hedds.get_dataset(
            "PowSpect-BASELINE", chunks=chunks, sel_runstep=sel_runstep
        )
        cls._check_sg_feasible(pbl_ds, params.pbl_sg_win, params.pbl_sg_porder)
        sboa_ds = hedds.get_dataset(
            "SPar1Spect-BO-ACTIVE", chunks=chunks, sel_runstep=sel_runstep
        )
        cls._check_sg_feasible(sboa_ds, params.sboa_sg_win, params.sboa_sg_porder)

        if not skip_sanitization:
            # This dataset may have misalignment of center spectrum on the order of ~50
            # kHz with the f_sc.  This was due to a bug in the DAQ.
            # CAUTION
            # This hardcoded tolerance assumes that (1-tolerance)*VNASPAN > SASPAN which
            # always holds for 8TB6G.
            sbobl_ds = cls.get_sanitized_dataset(
                hedds,
                "SPar1Spect-BO-BASELINE",
                chunks=chunks,
                tolerance=1e-1,
                sel_runstep=sel_runstep,
            )
            cls._check_sg_feasible(
                sbobl_ds, params.sbobl_sg_win, params.sbobl_sg_porder
            )
            # These datasets may have some misalignment of center bin with f_sc
            swobl_ds = cls.get_sanitized_dataset(
                hedds,
                "SPar1Spect-WO-BASELINE",
                chunks=chunks,
                sel_runstep=sel_runstep,
                tolerance=1e-3,
            )
            scobl_ds = cls.get_sanitized_dataset(
                hedds,
                "SPar1Spect-CO-BASELINE",
                chunks=chunks,
                sel_runstep=sel_runstep,
                tolerance=1e-3,
            )
        else:
            sbobl_ds = hedds.get_dataset(
                "SPar1Spect-BO-BASELINE",
                chunks=chunks,
                fail_if_inconsistent=False,
                sel_runstep=sel_runstep,
            )
            cls._check_sg_feasible(
                sbobl_ds, params.sbobl_sg_win, params.sbobl_sg_porder
            )
            swobl_ds = hedds.get_dataset(
                "SPar1Spect-WO-BASELINE",
                chunks=chunks,
                fail_if_inconsistent=False,
                sel_runstep=sel_runstep,
            )
            scobl_ds = hedds.get_dataset(
                "SPar1Spect-CO-BASELINE",
                chunks=chunks,
                fail_if_inconsistent=False,
                sel_runstep=sel_runstep,
            )

        pqa = pqa_ds.power
        pbl = pbl_ds.power
        sboa = sboa_ds.spar_mag
        sbobl = sbobl_ds.spar_mag
        scobl = scobl_ds.spar_mag
        swobl = swobl_ds.spar_mag

        pqa_fstep = get_sampling_step(pqa, "dnu")
        pbl_fstep = get_sampling_step(pbl, "dnu")
        sboa_fstep = get_sampling_step(sboa, "dnu")
        sbobl_fstep = get_sampling_step(sbobl, "dnu")
        scobl_fstep = get_sampling_step(scobl, "dnu")
        swobl_fstep = get_sampling_step(swobl, "dnu")

        if pqa_fstep != pbl_fstep:
            raise ProcessorError(
                "Power spectrum measurements "
                "have different frequency steps. "
                f"(BASELINE: {pqa_fstep}, QACTIVE: {pbl_fstep})"
            )
        if sboa_fstep != sbobl_fstep:
            raise ProcessorError(
                "SPar1Spect-BASELINE and ACTIVE "
                "have different frequency steps. "
                f"(BASELINE: {sboa_fstep}, ACTIVE: {sbobl_fstep})"
            )

        pqa.attrs["_fstep"] = pqa_fstep
        pbl.attrs["_fstep"] = pbl_fstep
        sboa.attrs["_fstep"] = sboa_fstep
        sbobl.attrs["_fstep"] = sbobl_fstep
        scobl.attrs["_fstep"] = scobl_fstep
        swobl.attrs["_fstep"] = swobl_fstep

        psbc = None
        try:
            psbc_ds = hedds.get_dataset(
                "PowSBCov-QACTIVE", dnu_mode=None, chunks=chunks
            )
            use_psbcov = (psbc_ds.dims["runstep"] != 0) and (not ignore_psbcov)
            if use_psbcov:
                psbc = psbc_ds.sbcov.assign_coords(
                    sbin=psbc_ds.sbin * pqa_fstep
                ).rename(sbin="dnu")
        except KeyError:
            logger.info("PowSBCov-QACTIVE was not found.")

        if psbc is None:
            logger.info(
                "Not using PowSBCov-QACTIVE, gamma_c will be estimated from power"
                " spectra."
            )

        logger.warning("Assuming all runsteps have same number of spectra averaged.")
        navg = float(pqa_ds.sel(runstep=0).nobs.sum("rep").data)
        nrep = pqa_ds.dims["rep"]

        if (params.gammasqr_avg_window is None) and (psbc is None):
            gasqr = params.fill_gammasqr_window_default(pqa_ds.dims["dnu"], pqa_fstep)
            logger.warning(
                f"No window provided for gamma_c^2 estimation, using {gasqr} Hz."
            )

        masterdf = hedds.mastertable
        return cls(
            hedds,
            masterdf,
            pqa,
            pbl,
            sboa,
            sbobl,
            swobl,
            scobl,
            psbc=psbc,
            params=params,
            navg=navg,
            nrep=nrep,
            chunks=chunks,
        )


@argsclass
class Process:
    infile: Path = arg(positional=True, help="Input filename.  Must be an HED dataset.")
    outfile: Path = arg(positional=True, help="Output filename.")
    params: Optional[Path] = arg(aliases=["-p"], help="Path to parameters JSON file.")
    run_names: Optional[Sequence[str]] = arg(
        aliases=["-r"], help="Space separated list of run_names to process."
    )
    chunks: int = arg(
        default=1,
        aliases=["-c"],
        help=(
            "Number of `runstep` chunks to use for reading and parallel processing of"
            " data.  Defaults to 1 if not given.  Try a number close to the number of"
            " runsteps in a run divided by the number of CPUs present in the system for"
            " good results."
        ),
    )
    ignore_sideband_covariance: bool = arg(
        default=False,
        help=(
            "Do not use PowSBCov data for gamma_c^2 estimation even if it is available."
        ),
    )
    append: bool = arg(default=False, help="Append without asking.")


@argsclass
class MakeParams:
    @staticmethod
    def make_params():
        to_json(DEFAULT_PARAMS, Path("./pp1_params.json"))


@argsclass(description=__doc__)
class Opts:
    action: Union[MakeParams, Process]


def process(procopts: Process):

    if procopts.params is None:
        params = DEFAULT_PARAMS
        logger.warning(
            "Parameter file is not provided.  Using default parameters as shown"
            f" below:\n{to_json(params)}"
        )
        response = input("Do you want to use these parameters? [n]/Y")
        if response != "Y":
            abort()
    else:
        params = PreprocParameters.from_json(procopts.params)

    if procopts.outfile.exists() and (not procopts.append):
        logger.warning(
            f"{procopts.outfile} exists.  Do you want to append to it (not recommended"
            " unless processing different runs)? [n]/Y"
        )
        response = input()
        if response != "Y":
            abort()

    hds = HEDDataset.from_path(procopts.infile)

    if procopts.run_names is None:
        run_names = hds.runtable.index
    else:
        run_names = procopts.run_names

    logs = StringIO()
    fmt = "[ t0+{elapsed} | {level} ] {message}"
    logger.add(logs, format=fmt)

    for runname in run_names:
        logger.info(f"BEGINNING TO PROCESS '{runname}'.")
        logger.info("Preparing access.")
        hds.select(runname=runname)
        agent = PP1ProcAgent.from_hedds(
            hds,
            params,
            chunks=procopts.chunks,
            ignore_psbcov=procopts.ignore_sideband_covariance,
        )
        pr = agent.process()
        _ = pr.to_netcdf(procopts.outfile, runname)

        nds = nc.Dataset(procopts.outfile, "a")
        parstr = to_json(params, pretty=False)
        nds[runname].setncattr("preproc_params", parstr)
        logger.info(f"FINISHED PROCESSING '{runname}'.")

    logs.seek(0)
    # Opening to write the preprocess stamp and the logs
    nds = nc.Dataset(procopts.outfile, "a")
    logs_str = logs.read()
    logger.info("Stamping the file.")
    for k, v in _STAMP.items():
        nds.setncattr(k, v)

    logger.info(
        f"Preprocess routine {_source} finished successfully, writing process logs."
    )
    nds.setncattr("process_log", logs_str)
    nds.close()


def abort():
    print("Aborting.")
    sys.exit(42)


def main(args):
    if isinstance(args.action, MakeParams):
        args.action.make_params()
    elif isinstance(args.action, Process):
        try:
            process(args.action)
        except KeyboardInterrupt:
            logger.error("Keyboard interrupt received.")
            abort()
        print("Done.")


if __name__ == "__main__":
    args = parse(Opts)
    main(args)

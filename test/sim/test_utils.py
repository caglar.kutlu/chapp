import unittest

import numpy as np

from chapp.sim.utils import repeating_array


class TestRepeatingArray(unittest.TestCase):
    def setUp(self):
        self.base_arr = np.array([1,2,3])

    def test_array_size_for_odd(self):
        n = 5
        a = repeating_array(self.base_arr, n)
        self.assertEqual(len(a), n)
        
    def test_array_size_for_even(self):
        n = 42
        a = repeating_array(self.base_arr, n)
        self.assertEqual(len(a), n)
        

if __name__ == "__main__":
    unittest.main()

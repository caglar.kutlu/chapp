#!/bin/bash

#I divided the phase 2 data into two sections that may have slightly different JPA OFF
#noise temperatures.  My current toolset makes it easier to deal with such separation.
 
# MUST BE RUN FROM capphed rootdir
#CQ2HED="poetry run python -m capphed"

# changed it to not use poetry
CQ2HED="python3 -m capphed"

OUTNAME1="$DATADIR/capp8tb6g-he-p2__1"
OUTNAME2="$DATADIR/capp8tb6g-he-p2__2"

CQDATADIR="$HOME/data/8TB6G"
# Datasets
CD10_R8="$CQDATADIR/CD10-R8/exp00000049"
CD10_R9="$CQDATADIR/CD10-R9/exp00000050"
CD10_R10="$CQDATADIR/CD10-R10/exp00000051"
CD10_R11="$CQDATADIR/CD10-R11/exp00000053"
CD10_R12="$CQDATADIR/CD10-R12/exp00000057"
CD11_R1="$CQDATADIR/CD11-R1/exp00000060"
CD12_R1="$CQDATADIR/CD12-R1/exp00000065"
CD12_R2="$CQDATADIR/CD12-R2/exp00000069"
CD12_R3="$CQDATADIR/CD12-R3/exp00000070"
CD12_R4="$CQDATADIR/CD12-R4/exp00000075"
CD12_R5="$CQDATADIR/CD12-R5/exp00000077"
CD12_R6="$CQDATADIR/CD12-R6/exp00000078"

SOURCE="CAPP8TB-6G"

NTMEASDIR1="$DATADIR/CD11_NTMEAS_SPEQAN_BINW15625_X10dB.proc.nc"
NTMEASDIR2="$DATADIR/CD12_NTMEAS_SPEQAN_BINW15625_X20dB.proc.nc"

CAPPHEDDIR="$HOME/.local/src/capphed"

THISDIR=$PWD

pushd $CAPPHEDDIR


###############
#RUNNAME="CD10-R8"
#_RUNNAME=${RUNNAME//-/_}
#RUNDIR="${!_RUNNAME}"
###############

#echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

#$CQ2HED\
#    --exp $RUNDIR\
#    --conf $(dirname $RUNDIR)/conf_capp8tb.py\
#    --name $RUNNAME --out "$OUTNAME1"\
#    --source $SOURCE\
#    --ntmeas $NTMEASDIR1\
#    --slice 70\  # SLICE UNTIL THIS NUMBER, NOT INCLUSIVE!


###############
#RUNNAME="CD10-R9"
#_RUNNAME=${RUNNAME//-/_}
#RUNDIR="${!_RUNNAME}"
###############

#echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

#$CQ2HED\
#    --exp $RUNDIR\
#    --conf $(dirname $RUNDIR)/conf_capp8tb.py\
#    --name $RUNNAME --out $OUTNAME1\
#    --source $SOURCE\
#    --slice 131\

###############
#RUNNAME="CD10-R10"
#_RUNNAME=${RUNNAME//-/_}
#RUNDIR="${!_RUNNAME}"
###############

#echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

#$CQ2HED\
#    --exp $RUNDIR\
#    --conf $(dirname $RUNDIR)/conf_capp8tb.py\
#    --name $RUNNAME --out $OUTNAME1\
#    --source $SOURCE\
#    --slice 15\

###############
#RUNNAME="CD10-R11"
#_RUNNAME=${RUNNAME//-/_}
#RUNDIR="${!_RUNNAME}"
###############

#echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

#$CQ2HED\
#    --exp $RUNDIR\
#    --conf $(dirname $RUNDIR)/conf_capp8tb.py\
#    --name $RUNNAME --out $OUTNAME1\
#    --source $SOURCE\
#    --slice 28\

###############
#RUNNAME="CD10-R12"
#_RUNNAME=${RUNNAME//-/_}
#RUNDIR="${!_RUNNAME}"
###############

#echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

#$CQ2HED\
#    --exp $RUNDIR\
#    --conf $(dirname $RUNDIR)/conf_capp8tb.py\
#    --name $RUNNAME --out $OUTNAME1\
#    --source $SOURCE\
#    --slice 134\

###############
#RUNNAME="CD11-R1"
#_RUNNAME=${RUNNAME//-/_}
#RUNDIR="${!_RUNNAME}"
###############

#echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

#$CQ2HED\
#    --exp $RUNDIR\
#    --conf $(dirname $RUNDIR)/conf_capp8tb.py\
#    --name $RUNNAME --out $OUTNAME1\
#    --source $SOURCE\
#    --slice 368\

##############
RUNNAME="CD12-R1"
_RUNNAME=${RUNNAME//-/_}
RUNDIR="${!_RUNNAME}"
##############

echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

$CQ2HED\
    --exp $RUNDIR\
    --conf $(dirname $RUNDIR)/conf_capp8tb.py\
    --name $RUNNAME --out $OUTNAME2\
    --source $SOURCE\
    --ntmeas $NTMEASDIR2\
    --slice 620\
    --append

##############
RUNNAME="CD12-R2"
_RUNNAME=${RUNNAME//-/_}
RUNDIR="${!_RUNNAME}"
##############

echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

$CQ2HED\
    --exp $RUNDIR\
    --conf $(dirname $RUNDIR)/conf_capp8tb.py\
    --name $RUNNAME --out $OUTNAME2\
    --source $SOURCE\
    --slice 294\
    --append

##############
RUNNAME="CD12-R3"
_RUNNAME=${RUNNAME//-/_}
RUNDIR="${!_RUNNAME}"
##############

echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

$CQ2HED\
    --exp $RUNDIR\
    --conf $(dirname $RUNDIR)/conf_capp8tb.py\
    --name $RUNNAME --out $OUTNAME2\
    --source $SOURCE\
    --slice 446\
    --append

##############
RUNNAME="CD12-R4"
_RUNNAME=${RUNNAME//-/_}
RUNDIR="${!_RUNNAME}"
##############

echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

$CQ2HED\
    --exp $RUNDIR\
    --conf $(dirname $RUNDIR)/conf_capp8tb.py\
    --name $RUNNAME --out $OUTNAME2\
    --source $SOURCE\
    --slice 348\
    --append

##############
RUNNAME="CD12-R5"
_RUNNAME=${RUNNAME//-/_}
RUNDIR="${!_RUNNAME}"
##############

echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

$CQ2HED\
    --exp $RUNDIR\
    --conf $(dirname $RUNDIR)/conf_capp8tb.py\
    --name $RUNNAME --out $OUTNAME2\
    --source $SOURCE\
    --slice 309\
    --append

##############
RUNNAME="CD12-R6"
_RUNNAME=${RUNNAME//-/_}
RUNDIR="${!_RUNNAME}"
##############

echo "COMPILING DATA FOR $RUNNAME from $RUNDIR"

$CQ2HED\
    --exp $RUNDIR\
    --conf $(dirname $RUNDIR)/conf_capp8tb.py\
    --name $RUNNAME --out $OUTNAME2\
    --source $SOURCE\
    --slice 644\
    --append

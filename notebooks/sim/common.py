"""This module is intended to provide some common variables for notebooks to
use.  Also, prepends the path to the development version of the module.

"""
import os, sys

__all__ = ['datapath']

# The linux path doesn't work when called from conda environment
datapath = os.environ['AXIONDATAPATH']
""" str: Path to the folder containing exp* folders """

try:
    # Check if it's already there
    sys.path.index('..')
except ValueError:
    sys.path.insert(0, '..')
